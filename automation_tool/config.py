import sys
import os
import logging

# Add root dir into sys.path
_ROOT_DIR = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
if _ROOT_DIR not in sys.path:
    sys.path.append(_ROOT_DIR)

logger = logging.getLogger('main')
logger.setLevel(logging.DEBUG)

config = {
    'ROOT_DIR': _ROOT_DIR,
    'LOG_DIR': os.path.join(_ROOT_DIR, 'logs'),
    'LOGGER': logger,
    'INTERFACE': 'SSH',
    'DBFILE': 'test.db'
}

targetSupport = {
    'build-v234ce-gnu-linux-d': 'linux-d',
    'build-v234ce-gnu-linux-o': 'linux-o',
    'build-v234ce-gnu-sa-d': 'sa-d',
    'build-v234ce-gnu-sa-o': 'sa-o',
    'x86-linux-d': 'linux-d',
    'x86-windows-d': 'windows-d'
}

boardSupport = {
    'evb': 'S32V234 EVB',
    'sbc': 'S32V234 SBC'
}

osSupport = {
    'windows': 'Windows 10',
    'linux': 'Ubuntu 16.04'
}
