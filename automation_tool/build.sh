#!/bin/bash

__TODAY=$(date '+%y%m%d.%H%M%S')

__buildfolders=
__recursive=
__grabs=
__grabpath='.'
__dograb=0
__job=2
_logsum="build.sum.log"
__failed=
__makecmd=

#__ME=`basename "$0"`
#__BASEDIR=$(dirname "$0")
__ME=$(readlink -f "$0")

ARGS=$(getopt -o f:b:r:j:g:m:n:h -l "failed:,buildfolders:,recursive:,job:,grab:,makecommand:name:help" -n "getopt.sh" -- "$@");

eval set -- "$ARGS";

function fhelp()
{
    echo "${__ME} -f -b -r [-g] [-j <default 2>]"
    echo "  -f|--failed: path to sum log to rebuild all failed case" 
    echo "  -b|--buildfolders: build folders" 
    echo "  -r|--recursive: path to search build folders"
    echo "  -g|--grab: enable grabbing files"
    echo "  -m|--makecommand: specified command when make"
    echo "  -j|--job: 1,2,8"
    echo "  -n|--name: pack name"
    echo "Ex:"
    echo "- Build recursive kernel folders"
    echo "  ${__ME} -b build-v234ce-gnu-linux-d -r \"\$SDK_ROOT/libs/*/*/kernel\""
    echo "- Build build-v234ce-gnu-linux-d, recursive in \$SDK_ROOT with command 'clean allsub'"
    echo "  ${__ME} -b build-v234ce-gnu-linux-d -r \$SDK_ROOT -m 'clean allsub' \\"
    echo "          -n \`date '+pack.%y%m%d.%H%M%S'\`"
    echo "- Rebuild failed case only then update result to pack.xxxx/build-d/log/build.sum.log"
    echo "  ${__ME} -f pack.xxxx/build-d/log/build.sum.log"
    echo "  ${__ME} -b build-v234ce-gnu-linux-d -b build-v234ce-gnu-linux-o -r \$SDK_ROOT"
    echo "  ${__ME} -b build-v234ce-gnu-linux-d -b build-v234ce-gnu-linux-o \\"
    echo "          -r \$SDK_ROOT/libs/isp -r \$SDK_ROOT/demos -r \$SDK_ROOT/tests \\"
    echo "          -g *.ko -g *.elf"
    exit 0;
}

while true; do
    case "$1" in
        -f|--failed)
            shift;
            if [ -n "$1" ]; then
                __failed=$1;
                shift;
            fi
        ;;

        -n|--name)
            shift;
            if [ -n "$1" ]; then
                _packfld=$1;
                shift;
            fi
        ;;

        -m|--makecommand)
            shift;
            if [ -n "$1" ]; then
                __makecmd="${__makecmd} $1";
                shift;
            fi
        ;;

        -j|--job)
            shift;
            if [ -n "$1" ]; then
                __job=$1;
                shift;
            fi
        ;;

        -b|--buildfolders)
            shift;
            if [ -n "$1" ]; then
                __buildfolders="${__buildfolders} $1";
                shift;
            fi
        ;;

        -g|--grab)
            shift;
            if [ -n "$1" ]; then
                __grabs="${__grabs} $1";
                shift;
            fi
            __dograb=1;
        ;;

        -r|--recursive)
            shift;
            if [ -n "$1" ]; then
                __recursive="${__recursive} $1";
                shift;
            fi
        ;;

        -h|--help)
            fhelp;
        ;;

        --)
            shift;
            break;
        ;;
    esac
done

__job="-j${__job}";
if [[ -z $_packfld ]]; then
	_packfld=pack.${__TODAY};
    rm -rf ${_packfld};
fi

mkdir -p ${_packfld};


if [[ -z "${__makecmd}" ]]; then
	__makecmd="clean allsub"
fi;

_NOW=`date +"%s"`

if [[ -f "${__failed}" ]]; then
    # grep FAILED "${__failed}" | sed -e 's#\(.*\): FAILED#make -C \1 '${__job}'#g'
    _failed_lst=`grep FAILED "${__failed}" | sed -e 's#\(.*\): FAILED#\1#g'`
    echo "  Found: `grep FAILED "${__failed}" | wc -l` FAILED";
    echo "  ------------------------------"
    for _f in ${_failed_lst}; do

        _cmd=`echo make -C ${_f} ${__makecmd} ${__job}`;
        echo "  ${_cmd}";
        ${_cmd};
        
        if [[ "$?" -gt 0 ]]; then 
            echo "    [!] FAILED";
        else
            sed -i -e 's#'${_f}': FAILED#'${_f}': passed#g' ${__failed}
        fi
        echo "  ------------------------------"

    done

    _DIFF=$(( `date +"%s"` - $_NOW ))

	echo "--------------------------------------------------------------------------"
	echo "Total time: `date -d@${_DIFF} -u +%H:%M:%S`"
	echo "--------------------------------------------------------------------------"

    exit 0;
fi;


if [[ ( -z "${__recursive}" ) ]]; then
    fhelp;
fi;

echo "=========================================================================="
echo "Build with ${__job}";
echo "Store in: ${_packfld}";

for _buildfld in ${__buildfolders}; do
    
    _buildfld=`echo ${_buildfld} | sed -e 's#/\$##'`;
    _grabfld=${_packfld}/${_buildfld}
    mkdir -p ${_grabfld}/log;
    
    for _spath in ${__recursive}; do
        echo "--------------------------------------------------------------------------"
        echo "Querying in ${_spath}";
        __fldfound=`find ${_spath} -type d -name "${_buildfld}"`
        echo "  Found: `echo ${__fldfound} | grep -o "${_buildfld}" | wc -l` ${_buildfld}";
        echo "  ------------------------------"

        if [[ -z $__fldfound ]]; then
            rm -rf ${_grabfld};
            continue;
        fi

        for _tg in $__fldfound; do
            _logfull=$_tg;
            #if [[ ! -z "${SDK_ROOT}" ]]; then
            #    _logfull=`echo ${_logfull} | sed -e "s#$SDK_ROOT##g"`;
            #fi
            _logfull=`echo ${_tg} | sed -e 's#.*s32v234_sdk##g' | sed -e 's#^/##g' | sed -e 's#\(.\)#/\1#' | sed -e 's#/#.#g'`;
            _logfull="build${_logfull}.log"
            echo "  Log `pwd -P`/${_grabfld}/log/${_logfull}";
            _cmd=`echo make -C ${_tg} ${__makecmd} ${__job}`;
            echo "  ${_cmd}";
            echo "${_cmd}" >> ${_grabfld}/log/${_logfull};
            ${_cmd} &>> ${_grabfld}/log/${_logfull};

            if [[ "$?" -gt 0 ]]; then 
                echo "    [!] FAILED";
                echo "${_tg}: FAILED" >> ${_grabfld}/log/${_logsum};
            else
                echo "${_tg}: passed" >> ${_grabfld}/log/${_logsum};
            fi
            echo "  ------------------------------"
        done

        if [[ ( ${__dograb} -gt 0 ) ]]; then
            for _grab in $__grabs; do
                echo "  Grabbing ${_grab} from ${_spath} with target ${_buildfld}";
                find ${_spath} -type f -name "${_grab}" | grep "${_buildfld}" | xargs -I+ cp + ${_grabfld};
            done
        fi
    done
done

_DIFF=$(( `date +"%s"` - $_NOW ))

echo "--------------------------------------------------------------------------"
echo "Total time: `date -d@${_DIFF} -u +%H:%M:%S`"
echo "--------------------------------------------------------------------------"
# echo "Zipping ${_packfld}"
# tar czf ${_packfld}.tgz ${_packfld}
# echo "=========================================================================="
