#!/bin/bash

# ./build_isp.sh
# ./build_only.sh
# ./build_apex.sh
#!/bin/bash

# sdk_root
export SDK_ROOT=/home/vudv/VisionSDK_S32V2_RTM_1_3_0_RC4/s32v234_sdk
# export SDK_ROOT=/home/cuongnh13/vsdk/s32v234_sdk

_pack_test_apex=
_pack_test_linux=
_run_sum_log=logs/run.sum.log.yml
PACKS_NAME=rtm_1.3.0_rc4_auto
PACKS_DIR=rtm_1.3.0_rc4
if [ ! -d $PACKS_DIR ]
then
	mkdir -p $PACKS_DIR
fi
######################################################################
# TEST ENVIROMENTS
######################################################################
FPT_TESTS_DIR=$SDK_ROOT/tests/fpt/unittest
DEMOS_DIR=$SDK_ROOT/demos
NXP_APEX_TESTS_DIR=$SDK_ROOT/tests/nxp/test_apex
NXP_ISP_TESTS_DIR=$SDK_ROOT/tests/nxp/test_isp
NXP_UNITTEST_DIR=$SDK_ROOT/tests/nxp/unittest

# board tftp boot
TFTP_ROOT=

# unittest modules
_test_modules=(
	'demos' 	# 0
	'acf'		# 1
	'apexcv'	# 2
	'apu'		# 3
	'apex'		# 4
	'cgd'		# 5
	'csi'		# 6
	'fdma'		# 7
	'h264dec'	# 8
	'h264enc'	# 9
	'jpegdcd'	# 10
	'oal'		# 11
	'sdi'		# 12
	'seq'		# 13
	'umat'		# 14
	'viu'		# 15
	)
_test_modules_autotested=(
	'acf'		# 0
	'apexcv'	# 1
	'apu'		# 2
	'apex'		# 3
	'fdma' 		# 4
	'h264dec' 	# 5
	'h264enc'	# 6
	'jpegdcd'	# 7
	'oal'		# 8
	'umat'		# 9
	# 'csi'		# 10
	)

# gcov
_test_gcov_modules=(
	# 'csi'		# 0
	# 'fdma'		# 1
	# 'h264dec'	# 2
	# 'h264enc'	# 3
	'jpegdcd'	# 4
	# 'viu'		# 5
	)
# compiler
_test_compilers=(
	'nxp'
	'tct'
	)

# targets
_test_targets=(
	'build-v234ce-gnu-linux-d'
	'build-v234ce-gnu-linux-o'
	# 'build-v234ce-gnu-sa-d'
	# 'build-v234ce-gnu-sa-o'
	)

# os
_test_OSs=(
	'linux'
	'windows'
	)

# board
_test_boards=(
	'evb' # S32V234 EVB
	'sbc' # S32V234 SBC
	)



######################################################################
#  EXECUTE ALL DEMOS AND TESTS
######################################################################
# generate report
for _module in "${_test_modules_autotested[@]}"; do
	for _board in "${_test_boards[@]}"; do
		cat logs/$_board.$_module.log.yml >> logs/$_module.log.yml
	done
	python 6_report.py -l logs/$_module.log.yml -t desc/desc.$_module.yml -m $_module -o report/
done
