#!/bin/bash

# ./build_isp.sh
# ./build_only.sh
# ./build_apex.sh
#!/bin/bash

# sdk_root
export SDK_ROOT=/home/vudv/VisionSDK_S32V2_RTM_1_3_0_RC4/s32v234_sdk
# export SDK_ROOT=/home/vudv/workspace_2018/vsdk_repo/vsdk/s32v234_sdk

_pack_test_apex=
_pack_test_linux=
_run_sum_log=logs/run.sum.log.yml
PACKS_DIR=rtm_1.3.0_rc4_auto
if [ ! -d $PACKS_DIR ]
then
	mkdir -p $PACKS_DIR
fi
######################################################################
# TEST ENVIROMENTS
######################################################################
FPT_TESTS_DIR=$SDK_ROOT/tests/fpt/unittest
DEMOS_DIR=$SDK_ROOT/demos
NXP_APEX_TESTS_DIR=$SDK_ROOT/tests/nxp/test_apex
NXP_ISP_TESTS_DIR=$SDK_ROOT/tests/nxp/test_isp
NXP_UNITTEST_DIR=$SDK_ROOT/tests/nxp/unittest

# board tftp boot
TFTP_ROOT=

# unittest modules
_test_modules=(
	'demos' 	# 0
	'acf'		# 1
	'apexcv'	# 2
	'apu'		# 3
	'apex'		# 4
	'fdma'		# 5
	'h264dec'	# 6
	'h264enc'	# 7
	'jpegdcd'	# 8
	'oal'		# 9
	'umat'		# 10
	'sdi'		# 11
	'seq'		# 12
	'cgd'		# 13
	'csi'		# 14
	'viu'		# 15
	)

# gcov
_test_gcov_modules=(
	'csi'		# 0
	'fdma'		# 1
	'h264dec'	# 2
	'h264enc'	# 3
	'jpegdcd'	# 4
	'viu'		# 5
	)
# compiler
_test_compilers=(
	'nxp'
	'tct'
	)

# targets
_test_targets=(
	'build-v234ce-gnu-linux-d'
	'build-v234ce-gnu-linux-o'
	# 'build-v234ce-gnu-sa-d'
	# 'build-v234ce-gnu-sa-o'
	)

# os
_test_OSs=(
	'linux'
	'windows'
	)

# board
_test_boards=(
	'evb' # S32V234 EVB
	'sbc' # S32V234 SBC
	)


######################################################################
#  BUILD ALL DEMOS AND TESTS
######################################################################
# export APU_PRECOMP=1
for _os in "${_test_OSs[0]}"; do # OS supported
	for _compiler in "${_test_compilers[0]}"; do # Compilers supported
		# cleansub
		./scripts/clean.sh $_compiler $SDK_ROOT/libs
		./scripts/clean.sh $_compiler $SDK_ROOT/tests

		# export compiler to compile
		export APU_COMP=$_compiler
		echo "APU_COMP = $_compiler"

		# build demos, FPT tests apex: acf, apexcv and apu
		for i in {1..15}; do
			for _module in "${_test_modules[$i]}"; do
				echo "module = $_module"
				_build_dir=$FPT_TESTS_DIR/$_module
				if [ $_module == "demos" ]
				then
					_build_dir=$SDK_ROOT/demos
				fi
				_pack_test=$PACKS_DIR/$_os/$_module
				for _target in "${_test_targets[0]}"; do # targets supported
					echo "target = $_target"
					./scripts/build.sh -b $_target -r $_build_dir -g *.elf -g *.ko -g *.json -g *.avi -g *.mpg -n $_pack_test -j16
					find $_build_dir/*/data -type d -name '*data*' | xargs -I+ cp -r + $_pack_test/$_target
					if [ $_module == "demos" ]
					then
						find $_build_dir/data -type d -name '*data*' | xargs -I+ cp -r + $_pack_test/$_target
					fi
				done
	# 			# copy to tftpboot
	# 			if [ -d $TFTP_ROOT/$_pack_test_apex ]
	# 			then
	# 				# remove here
	# 			fi
	# 			cp -a $_pack_test_apex $TFTP_ROOT
			done
		done

		build NXP test_apex
		for _target in "${_test_targets[@]}"; do # targets supported
			echo "builing nxp test_apex"
			./scripts/build.sh -b $_target -r $NXP_APEX_TESTS_DIR -g *.elf -n $PACKS_DIR/$_os/nxp_tests/test_apex/$_compiler -j16
		done
	done

	# build FPT tests test_isp
	echo "build test linux modules"
	for i in {5..14}; do
		for _module in "${_test_modules[@]}"; do
			echo "module = $_module"
			_pack_test_linux=$PACKS_DIR/$_os/$_module
			for _target in "${_test_targets[@]}"; do # targets supported
				echo "target = $_target"
				./scripts/build.sh -b $_target -r $FPT_TESTS_DIR/$_module  -g *.elf -g *.ko -g *.json -g *.avi -g *.mpg -n $_pack_test_linux -j16
				find $FPT_TESTS_DIR/$_module/*/data -type d -name '*data*' | xargs -I+ cp -r + $_pack_test_linux/$_target
			done
			# copy to tftpboot
			# if [ -d $TFTP_ROOT/$_pack_test_linux ]
			# then
			# 	# remove here
			# fi
			# cp -a $_pack_test_linux $TFTP_ROOT
		done
	done

	build NXP test_isp
	for _target in "${_test_targets[@]}"; do # targets supported
		echo "building nxp test_isp"
		./scripts/build.sh -b $_target -r $NXP_ISP_TESTS_DIR -g *.elf -n $PACKS_DIR/$_os/nxp_tests/test_isp -j16
	done

done
# exit

# ######################################################################
# #  EXECUTE ALL DEMOS AND TESTS
# ######################################################################
# for _board in "${_test_boards[@]}"; do
# 	echo "board = $_board"
# 	if [ $_board == "evb" ]
# 	then 
# 		_board_id='192.168.1.4' # S32V234 EVB, TOTO...
# 		TFTP_ROOT=/tftpboot/evb.9/rootfs/home/root
# 		echo "ip: $_board_id"
# 	fi
# 	if [ $_board == "sbc" ]
# 	then
# 		_board_id='192.168.1.7' # S32v234 SBC
# 		TFTP_ROOT=/tftpboot/rfs/rootfs/home/root
# 		echo "ip: $_board_id"
# 	fi

# 	# copy to the TFTP root
# 	# cp -a packs $TFTP_ROOT

# 	for _os in "${_test_OSs[@]}"; do 
# 		echo "OS = $_os"
# 		# demos and test apex
# 		for i in {0..2}; do
# 			for _module in "${_test_modules_autotested[$i]}"; do
# 				echo "module = $_module"
# 				for _compiler in "${_test_compilers[0]}"; do
# 					echo "compiler = $_compiler"
# 					for _target in "${_test_targets[@]}"; do
# 						echo "target = $_target"
# 						python 5_run.py -i /home/root/rtm_1.3.0_rc1_auto/$_os/$_module/$_compiler/$_target -a $_board_id -u root -p '' -s testscripts/script.$_module -b $_target  -v $_board -o $_os -l logs/$_board.$_os.$_module.$_compiler.log.yml
# 					done
# 				done
# 				python 6_report.py -l logs/$_board.$_os.$_module.$_compiler.log.yml -t desc/desc.$_module.yml -m $_module -o report/
# 			done
		
# 		done

# 		# # test apex linux
# 		for i in {3..10}; do
# 			for _module in "${_test_modules_autotested[$i]}"; do
# 				echo "module = $_module"
# 				for _target in "${_test_targets[@]}"; do
# 					echo "target = $_target"
# 					python 5_run.py -i /home/root/rtm_1.3.0_rc1_auto/$_os/$_module/$_compiler/$_target -a $_board_id -u root -p '' -s testscripts/script.$_module -b $_target  -v $_board -o $_os -l logs/$_board.$_os.$_module.log.yml
# 				done
# 				python 6_report.py -l logs/$_board.$_os.$_module.log.yml -t desc/desc.$_module.yml -m $_module -o report/
# 			done
# 		done
# 	done
# done

# exit


