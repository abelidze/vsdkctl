This file contains information how to set up the automation test for your tests/demos

Step 0: Setup environment
	`source export_path.sh ...[options]...`
        - Use this cript to set up build environment: SDK_ROOT, toolchains folder and version (gcc 4.9; gcc 6.3), 

Step 1: Build tests
	`./build.sh ...[options]...`
        - Use this script to build your tests/demos. The script will collect the elf files to a folder 'pack_ddmmyyyy'

Step 2: Prepare test scripts
    `python createtest.py -i pack_ddmmyyyy`
        - This script will create python test script files in the same folder for each found elf file with the content following
        the template file in app/templates/testsuite_py.html:
         
Step 3: Run the tests
	`python run.py -i /home/root/elf_folder ...[options]...`
        - Before running this script, make sure the connection between the PC and the board works fine.
        - The default ssh username/password are root/root. You can change it with proper options. See 'python run.py --help'.
        - This script will:
       		+ Collect the python test script files in provided folder.
            + Execute the test script files one by one.
            + Generate the result summary file (default: run.sum.yml) for each module with all environment configurations: 
              board versions, operating systems, compiler versions, build targets
            + Detailed running log for each test suite is stored in the logs/ folder.



