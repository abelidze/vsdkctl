#!/usr/bin/python
"""
##############################################################################
#
# NXP Confidential Proprietary
#
# Copyright 2019 NXP;
# All Rights Reserved
#
##############################################################################
#
# THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
# THE POSSIBILITY OF SUCH DAMAGE.
#
##############################################################################
#
#  CONTENT: Continuous Integration
#
#
#  AUTHOR
#    Mihail Marian Nistor
#
#  LANGUAGE
#    python
#
##############################################################################
"""

import shlex
import re
import fnmatch
import json
import configparser
import yaml
import argparse
import subprocess
import sys
import os
from time import time

SUFFIX_RUNTIME_LOG = 'runtime.log'
BOARDS_LISTS = [ 's32v234evb', 's32v234sbc' ]
BOARDS_TYPE = {
    's32v234evb': 'S32V234 EVB',
    's32v234sbc': 'S32V234 SBC'
}

HOST_OS = {
    'windows': 'Windows 10',
    'linux': 'Ubuntu 14.04'
}

LINUX_TARGETS = {
    'build-v234ce-gnu-linux-d': 'linux-d',
    'build-v234ce-gnu-linux-o': 'linux-o',
    'x86-linux-d': 'linux-d',
    'x86-windows-d': 'windows-d'
}

QNX_TARGETS = {
    'build-v234ce-qcc-qnx-d': 'qnx-d',
    'build-v234ce-qcc-qnx-o': 'qnx-o'
}

def __get_targets(arg_target_os):
    if arg_target_os == "Linux":
        return LINUX_TARGETS
    elif arg_target_os == "QNX":
        return QNX_TARGETS
    else:
        return None

# -----------------------------------------------------------------------------
def __parse_config(arg_config_file_path):
    """
    Parse the configuration file.
    """
    config_file = configparser.RawConfigParser()
    config_file.read(arg_config_file_path)

    if not config_file.has_section('OPTIONS'):
        return None

def __parse_options():
    """
    Parse command line options having as default the values set in configuration file.
    """
    # Set up command line parameters
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--module", dest="module", help="Module name")
    parser.add_argument("--apu_comp", dest="apu_comp", help="APU compiler type")
    parser.add_argument("--host_os", dest="host_os", help="Operation Systeam on PC")
    parser.add_argument("--target_os", dest="target_os", help="Operation Systeam on board")
    parser.add_argument("--test_desc", dest="test_desc", help="Test description file")
    parser.add_argument("--run_sum", dest="run_sum",
                        help="The runtime summary result of tests")
    parser.add_argument("--path_to_tests_logs", dest="path_to_tests_logs", help="Path to runtime tests log")
    parser.add_argument("--version", action='version', version='%(prog)s 0.1')

    options = parser.parse_args()

    return (options, parser)
    
def find_file(arg_dir, arg_pattern):
    """ find all files in a folder by using a pattern name"""
    results = []
    for curr_dir, dirs, files in os.walk(arg_dir):
        fs = fnmatch.filter(files, arg_pattern)
        results.extend(os.path.join(curr_dir, f) for f in fs)
    return results

def tc_status(arg_file, arg_pattern_tc):
    """ get status of test compoment """
    cmd = "grep -E \"{0}\" {1}".format(arg_pattern_tc, arg_file)
    retcmd = subprocess.call(cmd, shell=True)
    if retcmd == 0:
        return '1'
    else:
        return '0'

def process_board(arg_tartget_os, arg_board, arg_ts, arg_options):
    """  process all tests log for a board """
    dict_pc = dict()
    dict_target = dict()
    patten_file = "{0}*.{1}.{2}.{3}".format(arg_options.module,
                                                arg_board,
                                                arg_tartget_os,
                                                SUFFIX_RUNTIME_LOG)
    files = find_file(arg_options.path_to_tests_logs, patten_file)
    targets = __get_targets(arg_tartget_os)
    if targets is not None:
        for target, target_short in targets.items():
            dict_ts = dict()
            for f in files:
                target_file = os.path.basename(os.path.dirname(f))
                name_log = os.path.basename(f)
                if target == target_file:
                    names = name_log.split('.')[0].split('_')
                    assert(len(names)!=2)
                    name_ts = names[0] + '_' + names[1] + '_' + names[2]
                    dict_tc = dict()
                    for tc, desc in arg_ts[name_ts].items():
                        if dict_tc.get(tc) != '0':
                            pattern_tc = "{0} : OK".format(tc)
                            dict_tc.update({ tc: tc_status(f, pattern_tc)})
                    dict_ts.update({ name_ts: dict_tc })
            if len(dict_ts) != 0:
                dict_target.update({target_short:dict_ts})

    if len(dict_target) != 0:
        if not arg_options.apu_comp:
            dict_pc = { HOST_OS[OPTIONS.host_os]: dict_target }
        else:
            dict_apu_comp = { arg_options.apu_comp: dict_target }
            dict_pc = { HOST_OS[OPTIONS.host_os]: dict_apu_comp }

    return dict_pc

if __name__ == '__main__':
    (OPTIONS, PARSER) = __parse_options()

    if (not OPTIONS.module or
            not OPTIONS.host_os or
            not OPTIONS.target_os or
            not OPTIONS.test_desc or
            not OPTIONS.run_sum or
            not OPTIONS.path_to_tests_logs):
        print (OPTIONS)
        PARSER.print_help()
        sys.exit(-1)

    # delete the previous result
    if os.path.isfile(OPTIONS.run_sum):
        os.remove(OPTIONS.run_sum)

    TEST_DESC = dict()
    with open(OPTIONS.test_desc, 'r') as fh:
        TEST_DESC = yaml.load(fh)
    DICT_SUM = dict()
    for board in BOARDS_LISTS:
       dict_board = process_board(OPTIONS.target_os, board, TEST_DESC["description"], OPTIONS)
       if len(dict_board) != 0:
           DICT_SUM.update({BOARDS_TYPE[board]: dict_board })

    if len(DICT_SUM) != 0:
        with open(OPTIONS.run_sum, 'w') as yaml_result:
            dump = yaml.dump(DICT_SUM, default_flow_style=False)
            yaml_result.write(dump)

