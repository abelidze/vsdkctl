import os
from jinja2 import Environment, FileSystemLoader, Template
import fnmatch
import argparse
import shutil
def list_demos_path(demos_module_folder):
    array = [];
    for module in demos_module_folder:
        for root, dirs, filename in os.walk(module):
            for dirname in dirs:
                if dirname == 'src':
                    parent_dir = os.path.basename(root)
                    component_dir = os.path.basename(os.path.dirname(os.path.dirname(root)))
                    srcpath = os.path.join(root, dirname)
                    array.append(srcpath)
    return array


def list_fpt_path(fpt_module_folder):
    array = [];
    for module in fpt_module_folder:
        for root, dirs, filename in os.walk(module):
            for dirname in dirs:
                if dirname == 'src':
                    parent_dir = os.path.basename(root)
                    component_dir = os.path.basename(os.path.dirname(os.path.dirname(root)))
                    if component_dir in parent_dir and 'ts' in parent_dir:
                        srcpath = os.path.join(root, dirname)
                        array.append(srcpath)
    return array

def findtestcase(_Path):
    case_array=[]
    listOfFiles = os.listdir(_Path)
    pattern = "*tc*.cpp"  
    for entry in listOfFiles:
        if fnmatch.fnmatch(entry, pattern):
            case_array.append(get_file_base_name(entry))
    return case_array

def findtestsuit(_Path):
    listOfFiles = os.listdir(_Path)
    pattern = "*ts*.cpp"  
    for entry in listOfFiles:
        if fnmatch.fnmatch(entry, pattern) and 'cfg' not in entry:
            return get_file_base_name(entry)

def get_file_name(_Path):
    return os.path.basename(_Path.strip())

def get_file_base_name(_Path):
    file_name = get_file_name(_Path.strip())
    return os.path.splitext(file_name)[0]

def createpyfile(base, testcases, ret_tc):
    env = Environment(loader=FileSystemLoader('./templates'))
    template = env.get_template('testsuite_py.html')
    output_from_parsed_template = template.render(suit_name = base, testcases = testcases, ret_tc=ret_tc)
    fh = open('pyfiles/' + str(base) + '.py' , 'w+')
    fh.write(output_from_parsed_template)

def main():
    # #Argument parse
    parser = argparse.ArgumentParser()
    parser.add_argument('-b',action='append', dest='build_folder', required=False, help='|--buildfolders: build folders')
    parser.add_argument('-r',action='append', dest='recursive', required=False, help='|--recursive: path to search build folders')
    parser.add_argument('-g',action='append', dest='grab', required=False, help='|--grab: enable grabbing files')
    parser.add_argument('-d', dest='dir', required=True, help='--dir: binary output folder')
    args = parser.parse_args()
    _arg_bf = ''
    _arg_r = ''
    _arg_g = ''
    _arg_d = ''
    # for _arg in args.build_folder:
        # _arg_bf += ' -b ' + _arg
    # for _arg in args.recursive:
    #     _arg_r += ' -r ' + _arg
    # for _arg in args.grab:
    #     _arg_g += ' -g ' + _arg
    # _arg_d = ' -d ' + args.dir
    # _cmd =  '../build.sh'+ _arg_bf + _arg_r + _arg_g + _arg_d
    # print _cmd
    # #Execute build.sh
    # os.system(_cmd)

    #Generate python test files
    demos_test_path = os.environ["SDK_ROOT"] + "/demos"
    
    fpt_test_path = os.environ["SDK_ROOT"] + "/tests/fpt/unittest"

    fpt_module_folder = [
        fpt_test_path + '/test_apex',
        fpt_test_path + '/test_linux'
    ]

    demos_module_folder = [
        demos_test_path + '/apex',
        demos_test_path + '/apexcv_base',
        demos_test_path + '/apexcv_pro',
        demos_test_path + '/neon',
        demos_test_path + '/openvx',
        demos_test_path + '/other',
    ]

    fpt_suit = []
    fpt_suit = list_fpt_path(fpt_module_folder)
    demos_suit = []
    demos_suit = list_demos_path(demos_module_folder)

    for path in fpt_suit:
        base_suit = findtestsuit(path)
        base_case = findtestcase(path)
        ret_tc = []
        for index in range(0, len(base_case)):
            ret_tc.append(1)
        createpyfile(base_suit, base_case, ret_tc)

    for path in demos_suit:
        ret_tc = [1]
        demos_case = [get_file_base_name(os.path.abspath(os.path.join(path, os.pardir)))]
        createpyfile(get_file_base_name(os.path.abspath(os.path.join(path, os.pardir))), demos_case, ret_tc)
    # print "./" + args.dir +"/" + args.build_folder[0]
    for root, dirs, files in os.walk(args.dir):
        for file in files:
            if file.endswith(".elf"):
                get_file_base_name(os.path.join(root, file))
                shutil.copy2('./pyfiles/' + get_file_base_name(os.path.join(root, file)) + '.py', '../testscript')

    #Run test files
    
    #Generate report

main()