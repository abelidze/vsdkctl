#!/bin/bash

# ./build_isp.sh
# ./build_only.sh
# ./build_apex.sh
#!/bin/bash

# sdk_root
export SDK_ROOT=/home/vudv/VisionSDK_S32V2_RTM_1_3_0_RC4/s32v234_sdk
# export SDK_ROOT=/home/cuongnh13/vsdk/s32v234_sdk

_pack_test_apex=
_pack_test_linux=
_run_sum_log=logs/run.sum.log.yml
PACKS_NAME=rtm_1.3.0_rc4_auto
PACKS_DIR=rtm_1.3.0_rc4
if [ ! -d $PACKS_DIR ]
then
	mkdir -p $PACKS_DIR
fi
######################################################################
# TEST ENVIROMENTS
######################################################################
FPT_TESTS_DIR=$SDK_ROOT/tests/fpt/unittest
DEMOS_DIR=$SDK_ROOT/demos
NXP_APEX_TESTS_DIR=$SDK_ROOT/tests/nxp/test_apex
NXP_ISP_TESTS_DIR=$SDK_ROOT/tests/nxp/test_isp
NXP_UNITTEST_DIR=$SDK_ROOT/tests/nxp/unittest

# board tftp boot
TFTP_ROOT=

# unittest modules
_test_modules=(
	'demos' 	# 0
	'acf'		# 1
	'apexcv'	# 2
	'apu'		# 3
	'apex'		# 4
	'cgd'		# 5
	'csi'		# 6
	'fdma'		# 7
	'h264dec'	# 8
	'h264enc'	# 9
	'jpegdcd'	# 10
	'oal'		# 11
	'sdi'		# 12
	'seq'		# 13
	'umat'		# 14
	'viu'		# 15
	)
_test_modules_autotested=(
	'acf'		# 0
	'apexcv'	# 1
	'apu'		# 2
	'apex'		# 3
	'fdma' 		# 4
	'h264dec' 	# 5
	'h264enc'	# 6
	'jpegdcd'	# 7
	'oal'		# 8
	'umat'		# 9
	# 'csi'		# 10
	)

# gcov
_test_gcov_modules=(
	# 'csi'		# 0
	# 'fdma'		# 1
	# 'h264dec'	# 2
	# 'h264enc'	# 3
	'jpegdcd'	# 4
	# 'viu'		# 5
	)
# compiler
_test_compilers=(
	'nxp'
	'tct'
	)

# targets
_test_targets=(
	'build-v234ce-gnu-linux-d'
	'build-v234ce-gnu-linux-o'
	# 'build-v234ce-gnu-linux-d'
	# 'build-v234ce-gnu-linux-o'
	)

# os
_test_OSs=(
	'linux'
	'windows'
	)

# board
_test_boards=(
	'evb' # S32V234 EVB
	'sbc' # S32V234 SBC
	)


######################################################################
#  BUILD ALL DEMOS AND TESTS
######################################################################
# export APU_PRECOMP=1
# for _os in "${_test_OSs[0]}"; do # OS supported
# 	# for _compiler in "${_test_compilers[0]}"; do # Compilers supported
# 	# 	# cleansub
# 	# 	# ./scripts/clean.sh $_compiler $SDK_ROOT/libs
# 	# 	# ./scripts/clean.sh $_compiler $SDK_ROOT/tests

# 	# 	# export compiler to compile
# 	# 	export APU_COMP=$_compiler
# 	# 	echo "APU_COMP = $_compiler"
# 	# 	# build demos test apex: acf, apexcv and apu
# 	# 	for i in {0..3}; do
# 	# 		for _module in "${_test_modules[$i]}"; do
# 	# 			echo "module = $_module"
# 	# 			_build_dir=$FPT_TESTS_DIR/$_module
# 	# 			if [ $_module == "demos" ]
# 	# 			then
# 	# 				_build_dir=$SDK_ROOT/demos
# 	# 			fi
# 	# 			_pack_test_apex=$PACKS_DIR/$_os/$_module/$_compiler
# 	# 			for _target in "${_test_targets[@]}"; do # targets supported
# 	# 			# for _target in "build-apu-tct-sa-d"; do
# 	# 				echo "target = $_target"
# 	# 				./scripts/build.sh -b $_target -r $_build_dir -g *.elf -n $_pack_test_apex -j16
# 	# 				find $_build_dir/data -type d -name '*data*' | xargs -I+ cp -r + $_pack_test_apex/$_target
# 	# 				# ./scripts/build.sh -b $_target -r $_build_dir - -j16
# 	# 			done

# 	# # 			# copy to tftpboot
# 	# # 			if [ -d $TFTP_ROOT/$_pack_test_apex ]
# 	# # 			then
# 	# # 				# remove here
# 	# # 			fi
# 	# # 			cp -a $_pack_test_apex $TFTP_ROOT

# 	# 		done
# 	# 	done
# 	# done

# 	# build test linux modules
# 	echo "build test linux modules"
# 	for i in {5..15}; do
# 		for _module in "${_test_modules[$i]}"; do
# 			echo "module = $_module"
# 			_pack_test_linux=$PACKS_DIR/$_os/$_module
# 			for _target in "${_test_targets[@]}"; do # targets supported
# 				echo "target = $_target"
# 				./scripts/build.sh -b $_target -r $FPT_TESTS_DIR/$_module  -g *.elf -g *.ko -g *.json -g *.avi -g *.mpg -n $_pack_test_linux -j16
# 				find $FPT_TESTS_DIR/$_module/*/data -type d -name '*data*' | xargs -I+ cp -r + $_pack_test_linux/$_target
# 			done
# 			# copy to tftpboot
# 			# if [ -d $TFTP_ROOT/$_pack_test_linux ]
# 			# then
# 			# 	# remove here
# 			# fi
# 			# cp -a $_pack_test_linux $TFTP_ROOT
# 		done
# 	done

# done


######################################################################
#  EXECUTE ALL DEMOS AND TESTS
######################################################################
for _board in "${_test_boards[1]}"; do
	echo "board = $_board"
	if [ $_board == "evb" ]
	then 
		_board_id='192.168.1.4' # S32V234 EVB, TOTO...
		# TFTP_ROOT=/tftpboot/evb.9/rootfs/home/root
		echo "ip: $_board_id"
	fi
	if [ $_board == "sbc" ]
	then
		_board_id='192.168.1.7' # S32v234 SBC
		# TFTP_ROOT=/tftpboot/rfs/rootfs/home/root
		echo "ip: $_board_id"
	fi

	# copy to the TFTP root
	# cp -a packs $TFTP_ROOT

	for _os in "${_test_OSs[0]}"; do 
		echo "OS = $_os"
		# demos and test apex
		# for i in {0..2}; do
		# 	for _module in "${_test_modules_autotested[$i]}"; do
		# 		echo "module = $_module"
		# 		for _compiler in "${_test_compilers[0]}"; do
		# 			echo "compiler = $_compiler"
		# 			for _target in "${_test_targets[@]}"; do
		# 				echo "target = $_target"
		# 				python 5_run.py -i /home/root/rtm_1.3.0_rc1_auto/$_os/$_module/$_compiler/$_target -a $_board_id -u root -p '' -s testscripts/script.$_module -b $_target  -v $_board -o $_os -l logs/$_board.$_module.log.yml
		# 			done
		# 		done
		# 		# python 6_report.py -l logs/$_board.$_module.log.yml -t desc_/desc.$_module.yml -m $_module -o report/
		# 	done
		
		# done

		# # test apex linux
		for i in {3..9}; do
			for _module in "${_test_modules_autotested[$i]}"; do
				echo "module = $_module"
				for _target in "${_test_targets[1]}"; do
					echo "target = $_target"
					python 5_run.py -i /home/root/rtm_1.3.0_rc4_auto/$_os/$_module/$_target -a $_board_id -u root -p '' -s testscripts/script.$_module -b $_target  -v $_board -o $_os -l logs/$_board.$_module.log.yml
				done
				python 6_report.py -l logs/$_board.$_module.log.yml -t desc/desc.$_module.yml -m $_module -o report_rc4_sbc/
			done
		done
	done
done

exit


# for _module in "${_test_modules[3]}"; do
# 	echo "module = $_module"
# 	_packname=$packs_dir/pack.${_test_OSs[0]}.$_module
# 	if [ ! -d "$_packname" ]
# 	then
#     	mkdir -p $_packname;
# 	fi
# 	for _target in "${_test_targets[@]}" ; do
# 		echo "target = $_target"
# 		./build.sh -b $_target -r $FPT_TESTS_DIR/$_module -g *.elf -n $_packname -j16
# 	done
# done

# for _target in build-v234ce-gnu-linux-d build-v234ce-gnu-linux-o ; do
# 		./build.sh -b $_target -r $SDK_ROOT/demos -g *.elf -g *.ko -g *.json -g *.avi -g *.mpg -n $_packname -j16
# 		find $SDK_ROOT/demos/data -type d -name '*data*' | xargs -I+ cp -r + $_packname/$_target
# 		cp $_packname/$_target/log/build.sum.log $packs_dir/build.sum.log
# done


# build, run test 
# for _module in acf apex apexcv apu cgd csi fdma h264dec h264enc jpegdcd oal sdi seq umat viu ; do
# 	for _module in acf apex apexcv apu cgd csi fdma h264dec h264enc jpegdcd oal sdi seq umat viu ; do
# 	_packname=$packs_dir/pack.$_OS.tests/$_module
# 	if [ ! -d "$_packname" ]
# 	then
#     	mkdir $_packname;
# 	fi
# 	for _target in build-v234ce-gnu-linux-d build-v234ce-gnu-linux-o ; do
# 		# build
# 		./build.sh -b $_target -r $TEST_DIR/$_module -g *.elf -g *.ko -g *.json -g *.avi -g *.mpg -n $_packname -j16
# 		find $TEST_DIR/$_module/*/data -type d -name '*data*' | xargs -I+ cp -r + $_packname/$_target
# 		cp $_packname/$_target/log/build.sum.log $packs_dir/build.sum.log

# 		# run, has been not tested on tftpboot on the server
# 		cp -a $TEST_DIR/$_module/$_target $TFTPROOT
# 		python 5_run.py -i /home/root/$_packname/$_target -a 192.168.1.8 -u root -p '' -s scripts/script.$_module -b $_target  -v 1 -o $_os -l run.sum.$_module.yml
# 	done

# 	# report
# 	python 6_report.py -l run.sum.$_module.yml -t desc_/desc.$_module.yml -m $_module -o report/
# done



# # NXP TEST test apex
# for _module in apexcv_base apexcv_pro cppunitapp dnn ; do
# 	# for _apucomp in nxp ; do
# 		# export APU_COMP=$_apucomp
# 		_packname=$packs_dir/pack.$_OS.NXP_TEST_APEX.$_module
# 		if [ ! -d "$_packname" ]
# 		then
#     		mkdir $_packname;
# 		fi
# 		for _target in build-v234ce-gnu-linux-d build-v234ce-gnu-linux-o ; do
# 			# make -C $SDK_ROOT/tests/nxp/test_apex/cppunitapp/$_target cleansub;
# 			./build.sh.bk -b $_target -r $NXP_APEX_TEST_DIR -g *.elf -g *.ko -g *.json -g *.avi -g *.mpg -n $_packname -j16
# 		done
# 	# done
# done

# # # NXP TEST test isp
# # for _module in apexcv_base apexcv_pro cppunitapp dnn ; do
# 	# for _apucomp in nxp ; do
# 		# export APU_COMP=$_apucomp
# 		_packname=$packs_dir/pack.$_OS.NXP_TEST_ISP.$_module
# 		if [ ! -d "$_packname" ]
# 		then
#     		mkdir $_packname;
# 		fi
# 		for _target in build-v234ce-gnu-linux-d build-v234ce-gnu-linux-o ; do
# 			./build.sh.bk -b $_target -r $NXP_ISP_TEST_DIR -g *.elf -g *.ko -g *.json -g *.avi -g *.mpg -n $_packname -j16
# 		done
# 	# done
# done

