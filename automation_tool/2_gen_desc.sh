echo "description:"
_delim=$'\u21D1'
tss=`find /home/vudv/workspace_2018/vsdk_repo/vsdk/s32v234_sdk/tests/fpt/unittest/viu -type d -name '*_[tT][sS]_*'`
for _ts in $tss; do
    __ts=`echo $_ts | sed -e 's#.*/\(.*_[tT][sS].*\)#\1#g'`
    echo "    $__ts:"
    tcs=`find $_ts -type f -name '*_[tT][cC]_*.cpp'`
    for _tc in $tcs; do
        brief=`sed -n '/\* @test_id /{:append;N;/\* @details/!b append;s#[\r]*\n#'$_delim'#g; p}' $_tc | sed -e 's#.*\* @brief[ ]*##g' -e 's#\* @details.*##g' -e 's# \{2,\}##g' -e 's#'$_delim'.# #g' -e 's#'$_delim'##g'`
        tc=`echo $_tc | sed -e "s#$SDK_ROOT/tests/fpt##g" -e 's#.*/\([^/]*\)/src/\(.*\).cpp#\2#g'`
        echo "        $tc: \"$brief\"";
    done
done
 
#echo ""
# 
#echo "target:"
#for _tg in build-v234ce-gnu-linux-d build-v234ce-gnu-linux-o build-v234ce-gnu-sa-d build-v234ce-gnu-sa-o; do
#    echo "    $_tg:"
#    echo "        name: `echo $_tg | sed -e 's#.*-\(.*-.*-.*\)#\1#g'`"
#    echo "        test-suites:"
#    find $SDK_ROOT/tests/fpt/unittest/test_linux/apex -type d -name $_tg | grep _[tT][sS]_ | sed -e 's#.*/\(.*\)/'$_tg'#            - \1#g'
#done
