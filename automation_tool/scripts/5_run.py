#!/usr/bin/env python2.7
import sys
import os
import logging

# Add root dir into sys.path
_ROOT_DIR = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
if _ROOT_DIR not in sys.path:
    sys.path.append(_ROOT_DIR)

from config import logger, config
config['ROOT_DIR'] = _ROOT_DIR
from config import targetSupport, boardSupport, osSupport
from ../lib.utils import log_message
import argparse, fnmatch, imp
import sys, traceback, threading
import yaml
from ../lib import SSHConnection
def run_testscript(runner, tscript):
    msg = "-------------------------------------------------------------------------"
    log_message(None, msg)
    
    tsname = tscript.__name__
    log_message(None, "Start execution of test script: [%s]" %(tsname))
    # prepare log file
    logPath = os.path.join(config['LOG_DIR'], tsname + '.log')
    reporter = logging.getLogger('%s' %tsname)
    reporter.setLevel(logging.DEBUG)
    fhandler = logging.FileHandler(logPath, mode='w')
    fhandler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(message)s')
    fhandler.setFormatter(formatter)
    reporter.addHandler(fhandler)

    testScriptObj = None
    finalResult = True
    try:
        testScriptObj = tscript(runner, reporter)
        if testScriptObj:
            try:
                log_message(None, "Function %s.preExecute():" %tsname, 1)
                res = testScriptObj.preExecute()
                log_message(None, "returns: %s" % (str(res).upper()), 1)
                if not res:
                    finalResult = False
            except Exception as ex:
                finalResult = False
                log_message(None, "returns: ERROR [%s]" % (str(ex)))
                log_message(reporter, traceback.format_exc())

        # If preExecute ok, run the test case
        if finalResult:
            log_message(None, "Function %s.run():" %tsname, 1)
            res = testScriptObj.run()
            log_message(None, "returns: %s" % (str(res).upper()), 1)
            if not res:
                finalResult = False
    except KeyboardInterrupt as ki:
        finalResult = False
        log_message(None, "User interrupted executing!", 1)
    except Exception as ex:
        log_message(None, "returns: ERROR [%s]" % (str(ex)), 1)
        log_message(reporter, traceback.format_exc())
        finalResult = False
    finally:
        if testScriptObj:
            try:
                log_message(None, "Function %s.postExecute():" %tsname, 1)
                res = testScriptObj.postExecute()
                log_message(None, "returns: %s" % (str(res).upper()), 1)
                if not res:
                    finalResult = False
            except Exception as ex:
                finalResult = False
                log_message(None, "returns: ERROR [%s]" % (str(ex)), 1)
                log_message(reporter, traceback.format_exc())
            
        if finalResult:
            log_message(None, "[%s]: passed" % tsname)
            log_message(reporter, "[%s]: passed" % tsname)
        else:
            log_message(None, "[%s]: FAILED" % tsname)
            log_message(reporter, "[%s]: FAILED" % tsname)

        # Close test suite report file handler
        fhandler.close()
    return finalResult

def runTest(runner, scriptDir):
    # TO-DO: get the test list from testscript folder
    # Support to execute defined test list
    files = fnmatch.filter(os.listdir(scriptDir), '*.py')
    for _file in files:
        (class_name, ext) = os.path.splitext(_file)
        filePath = os.path.join(scriptDir, _file)
        (fp, filePath, desc) = imp.find_module(class_name)
        test_mod = imp.load_module(class_name, fp, filePath, desc)
        testscript = getattr(test_mod, class_name)
        run_testscript(runner, testscript)
    


# start execution
if __name__ == '__main__':
    # Argument parsing
    ex_msg = 'example: python run.py -i /home/root/elf_folder -s testscript -a 10.0.0.2 -u root -p root -b build-v234ce-gnu-linux-d -v 1 -c nxp -o linux'
    
    parser = argparse.ArgumentParser(epilog=ex_msg)
    parser.add_argument('-i', dest='inputDir', required=True, help='Path to the folder contains all input files on the board. Ex: /home/root/elf_folder')
    parser.add_argument('-s', dest='scriptDir', required=True, help='Path to the local folder contains all the test scripts. Ex: ../testscript')
    parser.add_argument('-b', dest='buildTarget', required=True, help='Full name of the build target. Ex: build-v234ce-gnu-linux-d')
    parser.add_argument('-v', dest='boardID', required=True, help='The board ID is being used: 1 is \'S32V234 EVB\', 2 is \'S32V234 SBC\'')
    parser.add_argument('-o', dest='osName', required=True, help='The tests were built on the operating system: \'linux\' or \'windows \'.')
    parser.add_argument('-c', dest='compiler', help='The compiler is being used: \'nxp\' or \'ctc\'.')
    parser.add_argument('-l', dest='sumLog', default='run.sum.yml', help='The summary running log file. Tool will use the test result in the file if any.')
    parser.add_argument('-a', dest='boardAddr', default='10.0.0.2', help='Board IP address. Default is \'10.0.0.2\'')
    parser.add_argument('-u', dest='userName', default='root', help='SSH User Name. Default is \'root\'')
    parser.add_argument('-p', dest='password', default='root', help='SSH Password. Default is \'root\'')
    args = parser.parse_args()

    # Decorate path of directories
    args.inputDir = args.inputDir.rstrip('/')
    args.scriptDir = args.scriptDir.rstrip('/')

    # Parameter validation
    if not args.scriptDir.startswith('/'):
        args.scriptDir = os.path.abspath(os.path.join(_ROOT_DIR, args.scriptDir))
    if not os.path.exists(args.scriptDir):
        log_message(None, "ERROR: Script directory does not exist")
        sys.exit(1)
    if args.scriptDir not in sys.path:
        sys.path.append(args.scriptDir)
    if args.buildTarget not in targetSupport:
        log_message(None, "ERROR: Build target should be in %s" %targetSupport.keys())
        sys.exit(1)

    # Create log folder
    logDir = os.path.join(config['LOG_DIR'], args.buildTarget)
    config['LOG_DIR'] = logDir
    if not os.path.exists(logDir):
        os.makedirs(logDir)

    # Save the test result from previous running if existing
    origin = None
    args.sumLog = os.path.join(_ROOT_DIR, args.sumLog)
    if os.path.isfile(args.sumLog):
        log_message(None, 'The "%s" file exists. Save the previous test result.' %(os.path.basename(args.sumLog)))
        with open(args.sumLog, 'r') as fh:
            origin = yaml.load(fh)

    # Allow over-writing the summary file with the present result
    formatter = logging.Formatter('%(message)s')
    file_handler = logging.FileHandler(args.sumLog, mode='w')
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    # Opening connection to the board
    runner = SSHConnection(args.boardAddr, args.userName, args.password, args.inputDir)
    runner.open()
    runTest(runner, args.scriptDir)
    runner.close()

    # Post-proccess the summary file after being filled by tests
    # Retrieve the board name, OS info, compiler info, target-build name
    boardName = boardSupport[args.boardID]
    osInfo = osSupport[args.osName.lower()]
    if args.compiler:
        args.compiler = args.compiler.lower()
    targetName = targetSupport[args.buildTarget]
    # Read the log file
    with open(args.sumLog, 'r') as fh:
        current = yaml.load(fh)

    targetDict = dict()
    compilerDict = dict()
    osDict = dict()
    # If the previous result exists
    if bool(origin):
        if boardName in origin.keys():
            osDict = origin[boardName]
            if osInfo in osDict.keys():
                compilerDict = osDict[osInfo]
                if 'nxp' in compilerDict.keys() or 'tct' in compilerDict.keys():
                    if bool(args.compiler) and args.compiler in compilerDict: # Compiler info is available
                        targetDict = compilerDict[args.compiler]
                        if targetName in targetDict.keys():
                            log_message(None, "WARNING: the result for target '%s' is going to be over-written." %targetName)
                else:
                    targetDict = osDict[osInfo]
                    if targetName in targetDict.keys():
                        log_message(None, "WARNING: the result for target '%s' is going to be over-written." %targetName)
    else:
        origin = dict()

    targetDict[targetName] = current
    if bool(args.compiler):
        compilerDict[args.compiler] = targetDict
        osDict[osInfo] = compilerDict
    else:
        osDict[osInfo] = targetDict
    origin[boardName] = osDict

    with open(args.sumLog, 'w') as fh:
        yaml.dump(origin, fh, default_flow_style=False)

    # Check resources leaked then exit
    still_remain_thread = False
    for thread_id, frame in sys._current_frames().items():
        name = thread_id
        for thread in threading.enumerate():
            if thread.ident == thread_id:
                name = thread.name
        if name == "MainThread":
            continue
        log_message(None, "WARNING: Waiting for thread %s" %str(name))
        log_message(None, str(traceback.extract_stack(frame)))
        still_remain_thread = True
    if still_remain_thread:
        os._exit(0)
    else:
        sys.exit(0)
