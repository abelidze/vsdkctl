#!/bin/sh

# Guidelines
# Command line arguments
# clean.sh [Parameter 1]
# + Parameter 1: Whether using Synopsys (tct) or NXP Compiler (nxp) -> COMPILER
# + Parameter 2: Pass to the path that the directory want to clean.
#
# Notes:
# + Necessary APU compiler is installed
# + APU_TOOLS variable is exported

# Prefixes
STD_PREFIX="[ADAS]"
E_PREFIX="[ADAS] Error:"
W_PREFIX="[ADAS] Warning:"

COMPILER=$1
APP_DIR=$2

echo "$STD_PREFIX $0 $1 $2"
echo "$STD_PREFIX Start clean folder: $1"

if [ "$COMPILER" = "tct" ]
then
    # clean tct
	for tct in `find $APP_DIR -name "*build-apu-tct-sa-d*" -type d`
	do
	    echo "$STD_PREFIX Path=$tct";
	    find $tct -name "*.*" -type f -delete
	done
elif [ "$COMPILER" = "nxp" ]
then
    # clean nxp
	for nxp in `find $APP_DIR -name "*build-apu-nxp-sa-d*" -type d`
	do
	    echo "$STD_PREFIX Path=$nxp";
	    find $nxp -name "*.*" -type f -delete
	done
elif [ "$COMPILER" = "all" ]
then
	# clean tct
	for tct in `find $APP_DIR -name "*build-apu-tct-sa-d*" -type d`
	do
	    echo "$STD_PREFIX Path=$tct";
	    find $tct -name "*.*" -type f -delete
	done

	# clean nxp
	for nxp in `find $APP_DIR -name "*build-apu-nxp-sa-d*" -type d`
	do
	    echo "$STD_PREFIX Path=$nxp";
	    find $nxp -name "*.*" -type f -delete
	done
else
    echo "$E_PREFIX APU COMPILER is undefined!"
    echo "$STD_PREFIX Syntax: graphs.sh [tct/nxp]!"
    echo ""
    exit 1
fi

# clean other file unnecessary
find $APP_DIR -name "*.exe" -type f -delete
find $APP_DIR -name "*.o" -type f -delete
find $APP_DIR -name "*.a" -type f -delete
find $APP_DIR -name "*.d" -type f -delete
find $APP_DIR -name "*.el" -type f -delete
find $APP_DIR -name "*.elf" -type f -delete
find $APP_DIR -name "*.map" -type f -delete
find $APP_DIR -name "*.log" -type f -delete
find $APP_DIR -name "*.obj" -type f -delete
find $APP_DIR -name "*.gcda" -type f -delete
find $APP_DIR -name "*.gcno" -type f -delete

echo "Clean all done"
exit
