import os
import sys
import re
import paramiko
from utils import *


class SSHConnection:
    _server_name = ''
    _user_name = ''
    _pass_word = ''
    _runner = None
    _root_folder = '~'
    _buffer = ['']
    
    def __init__(self, servername, username, password, rootfolder):
        self._server_name = servername
        self._user_name = username
        self._pass_word = password
        self._root_folder = rootfolder
    
    def open(self):
        self._runner = paramiko.SSHClient()
        self._runner.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self._runner.connect(hostname=self._server_name, username=self._user_name, password=self._pass_word, timeout=5)
        return True

    def close(self):
        self._buffer = None
        self._runner.close()
        return True

    def exec_cmd(self, cmd, outp=None, isPrint=False, timeout=300):
        stdin, stdout, stderr = self._runner.exec_command(cmd, timeout=timeout)
        status = True
        cmd_out = stdout.readlines()
        cmd_err = stderr.readlines()
        if(cmd_err):
            cmd_err= ''.join(cmd_err)
            if(isPrint):
                log_message(None, "ERROR:")
                log_message(None, cmd_err)
            if outp is not None:
                outp[0] = cmd_err
                status = False

        if(cmd_out):
            cmd_out= ''.join(cmd_out)
            if(isPrint):
                log_message(None, "Output:")
                log_message(None, cmd_out)
            if outp is not None:
                outp[0] += cmd_out
        return status

    def executeCmd(self, cmd, timeout):
        ret = False
        cmd = 'cd %s; %s' %(self._root_folder, cmd)
        #log_message(None, "Executing %s ..." % cmd, 2)
        ret = self.exec_cmd(cmd, self._buffer, False, timeout)
        # Print the returned message to the console in case of command error
        if not ret:
            log_message(None, self._buffer[0].strip(), 1)
        return ret
        
    def waitFor(self, msg, timeout):
        ret = False
        logBuffer = self._buffer[0]
        if msg in logBuffer:
            ret = True
        else:
            # Match the message: ignore case, from multiple lines, newline is 
            # treated as a normal character and can be matched by '.'
            pattern = re.compile(r'%s' %msg, re.I | re.M | re.DOTALL)
            if re.search(pattern, logBuffer):
                # Split the log by pattern. Then update the buffer
                splited = re.split(pattern, logBuffer)
                self._buffer[0] = ''.join(splited)
                ret = True
        return ret
    
    def getBuffer(self):
        return self._buffer[0].strip()

    def clearBuffer(self):
        self._buffer = ['']


