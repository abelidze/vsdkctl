import os
import sys
import yaml
import openpyxl, pandas
from config import config
from lib.ExcelWriter import ExcelWriter
from utils import log_message, get_column_char

class ReportGenerator(object):
    """docstring for ReportGenerator"""
    result_columns1 = ['No', 'Test Case', 'Brief Description', 'Test Suite']
    result_columns2 = ['Hardware', 'Ticket ID', 'Error Description', 'Comment']
    summary_rows = ['Total# of developed test cases', 'Total# of tests run this run', 'Total# of passed this run',
                    'Total# of failed this run', 'Percentage passed of this run', 'Percentage passed of all tests']
    def __init__(self, module=None, testResult=None, testDesc=None, outDir=None, hwConfig=None):
        super(ReportGenerator, self).__init__()
        self.module = module

        # 'result' is the dictionary to store the test result which was extracted from the summary log
        # result: board -> os -> [compiler if applied] -> target -> test_suite -> test_case:'failed'/'passed' [0/1]
        self.result = testResult

        # 'description' is the dictionary to store the description of the tests: 
        # description: description -> test_suite -> test_case:'the brief description of test case'
        self.description = testDesc['description']

        # 'hardware' is the dictionary to store the list of hardware being used for each test suite
        self.hardware = hwConfig

        # the report file to generate - used by self.writer and pandas.to_excel() method
        self.report = os.path.join(outDir, '%s_TestReport.xlsx' % module.upper())

        # the excel template to start from
        template = os.path.join(config['ROOT_DIR'], 'template', 'header.xlsx')
        # check if the file does exist
        if not os.path.isfile(template):
            log_message(None, "ERROR: The excel template file '%s' does not exist!" % template)
            sys.exit(1)
        # check if required sheets do exist
        sheets = ['Document Info', 'Summary']
        wb = openpyxl.load_workbook(template, read_only=True)
        ret = all(sheet in wb.sheetnames for sheet in sheets)
        if not ret:
            log_message(None, "ERROR: The template file must contain '%s' sheets!" % sheets)
            sys.exit(1)

        # the image file of the logo
        logo = os.path.join(config['ROOT_DIR'], 'template', 'logo.png')
        if not os.path.isfile(logo):
            log_message(None, "ERROR: The logo file '%s' does not exist!" % logo)
            sys.exit(1)

        # the release config file
        self.release_cfg = os.path.join(config['ROOT_DIR'], 'template', 'release_config.yml')
        if not os.path.isfile(self.release_cfg):
            log_message(None, "ERROR: The release configuration file '%s' does not exist!" % self.release_cfg)
            sys.exit(1)

        # the module config file
        self.module_cfg = os.path.join(config['ROOT_DIR'], 'template', 'module_config.yml')
        if not os.path.isfile(self.module_cfg):
            log_message(None, "ERROR: The module configuration file '%s' does not exist!" % self.module_cfg)
            sys.exit(1)

        # the excel writer to support filling contents to the report file
        self.writer = ExcelWriter(self.report, template, logo)

    def buildReleaseInfo(self):
        releaseInfo = None
        with open(self.release_cfg, 'r') as fh:
            releaseInfo = yaml.load(fh)
        with open(self.module_cfg, 'r') as fh:
            releaseInfo.update(yaml.load(fh))
        if self.module.upper() not in releaseInfo:
            log_message(None, "ERROR: The module '%' is not defined in file '%s'!" % (self.module.upper(), self.module_cfg))
        return releaseInfo

    def buildCore(self):
        # From result, extract the core information: boards, OSes, compilers (if any), targets
        core = {}

        # The result first starts with 
        core['board'] = sorted(self.result.keys())
        # Assume that the tests were built/executed with/on the same number of compilers, targets
        # So we just take the sample result from one of them
        osDict = self.result[core['board'][0]]
        core['os'] = sorted(osDict.keys())

        compDict = osDict[core['os'][0]]
        # The test result contains compilers info (for APEX modules, Demo tests)
        if 'nxp' in compDict or 'tct' in compDict:
            core['comp'] = sorted(compDict.keys())
            targetDict = compDict[core['comp'][0]]
        else:
            core['comp'] = []
            targetDict = compDict

        core['target'] = sorted(targetDict.keys())
        return core
   
    def buildTestDict(self):
        # Build a hash from the dictionary 'description': testDict[tc] = (tc, desc, ts)
        testDict = {}
        hwDict = {}
        for ts, tcDict in self.description.items():
            hwInfo = ''
            if self.hardware and ts in self.hardware:
                hwInfo = self.hardware[ts]

            for tc, desc in tcDict.items():
                testDict[tc] = (tc, desc, ts)
                hwDict[tc] = hwInfo
        return testDict, hwDict

    def buildResultData(self, board, testDict, hwDict, core):
        testResult = []
        testLog = {}
        # length = 1
        core_length = 1
        (compDict, targetDict, tsDict, tcDict) = (None, None, None, None)

        for os in core['os']:
            # compiler list is not empty
            if bool(core['comp']):
                core_length = len(core['os']) * len(core['comp']) * len(core['target'])
                compDict = self.result[board][os]
                for comp in core['comp']:
                    targetDict = compDict[comp]
                    for target in core['target']:
                        tsDict = targetDict[target]
                        for ts, tcDict in tsDict.items():
                            if tcDict:  # Prevent the crashed tests - there is no test cases' result
                                for tc, ret in tcDict.items():
                                    ret = "Passed" if ret == '1' else "Failed"
                                    if tc not in testLog:
                                        testLog[tc] = (ret,)
                                    else:
                                        testLog[tc] += (ret,)
                            else:
                                if ts in self.description and self.description[ts]:
                                    for tc in self.description[ts].keys():
                                        if tc not in testLog:
                                            testLog[tc] = ("Failed",)
                                        else:
                                            testLog[tc] += ("Failed",)
            else:
                # No compiler info
                #length = len(core['os']) * len(core['target'])
                core_length = len(core['os']) * len(core['target'])
                targetDict = self.result[board][os]
                for target in core['target']:
                    tsDict = targetDict[target]
                    for ts, tcDict in tsDict.items():
                        if tcDict: # Prevent the crashed tests - there is no test cases' result
                            for tc, ret in tcDict.items():
                                ret = "Passed" if ret == '1' else "Failed"
                                if tc not in testLog:
                                    testLog[tc] = (ret,)
                                else:
                                    testLog[tc] += (ret,)
                        else:
                            if ts in self.description and self.description[ts]:
                                for tc in self.description[ts].keys():
                                    ret = "Failed" # Fill 'Failed' for all crashed test cases
                                    if tc not in testLog:
                                        testLog[tc] = (ret,)
                                    else:
                                        testLog[tc] += (ret,)

        for tc in sorted(testDict.keys()):
            data = testDict[tc]
            # Append the test result
            if tc in testLog: # test case which has test log
                data += testLog[tc]
            else:
                data += ('',) * core_length

            # Append the hw information if any
            data += (hwDict[tc],)

            testResult.append(data)
        return testResult

    def buildSummaryData(self, core, refSheet, start_row, start_col):
        # row_titles = ['Total# of developed test cases', 'Total# of tests run this run', 'Total# of passed this run', \
        #        'Total# of failed this run', 'Percentage passed of this run', 'Percentage passed of all tests']
        data_rows = []
        # compiler list is not empty
        if bool(core['comp']):
            length = len(core['os']) * len(core['comp']) * len(core['target'])
        else:
            # No compiler info
            length = len(core['os']) * len(core['target'])

        a_row0 = [self.summary_rows[0]]
        a_row1 = [self.summary_rows[1]]
        a_row2 = [self.summary_rows[2]]
        a_row3 = [self.summary_rows[3]]
        a_row4 = [self.summary_rows[4]]
        a_row5 = [self.summary_rows[5]]
        # 1. Formula of 'Total# of developed test cases'
        # Count non-empty values from A1 to A1000 of reference sheet
        a_row0.extend(['=COUNT(\'%s\'!$A$1:$A$1000)' %refSheet] * length)

        for i in range(length):
            cur_col = start_col + 1 + i
            colName = get_column_char(cur_col)
            # 2. Fill formula of 'Total# of tests run this run' - A
            a_row1.append('=%s%s + %s%s' % (colName, start_row + 2, colName, start_row + 3))
            # 5. Fill formula of 'Percentage passed of this run'
            a_row4.append('=%s%s / (100 * %s%s)%s' % (colName, start_row + 2, colName, start_row + 1, '%'))
            # 6. Fill formula of 'Percentage passed of all tests'
            a_row5.append('=%s%s / (100 * %s%s)%s' % (colName, start_row + 2, colName, start_row, '%'))

        # 3. Calculate formula of 'Total# of passed this run'
        # 4. Calculate formula of 'Total# of failed this run'
        detail_start_col = len(self.result_columns1) + 1 # Test result in detail sheet starts from 'E' column
        for i in range(length):
            detail_cur_col = detail_start_col + i
            colName = get_column_char(detail_cur_col)
            a_row2.append('=COUNTIF(\'%s\'!%s1:%s1000, "Passed")' % (refSheet, colName, colName))
            a_row3.append('=COUNTIF(\'%s\'!%s1:%s1000, "Failed")' % (refSheet, colName, colName))

        # Add all calculated rows to data_rows
        data_rows.append(a_row0)
        data_rows.append(a_row1)
        data_rows.append(a_row2)
        data_rows.append(a_row3)
        data_rows.append(a_row4)
        data_rows.append(a_row5)
        return data_rows

    def fillColumnsTitle(self, core, start_row):
        # Using Pandas to store the columns' title.
        # In Pandas, they support to print merged cells to excel if the columns's name are stored in tuples

        # number of rows in header: 1 row for OS, 1 row for Target, (0|1) row for Compiler
        header_rows = 3 if bool(core['comp']) else 2
        data = {}
        data1 = {}
        extra = ('',) * (header_rows - 1)
        for title in self.result_columns1[1:]:
            data[(title, ) + extra] = ()

        for os in core['os']:
            if bool(core['comp']):
                for comp in core['comp']:
                    for target in core['target']:
                        data[(os, comp, target)] = ()
                        data1[(os, comp, target)] = ()
            else:
                for target in core['target']:
                    data[(os, target)] = ()
                    data1[(os, target)] = ()
        for title in self.result_columns2:
            data[(title, ) + extra] = ()

        ### Write column title of detailed result sheet
        # first part of columns
        column = self.result_columns1[1:]
        # Append columns for tested OSes
        column.extend(core['os'])
        # last part of columns
        column.extend(self.result_columns2)
        df = pandas.DataFrame(data)
        df1 = pandas.DataFrame(data1)
        # Keep the order of columns as expected. To prevent Pandas from sorting them automatically
        df = df[column]
        writer = pandas.ExcelWriter(self.report, engine='openpyxl')
        writer.book = openpyxl.load_workbook(self.report)
        writer.sheets = dict((ws.title, ws) for ws in writer.book.worksheets)

        for (i, board) in enumerate(core['board']):
            # Write columns' title in detail sheet
            df.to_excel(writer, board, startrow=1, startcol=0)
            # Write columns' title in summary sheet
            df1.to_excel(writer, "Summary", startrow=start_row-1, startcol=1)
            start_row += len(self.summary_rows) + (i + 1) * header_rows
        writer.save()

    def generate(self):
        releaseInfo = self.buildReleaseInfo()
        core = self.buildCore()
        testDict, hwDict = self.buildTestDict()
        self.writer.open()
        self.writer.fillReleaseInfo(releaseInfo, self.module.upper())

        # Generate report
        # The data is flushed to the report first, using framework's ExcelWriter (in lib/)
        header_rows = 3 if bool(core['comp']) else 2
        start_row = 5 if (header_rows == 3) else 4
        if bool(core['comp']):
            num_of_target_columns = len(core['os']) * len(core['comp']) * len(core['target'])
        else:
            num_of_target_columns = len(core['os']) * len(core['target'])
        # For each board information, create the corespondent sheet and fill the test result
        for board in core['board']:
            data = self.buildResultData(board, testDict, hwDict, core)
            self.writer.fillResult(board, data, start_row)
            self.writer.formatResultTable(board, num_of_target_columns, len(data), start_row)
            self.writer.formatResultTitle(board, num_of_target_columns, 2, start_row - 1)
        # Close the report to save these sheets before calculating formulas for summary table
        self.writer.close()

        self.writer.open()
        # write the summary info starting from row #24
        start_row = 24
        row = start_row
        for (i, board) in enumerate(core['board']):
            data = self.buildSummaryData(core, board, row, 2) # 2 - the table starts from column B
            self.writer.fillSummaryDetail(data, row, 2)
            self.writer.formatSummaryTitle(num_of_target_columns, row - header_rows, row - 1)
            self.writer.formatSummaryDetail(num_of_target_columns, row, 2)
            row += len(self.summary_rows) + (i + 1) * header_rows
        self.writer.close()

        # In the end, fill the titles of result sheet using Pandas
        self.fillColumnsTitle(core, start_row - header_rows)
        

