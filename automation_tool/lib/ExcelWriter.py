import re
import openpyxl
from openpyxl.styles.differential import DifferentialStyle
from openpyxl.formatting.rule import Rule
from shutil import copyfile
from utils import log_message, get_column_char

class ExcelWriter(object):
    _blueFill = openpyxl.styles.fills.PatternFill(start_color='8DB4E2', end_color='8DB4E2', fill_type='solid')
    _failFill = openpyxl.styles.fills.PatternFill(start_color='FFCCCC', end_color='FFCCCC', fill_type='solid')
    _passFill = openpyxl.styles.fills.PatternFill(start_color='95DDB7', end_color='95DDB7', fill_type='solid')
    _untestFill = openpyxl.styles.fills.PatternFill(start_color='FEEDA4', end_color='FEEDA4', fill_type='solid')
    _failFont = openpyxl.styles.Font(color='A50021')
    _passFont = openpyxl.styles.Font(color='006600')
    _redBold = openpyxl.styles.Font(color='A50021', bold=True)
    _greenBold = openpyxl.styles.Font(color='006600', bold=True)
    _untestFont = openpyxl.styles.Font(color='974706')
    _boldFont = openpyxl.styles.Font(bold=True)
    _arialFont = openpyxl.styles.Font(name='Arial', size=10)
    _thSide = openpyxl.styles.Side(style='thin')
    _thBorder = openpyxl.styles.Border(
            left=_thSide, right=_thSide,
            top=_thSide, bottom=_thSide
        )
    result_columns1 = ['No', 'Test Case', 'Brief Description', 'Test Suite']
    result_columns2 = ['Hardware', 'Ticket ID', 'Error Description', 'Comment']

    def __init__(self, reportFile, tmplheader, logo):
        super(ExcelWriter, self).__init__()
        # Create the report file from the excel template
        try:
            copyfile(tmplheader, reportFile)
        except IOError:
            log_message(None, "The '%s' exists and is being opened. Please close to rewrite!" %reportFile)
            exit(1)

        self.wb = None
        self.logo = logo
        self.reportFile = reportFile
        
    def open(self):
        self.wb = openpyxl.load_workbook(self.reportFile)

    def close(self):
        self.wb.save(self.reportFile)

    def fillReleaseInfo(self, release_info, module):
        ws = self.wb['Document Info']
        img = openpyxl.drawing.image.Image(self.logo)
        ws.add_image(img, 'B2')
        # Replace required tags in range 'B6:E22'
        title = '%s %s %s Test Report' %(release_info['Company'], release_info['Platform Name'], release_info['Product Name'])
        for i in range(6, 23, 1):
            # consider the first cell
            cell = ws.cell(row=i, column=2)
            # if it is empty, skip the current line
            if not bool(cell.value):
                continue

            for j in range(2, 6, 1):
                cell = ws.cell(row=i, column=j)
                value = cell.value
                if bool(value):
                    value = re.sub('<TITLE>', title, value)
                    value = re.sub('<PRODUCT_NAME>', release_info['Product Name'].upper(), value)
                    value = re.sub('<RELEASE_VERSION>', release_info['Release Version'], value)
                    value = re.sub('<MODULE>', module, value)
                    value = re.sub('<Release_Date>', release_info['Release Date'], value)
                    value = re.sub('<Owner>', release_info[module]['Owner'], value)
                    value = re.sub('<Owner_Manager>', release_info['Owner Manager'], value)
                    value = re.sub('<QA>', release_info['QA Representative'], value)
                    value = re.sub('<Tester>', release_info[module]['Author'], value)
                    cell.value = value

        ws = self.wb['Summary']
        sw_version = '%s %s' % (release_info['Product Name'], release_info['Release Version'])
        # Replace required tags in range 'A1:E19'
        for i in range(1, 20, 1):
            # consider the first cell
            cell = ws.cell(row=i, column=1)
            # if it is empty, skip the current line
            if not bool(cell.value):
                continue

            for j in range(1, 6, 1):
                cell = ws.cell(row=i, column=j)
                value = cell.value
                if bool(value):
                    value = re.sub('<Product_Name>', release_info['Product Name'], value)
                    value = re.sub('<MODULE>', module, value)
                    value = re.sub('<Test_Date>', release_info['Test Date'], value)
                    value = re.sub('<Test_Purpose>', 'Verify %s' % module, value)
                    value = re.sub('<os_version>', release_info['PC Configuration'], value)
                    value = re.sub('<board_version>', release_info[module]['Board'], value)
                    value = re.sub('<chip_version>', release_info['Chip Version'], value)
                    value = re.sub('<comp_version>', release_info['Compiler Version'], value)
                    value = re.sub('<bsp_version>', release_info['Linux BSP Version'], value)
                    value = re.sub('<qnx_sdp_version>', release_info['QNX SDP Version'], value)
                    value = re.sub('<qnx_bsp_version>', release_info['QNX BSP Version'], value)
                    value = re.sub('<sw_version>', sw_version, value)
                    value = re.sub('<Tester>', release_info[module]['Author'], value)
                    if re.search('<apu_version>', value) is not None:
                        if 'APU Version' in release_info[module]:
                            value = re.sub('<apu_version>', release_info[module]['APU Version'], value)
                        else:
                            # Clear this line from the report
                            value = ''
                            cell_cleared = ws.cell(row=i, column=1)
                            cell_cleared.value = ''
                    cell.value = value
                
    def fillResult(self, sheet, data, start_row):
        # start writing from row=start_row
        # upper rows are reserved to print columns' title later using pandas
        idx = start_row
        self.wb.create_sheet(sheet)
        ws = self.wb[sheet]

        for (i, row_data) in enumerate(data):
            row_data = (i + 1,) + row_data
            # Write test result of each test case, start from row=idx, col=1
            self.writeRow(ws, idx, 1, row_data)
            idx += 1

    def formatResultTable(self, sheet, num_of_target_columns, data_len, start_row):
        ws = self.wb[sheet]
        freeze_cell = 'E%s' % start_row
        ws.freeze_panes = ws[freeze_cell]

        # Apply border for range of filled data from column 'No.' to last result column
        # Add 4 more columns for 'Hardware', 'Ticket ID', 'Error Description' and 'Comment' because
        # they were not counted in 'data'
        self.applyBorder(ws, self._thBorder, start_row, start_row + data_len - 1,
                         1, len(self.result_columns1) + num_of_target_columns + len(self.result_columns2))

        failed_dxf = DifferentialStyle(font=self._failFont, fill=self._failFill)
        passed_dxf = DifferentialStyle(font=self._passFont, fill=self._passFill)
        failed_rule = Rule(type="containsText", operator="containsText", text="Failed", dxf=failed_dxf)
        passed_rule = Rule(type="containsText", operator="containsText", text="Passed", dxf=passed_dxf)
        start_cell = 'E%s' % start_row
        end_col = get_column_char(len(self.result_columns1) + num_of_target_columns)
        end_cell = '%s%s' % (end_col, start_row + data_len)
        failed_rule.formula = ['NOT(ISERROR(SEARCH("Failed",%s)))' % start_cell]
        passed_rule.formula = ['NOT(ISERROR(SEARCH("Passed",%s)))' % start_cell]
        ws.conditional_formatting.add('%s:%s' % (start_cell, end_cell), failed_rule)
        ws.conditional_formatting.add('%s:%s' % (start_cell, end_cell), passed_rule)

    def formatResultTitle(self, sheet, num_of_target_columns, start_row, end_row):
        ws = self.wb[sheet]
        ## Column width adjustment and merge cell
        ws.column_dimensions['A'].width = 5
        ws.column_dimensions['B'].width = 20
        ws.column_dimensions['C'].width = 55
        ws.column_dimensions['D'].width = 20

        ## Merge cells
        ws.merge_cells('A%s:A%s' % (start_row, end_row))
        ws.merge_cells('B%s:B%s' % (start_row, end_row))
        ws.merge_cells('C%s:C%s' % (start_row, end_row))
        ws.merge_cells('D%s:D%s' % (start_row, end_row))

        # The result column (target) starts from column E
        cur_col = 5
        for i in range(num_of_target_columns):
            col_char = get_column_char(cur_col)
            ws.column_dimensions[col_char].width = 10
            cur_col += 1

        # The last 4 columns are 'Hardware', 'Ticket ID', 'Error Description', 'Comment'
        col_char = get_column_char(cur_col)
        ws.column_dimensions[col_char].width = 15
        ws.merge_cells('%s%s:%s%s' % (col_char, start_row, col_char, end_row))
        col_char = get_column_char(cur_col + 1)
        ws.column_dimensions[col_char].width = 15
        ws.merge_cells('%s%s:%s%s' % (col_char, start_row, col_char, end_row))
        col_char = get_column_char(cur_col + 2)
        ws.column_dimensions[col_char].width = 20
        ws.merge_cells('%s%s:%s%s' % (col_char, start_row, col_char, end_row))
        col_char = get_column_char(cur_col + 3)
        ws.column_dimensions[col_char].width = 15
        ws.merge_cells('%s%s:%s%s' % (col_char, start_row, col_char, end_row))

        # Fill color for header rows
        # column starts from 1, row starts from start_row
        num_of_col = len(self.result_columns1) + num_of_target_columns + len(self.result_columns2)
        num_of_row = end_row - start_row + 1
        for i in range(num_of_row):
            for j in range(num_of_col):
                cell = ws.cell(row=start_row + i, column=1+j)
                cell.fill = self._blueFill

    def fillSummaryDetail(self, data, start_row, start_col):
        ws = self.wb['Summary']
        row = start_row
        for row_data in data:
            self.writeRow(ws, row, start_col, row_data)
            # Apply bold font to the row title of summary detail
            cell = ws.cell(row=row, column=start_col)
            cell.font = self._boldFont
            row += 1
        alignment = openpyxl.styles.Alignment(horizontal='center')
        self.applyBorder(ws, self._thBorder, start_row, start_row + len(data) - 1, start_col, start_col + len(data[0]) - 1)
        self.applyAlignment(ws, alignment, start_row, start_row + len(data), start_col + 1, start_col + len(data[0]))

    def formatSummaryTitle(self, num_of_target_columns, start_row, end_row):
        ws = self.wb['Summary']
        ## Merge cells
        ws.merge_cells('B%s:B%s' % (start_row, end_row))

        # The summary result column (target) starts from column C
        # Fill color for title rows
        # column starts from 2, row starts from start_row
        num_of_col = 1 + num_of_target_columns
        num_of_row = end_row - start_row + 1
        for i in range(num_of_row):
            for j in range(num_of_col):
                cell = ws.cell(row=start_row + i, column=2 + j)
                cell.fill = self._blueFill

    def formatSummaryDetail(self, num_of_target_columns, start_row, start_col):
        ws = self.wb['Summary']
        for i in range(num_of_target_columns):
            cell = ws.cell(row=start_row, column=start_col + 1 + i)
            cell.font = self._boldFont
            cell = ws.cell(row=start_row + 1, column=start_col + 1 + i)
            cell.font = self._boldFont
            cell = ws.cell(row=start_row + 2, column=start_col + 1 + i)
            cell.font = self._greenBold
            cell = ws.cell(row=start_row + 3, column=start_col + 1 + i)
            cell.font = self._redBold
            cell = ws.cell(row=start_row + 4, column=start_col + 1 + i)
            cell.font = self._greenBold
            cell.number_format = openpyxl.styles.numbers.FORMAT_PERCENTAGE
            cell = ws.cell(row=start_row + 5, column=start_col + 1 + i)
            cell.font = self._greenBold
            cell.number_format = openpyxl.styles.numbers.FORMAT_PERCENTAGE

    def writeRow(self, ws, row, col, data):
        for (i, val) in enumerate(data):
            cell = ws.cell(row=row, column=col+i)
            cell.value = val

    def writeColumn(self, ws, row, col, data):
        for (i, val) in enumerate(data):
            cell = ws.cell(row=row + i, column=col)
            cell.value = val
            cell.font = self._txFont
            cell.alignment = openpyxl.styles.Alignment(horizontal='center', wrapText=True)
               
    def applyBorder(self, ws, border, start_row, end_row, start_col, end_col):
        start_col = get_column_char(start_col)
        end_col = get_column_char(end_col)
        cell_range = ws['%s%s:%s%s' %(start_col, start_row, end_col, end_row)]
        for row in cell_range:
            for cell in row:
                cell.border = border

    def applyAlignment(self, ws, alignment, start_row, end_row, start_col, end_col):
        start_col = get_column_char(start_col)
        end_col = get_column_char(end_col)
        cell_range = ws['%s%s:%s%s' %(start_col, start_row, end_col, end_row)]
        for row in cell_range:
            for cell in row:
                cell.alignment = alignment
