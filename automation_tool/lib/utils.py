#####
# Brief: Log message using file handler
# Input: filehdl - file to be writen
#        msg - message to be writen
#        level - number of 4-space in front of the message
# Note: Use filehdl=None in case of printing to the console
#####
def log_message(filehdl, msg, level=0):
    msg = level * '    ' + msg
    if filehdl is None:
        print msg
    else:
        filehdl.debug(msg)

def get_column_char(col_idx):
    # Convert column index (start from 1) to A-Z ASCII character
    if col_idx > 0 and col_idx < 27:
        return chr(col_idx + 64)
    else:
        log_message(None, "The column index is out of supported range A-Z")
        exit(1)
