import os
import sys
import logging
# Add root dir into sys.path
_ROOT_DIR = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
if _ROOT_DIR not in sys.path:
        sys.path.append(_ROOT_DIR)

import argparse
import yaml
from config import logger
from lib.utils import log_message
from lib import ReportGenerator

if __name__ == '__main__':
    formatter = logging.Formatter('%(message)s')
    file_handler = logging.FileHandler(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'report.sum.log'), mode='w')
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    
    # Argument parsing
    ex_msg = 'example: python report.py -l run.sum.yml -t desc.yml -m APEX -o output/reports -c template/hardware_config.yml'
    parser = argparse.ArgumentParser(epilog=ex_msg)
    parser.add_argument('-l', dest='sumLog', required=True, help='The result summary file')
    parser.add_argument('-t', dest='testDesc', required=True, help='Test cases description file')
    parser.add_argument('-m', dest='module', required=True, help='The module to generate report')
    parser.add_argument('-o', dest='outDir', help='The folder to store the report')
    parser.add_argument('-c', dest='hwConfig', help='The configuration file for hardware (camera) usage')
    args = parser.parse_args()

    # Check existance
    if not os.path.exists(args.sumLog):
        log_message(None, "ERROR: the result summary file '%s' does not exist!" % args.sumLog)
        sys.exit(1)
    if not os.path.exists(args.testDesc):
        log_message(None, "ERROR: the test cases description '%s' file does not exist!" % args.testDesc)
        sys.exit(1)
    if args.hwConfig:
        if not os.path.exists(args.hwConfig):
            log_message(None, "ERROR: the hardware configuration file '%s' does not exist!" % args.hwConfig)
            sys.exit(1)
    else:
        args.hwConfig = os.path.join(_ROOT_DIR, 'template', 'hardware_config.yml')

    if args.outDir:
        if not os.path.isabs(args.outDir):
            args.outDir = os.path.abspath(args.outDir)
        if not os.path.exists(args.outDir):
            os.makedirs(args.outDir)
    else:
        args.outDir = _ROOT_DIR

    summaryLog = None
    testDesc = None
    hwConfig = None
    # Extract the test result
    with open(args.sumLog, 'r') as fh:
        summaryLog = yaml.load(fh)
    # Extract the test description from tdecs.yml file
    with open(args.testDesc, 'r') as fh:
        testDesc = yaml.load(fh)
    with open(args.hwConfig, 'r') as fh:
        hwConfig = yaml.load(fh)

    # Check proper input
    if summaryLog is None or testDesc is None:
        log_message(None, "ERROR: The result summary file or test description file is empty!")
        sys.exit(1)
    if hwConfig is None:
        log_message(None, "ERROR: There is error in the hardware configuration file!")
        sys.exit(1)

    hardware = None
    if args.module.upper() in hwConfig:
        hardware = hwConfig[args.module.upper()]

    report = ReportGenerator(args.module, summaryLog, testDesc, args.outDir, hardware)
    report.generate()
