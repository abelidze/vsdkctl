#export PATH=/media/home/longlt3/adas/toolchain/aarch64-linux-gnu-bin-2015:/media/home/longlt3/adas/toolchain/arm-eabi-bin-2015:$PATH

__platform='tr'
__workspace='/media/host/ws'
__toolchain='6.3'
__apucomp='tct'

ARGS=$(getopt -o p:w:t:h -l "platform:,workspace:,toolchain:,help" -n "getopt.sh" -- "$@");

eval set -- "$ARGS";

while true; do
    case "$1" in
        -p|--platform)
            shift;
            if [ -n "$1" ]; then
                __platform=$1;
                shift;
            fi
        ;;

        -w|--workspace)
            shift;
            if [ -n "$1" ]; then
                __workspace=$1;
                shift;
            fi
        ;;

        -t|--toolchain)
            shift;
            if [ -n "$1" ]; then
                __toolchain=$1;
                shift;
            fi
        ;;

        -h|--help)
            echo "source ./export_path.sh [-p|--platform: qnx|tr] [-w|--workspace: /media/host/ws] [-t|--toolchain: 4.9|6.3]"
            exit 0;
        ;;

        --)
            shift;            break;
        ;;
    esac
done

# __BIN_SHARE=$PWD

export __TOOLS_ROOT=$PWD
export __TOOLCHAINS=${__TOOLS_ROOT}/toolchains
export __COVERITY_ROOT=${__TOOLCHAINS}/coverity

export LINUX_S32V234_DIR=${__workspace}/linux

if [[ $__platform == "qnx" ]]; then
    source ${__TOOLCHAINS}/qnx/qnx7/qnxsdp-env.sh
fi

if [[ ! -z "$QNX_BASE" ]]; then
    export SDK_ROOT=${__workspace}/qnx_vsdk/s32v234_sdk
else
    export SDK_ROOT=${__workspace}/tr_vsdk/s32v234_sdk
fi

export UBOOT_S32V234_DIR=${__workspace}/u-boot
export S32V234_SDK_ROOT=${SDK_ROOT}
export S32DS_VSDK_DIR=${SDK_ROOT}
export LINUX_ROOT=${LINUX_S32V234_DIR}
export CMA_32M_CONFIG=${SDK_ROOT}/os/linux/cma_32_config
export BUILDROOT_S32V234_DIR=/temp/s32v234
export APU_TOOLS=${__TOOLCHAINS}/apucc-root
export APU_COMP=${__apucomp}

export PATH=${__TOOLCHAINS}/apucc-root/bin:${__TOOLCHAINS}/target/aarch64-linux-gnu-bin:${__TOOLCHAINS}/target/arm-eabi-bin:${__TOOLCHAINS}/target/aarch64-elf-bin:${__COVERITY_ROOT}/bin:$PATH

cov-configure --comptype gcc --compiler aarch64-linux-gnu-gcc --template
cov-configure --comptype g++ --compiler aarch64-linux-gnu-g++ --template
# cov-configure --comptype gcc --compiler arm-eabi-gcc --template

export INTERMEDIATE_DIR="data";
export BUILD_DIR="build-v234ce-gnu-linux-d"
export COVERITY_HOST="coverity3.nxp.com";
export AUTH_KEY_FILE="${__TOOLS_ROOT}/coverity_key";
# export STRIP_PATH="${__BIN_SHARE}/stash";
# export LIST_OF_COMPONENTS="fdma h264enc";
# export COVERITY_STREAM="longlt3_so_trim"


export FPT_TOOL=$SDK_ROOT/tools/fpt/bin
export FPT_TESTS=$SDK_ROOT/tests/fpt/unittest/test_linux
export _CROSS="ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu-"

rm -f ${__TOOLCHAINS}/target;
ln -fs ${__TOOLCHAINS}/${__toolchain} ${__TOOLCHAINS}/target;
