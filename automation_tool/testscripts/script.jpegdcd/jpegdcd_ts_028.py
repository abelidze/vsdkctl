import re
from config import logger
from lib.utils import log_message
from lib import TestScript

class jpegdcd_ts_028(TestScript):
    _test_script = 'jpegdcd_ts_028'

    _input_files = {
        'jpegdcd_ts_028.elf'
    }

    _test_params = [
    ' '
                   ]
    _ret_tc = [1]

    _test_cases = [

        "jpegdcd_tc_100",

    ]

    def __init__(self, runner, tsReporter):
        self.runner = runner
        self.reporter = tsReporter
        super(jpegdcd_ts_028, self).__init__(runner, tsReporter)

    def run(self):
        timeout = 200
        ts_ret = True
        msg = "-------------------------------------------------------------------------"
        # Write result to summary run log file
        log_message(logger, '%s:' %(self._test_script))
        for param in range(0, len(self._test_params)):
            log_message(self.reporter, msg)
            cmd = './jpegdcd_ts_028.elf %s' %(self._test_params[param])
            log_message(self.reporter, "Executing %s..." % cmd)
            self.runner.executeCmd(cmd, timeout)
            for index in range(0, len(self._test_cases)):
                self._ret_tc[index] &= self.runner.waitFor(self._test_cases[index] + " : OK", timeout)

        for idx, ret in enumerate(self._ret_tc):
            if ret!= 1:
                ts_ret = False
            # Write result to summary run log file
            log_message(logger, '%s: \'%s\'' %(self._test_cases[idx], ret), 1)

        # Save running buffer to the log file of the test script
        log_message(self.reporter, self.runner.getBuffer())
        # Clear the buffer after saving
        self.runner.clearBuffer()
        return  ts_ret

    def preExecute(self):
        return True

    def postExecute(self):
        return True