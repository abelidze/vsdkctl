import re
from config import logger
from lib.utils import log_message
from lib import TestScript

class apexcv_ts_011(TestScript):
    _test_script = 'apexcv_ts_011'

    _input_files = {
        'apexcv_ts_011.elf'
    }

    _test_params = [
    'sanity '
                   ]
    _ret_tc = [1, 1, 1, 1, 1, 1, 1]

    _test_cases = [

        "apexcv_tc_1106",

        "apexcv_tc_1103",

        "apexcv_tc_1104",

        "apexcv_tc_1107",

        "apexcv_tc_1101",

        "apexcv_tc_1102",

        "apexcv_tc_1105",

    ]

    def __init__(self, runner, tsReporter):
        self.runner = runner
        self.reporter = tsReporter
        super(apexcv_ts_011, self).__init__(runner, tsReporter)

    def run(self):
        timeout = 10000
        ts_ret = True
        msg = "-------------------------------------------------------------------------"
        # Write result to summary run log file
        log_message(logger, '%s:' %(self._test_script))
        for param in range(0, len(self._test_params)):
            log_message(self.reporter, msg)
            cmd = './apexcv_ts_011.elf %s' %(self._test_params[param])
            log_message(self.reporter, "Executing %s..." % cmd)
            self.runner.executeCmd(cmd, timeout)
            for index in range(0, len(self._test_cases)):
                self._ret_tc[index] &= self.runner.waitFor(self._test_cases[index] + " : OK", timeout)

        for idx, ret in enumerate(self._ret_tc):
            if ret!= 1:
                ts_ret = False
            # Write result to summary run log file
            log_message(logger, '%s: \'%s\'' %(self._test_cases[idx], ret), 1)

        # Save running buffer to the log file of the test script
        log_message(self.reporter, self.runner.getBuffer())
        # Clear the buffer after saving
        self.runner.clearBuffer()
        return  ts_ret

    def preExecute(self):
        return True

    def postExecute(self):
        return True