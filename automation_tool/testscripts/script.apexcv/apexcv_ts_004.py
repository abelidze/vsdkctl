import re
from config import logger
from lib.utils import log_message
from lib import TestScript

class apexcv_ts_004(TestScript):
    _test_script = 'apexcv_ts_004'

    _input_files = {
        'apexcv_ts_004.elf'
    }

    _test_params = [
    'sanity '
                   ]
    _ret_tc =  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1]

    _test_cases = [
            "apexcv_tc_0443",
            "apexcv_tc_0497",
            "apexcv_tc_0451",
            "apexcv_tc_04102",
            "apexcv_tc_0476",
            "apexcv_tc_04150",
            "apexcv_tc_0466",
            "apexcv_tc_04151",
            "apexcv_tc_0465",
            "apexcv_tc_0421",
            "apexcv_tc_0499",
            "apexcv_tc_0438",
            "apexcv_tc_0423",
            "apexcv_tc_04139",
            "apexcv_tc_0437",
            "apexcv_tc_0496",
            "apexcv_tc_04146",
            "apexcv_tc_0494",
            "apexcv_tc_0434",
            "apexcv_tc_0444",
            "apexcv_tc_0453",
            "apexcv_tc_0462",
            "apexcv_tc_0430",
            "apexcv_tc_0490",
            "apexcv_tc_0492",
            "apexcv_tc_04109",
            "apexcv_tc_04113",
            "apexcv_tc_0459",
            "apexcv_tc_0431",
            "apexcv_tc_0432",
            "apexcv_tc_04128",
            "apexcv_tc_0483",
            "apexcv_tc_04110",
            "apexcv_tc_0467",
            "apexcv_tc_0498",
            "apexcv_tc_0424",
            "apexcv_tc_0489",
            "apexcv_tc_0433",
            "apexcv_tc_04138",
            "apexcv_tc_04155",
            "apexcv_tc_0403",
            "apexcv_tc_0487",
            "apexcv_tc_0429",
            "apexcv_tc_0426",
            "apexcv_tc_0456",
            "apexcv_tc_04149",
            "apexcv_tc_0422",
            "apexcv_tc_0484",
            "apexcv_tc_04105",
            "apexcv_tc_04129",
            "apexcv_tc_0481",
            "apexcv_tc_04144",
            "apexcv_tc_04123",
            "apexcv_tc_0447",
            "apexcv_tc_0415",
            "apexcv_tc_0461",
            "apexcv_tc_0448",
            "apexcv_tc_04115",
            "apexcv_tc_0402",
            "apexcv_tc_0471",
            "apexcv_tc_0478",
            "apexcv_tc_0469",
            "apexcv_tc_04108",
            "apexcv_tc_0491",
            "apexcv_tc_0417",
            "apexcv_tc_0474",
            "apexcv_tc_04111",
            "apexcv_tc_0414",
            "apexcv_tc_0409",
            "apexcv_tc_0488",
            "apexcv_tc_0470",
            "apexcv_tc_04131",
            "apexcv_tc_04112",
            "apexcv_tc_0412",
            "apexcv_tc_0407",
            "apexcv_tc_04114",
            "apexcv_tc_0440",
            "apexcv_tc_04136",
            "apexcv_tc_04133",
            "apexcv_tc_0485",
            "apexcv_tc_0495",
            "apexcv_tc_0413",
            "apexcv_tc_04101",
            "apexcv_tc_0452",
            "apexcv_tc_04154",
            "apexcv_tc_0410",
            "apexcv_tc_0460",
            "apexcv_tc_0482",
            "apexcv_tc_04104",
            "apexcv_tc_04125",
            "apexcv_tc_04148",
            "apexcv_tc_04134",
            "apexcv_tc_0455",
            "apexcv_tc_0457",
            "apexcv_tc_0472",
            "apexcv_tc_04145",
            "apexcv_tc_0446",
            "apexcv_tc_0464",
            "apexcv_tc_04124",
            "apexcv_tc_0425",
            "apexcv_tc_04132",
            "apexcv_tc_04106",
            "apexcv_tc_0475",
            "apexcv_tc_0441",
            "apexcv_tc_04103",
            "apexcv_tc_0486",
            "apexcv_tc_0468",
            "apexcv_tc_0427",
            "apexcv_tc_0404",
            "apexcv_tc_04127",
            "apexcv_tc_0416",
            "apexcv_tc_0411",
            "apexcv_tc_0454",
            "apexcv_tc_0463",
            "apexcv_tc_04153",
            "apexcv_tc_0435",
            "apexcv_tc_04130",
            "apexcv_tc_04147",
            "apexcv_tc_04122",
            "apexcv_tc_0458",
            "apexcv_tc_0450",
            "apexcv_tc_04126",
            "apexcv_tc_0477",
            "apexcv_tc_0449",
            "apexcv_tc_0445",
            "apexcv_tc_04152",
            "apexcv_tc_04140",
            "apexcv_tc_0418",
            "apexcv_tc_04100",
            "apexcv_tc_0479",
            "apexcv_tc_0405",
            "apexcv_tc_0428",
            "apexcv_tc_04135",
            "apexcv_tc_0493",
            "apexcv_tc_0480",
            "apexcv_tc_0420",
            "apexcv_tc_04137",
            "apexcv_tc_0436",
            "apexcv_tc_0408",
            "apexcv_tc_0442",
            "apexcv_tc_04107",
            "apexcv_tc_0439",
            "apexcv_tc_0419",
            "apexcv_tc_0406",
            "apexcv_tc_0401",
            "apexcv_tc_0473",
            "apexcv_tc_04141"
    ]

    def __init__(self, runner, tsReporter):
        self.runner = runner
        self.reporter = tsReporter
        super(apexcv_ts_004, self).__init__(runner, tsReporter)

    def run(self):
        timeout = 10000
        ts_ret = True
        msg = "-------------------------------------------------------------------------"
        # Write result to summary run log file
        log_message(logger, '%s:' %(self._test_script))
        for param in range(0, len(self._test_params)):
            log_message(self.reporter, msg)
            cmd = './apexcv_ts_004.elf %s' %(self._test_params[param])
            log_message(self.reporter, "Executing %s..." % cmd)
            self.runner.executeCmd(cmd, timeout)
            for index in range(0, len(self._test_cases)):
                self._ret_tc[index] &= self.runner.waitFor(self._test_cases[index] + " : OK", timeout)

        for idx, ret in enumerate(self._ret_tc):
            if ret!= 1:
                ts_ret = False
            # Write result to summary run log file
            log_message(logger, '%s: \'%s\'' %(self._test_cases[idx], ret), 1)

        # Save running buffer to the log file of the test script
        log_message(self.reporter, self.runner.getBuffer())
        # Clear the buffer after saving
        self.runner.clearBuffer()
        return  ts_ret

    def preExecute(self):
        return True

    def postExecute(self):
        return True