import re
from config import logger
from lib.utils import log_message
from lib import TestScript

class apexcv_ts_001(TestScript):
    _test_script = 'apexcv_ts_001'

    _input_files = {
        'apexcv_ts_001.elf'
    }

    _test_params = [
    'sanity'
                   ]
    _ret_tc =  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1,
                ]

    _test_cases = [
        "apexcv_tc_0131",
        "apexcv_tc_0143",
        "apexcv_tc_0126",
        "apexcv_tc_0130",
        "apexcv_tc_0170",
        "apexcv_tc_01137",
        "apexcv_tc_01108",
        "apexcv_tc_0124",
        "apexcv_tc_0117",
        "apexcv_tc_0153",
        "apexcv_tc_0107",
        "apexcv_tc_01111",
        "apexcv_tc_01121",
        "apexcv_tc_0177",
        "apexcv_tc_01127",
        "apexcv_tc_0147",
        "apexcv_tc_01133",
        "apexcv_tc_0181",
        "apexcv_tc_0149",
        "apexcv_tc_01144",
        "apexcv_tc_01116",
        "apexcv_tc_0159",
        "apexcv_tc_01125",
        "apexcv_tc_0127",
        "apexcv_tc_01129",
        "apexcv_tc_0133",
        "apexcv_tc_01142",
        "apexcv_tc_0172",
        "apexcv_tc_0134",
        "apexcv_tc_0122",
        "apexcv_tc_01138",
        "apexcv_tc_0114",
        "apexcv_tc_0104",
        "apexcv_tc_0194",
        "apexcv_tc_0168",
        "apexcv_tc_0190",
        "apexcv_tc_0188",
        "apexcv_tc_0105",
        "apexcv_tc_0113",
        "apexcv_tc_01120",
        "apexcv_tc_01119",
        "apexcv_tc_0145",
        "apexcv_tc_01136",
        "apexcv_tc_0169",
        "apexcv_tc_0182",
        "apexcv_tc_0183",
        "apexcv_tc_0146",
        "apexcv_tc_01139",
        "apexcv_tc_0165",
        "apexcv_tc_0157",
        "apexcv_tc_0173",
        "apexcv_tc_01135",
        "apexcv_tc_0148",
        "apexcv_tc_0111",
        "apexcv_tc_01126",
        "apexcv_tc_0140",
        "apexcv_tc_0152",
        "apexcv_tc_01124",
        "apexcv_tc_0191",
        "apexcv_tc_0180",
        "apexcv_tc_01109",
        "apexcv_tc_0195",
        "apexcv_tc_0167",
        "apexcv_tc_01123",
        "apexcv_tc_01128",
        "apexcv_tc_01143",
        "apexcv_tc_0156",
        "apexcv_tc_0121",
        "apexcv_tc_0189",
        "apexcv_tc_0160",
        "apexcv_tc_0192",
        "apexcv_tc_0171",
        "apexcv_tc_0108",
        "apexcv_tc_0123",
        "apexcv_tc_0187",
        "apexcv_tc_0120",
        "apexcv_tc_01117",
        "apexcv_tc_01130",
        "apexcv_tc_01122",
        "apexcv_tc_0101",
        "apexcv_tc_0174",
        "apexcv_tc_01112",
        "apexcv_tc_0110",
        "apexcv_tc_0138",
        "apexcv_tc_01140",
        "apexcv_tc_01141",
        "apexcv_tc_0150",
        "apexcv_tc_0137",
        "apexcv_tc_01134",
        "apexcv_tc_0179",
        "apexcv_tc_0103",
        "apexcv_tc_0125",
        "apexcv_tc_0162",
        "apexcv_tc_0164",
        "apexcv_tc_0144",
        "apexcv_tc_01110",
        "apexcv_tc_0155",
        "apexcv_tc_0193",
        "apexcv_tc_01115",
        "apexcv_tc_0129",
        "apexcv_tc_0128",
        "apexcv_tc_0166",
        "apexcv_tc_0102",
        "apexcv_tc_0196",
        "apexcv_tc_0141",
        "apexcv_tc_0158",
        "apexcv_tc_0118",
        "apexcv_tc_0109",
        "apexcv_tc_0176",
        "apexcv_tc_01113",
        "apexcv_tc_0197",
        "apexcv_tc_0136",
        "apexcv_tc_01118",
        "apexcv_tc_0132",
        "apexcv_tc_0139",
        "apexcv_tc_0151",
        "apexcv_tc_0106",
        "apexcv_tc_0186",
        "apexcv_tc_01114",
        "apexcv_tc_0175",
        "apexcv_tc_0163",
        "apexcv_tc_0116",
        "apexcv_tc_0178",
        "apexcv_tc_0154",
        "apexcv_tc_0119",
        "apexcv_tc_0135",
        "apexcv_tc_0198",
        "apexcv_tc_0112",
        "apexcv_tc_0115",
        "apexcv_tc_0142",
        "apexcv_tc_0114"
    ]


    def __init__(self, runner, tsReporter):
        self.runner = runner
        self.reporter = tsReporter
        super(apexcv_ts_001, self).__init__(runner, tsReporter)

    def run(self):
        timeout = 10000
        ts_ret = True
        msg = "-------------------------------------------------------------------------"
        # Write result to summary run log file
        log_message(logger, '%s:' %(self._test_script))
        for param in range(0, len(self._test_params)):
            log_message(self.reporter, msg)
            cmd = './apexcv_ts_001.elf %s' %(self._test_params[param])
            log_message(self.reporter, "Executing %s..." % cmd)
            self.runner.executeCmd(cmd, timeout)
            for index in range(0, len(self._test_cases)):
                self._ret_tc[index] &= self.runner.waitFor(self._test_cases[index] + " : OK", timeout)

        for idx, ret in enumerate(self._ret_tc):
            if ret!= 1:
                ts_ret = False
            # Write result to summary run log file
            log_message(logger, '%s: \'%s\'' %(self._test_cases[idx], ret), 1)

        # Save running buffer to the log file of the test script
        log_message(self.reporter, self.runner.getBuffer())
        # Clear the buffer after saving
        self.runner.clearBuffer()
        return  ts_ret

    def preExecute(self):
        return True

    def postExecute(self):
        return True
