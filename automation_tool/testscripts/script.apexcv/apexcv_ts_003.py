import re
from config import logger
from lib.utils import log_message
from lib import TestScript

class apexcv_ts_003(TestScript):
    _test_script = 'apexcv_ts_003'

    _input_files = {
        'apexcv_ts_003.elf'
    }

    _test_params = [
    ' sanity'
                   ]
    _ret_tc =  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1]

    _test_cases = [

        "apexcv_tc_0359",

        "apexcv_tc_0338",

        "apexcv_tc_0341",

        "apexcv_tc_0350",

        "apexcv_tc_0361",

        "apexcv_tc_0321",

        "apexcv_tc_0309",

        "apexcv_tc_0339",

        "apexcv_tc_0302",

        "apexcv_tc_0336",

        "apexcv_tc_0337",

        "apexcv_tc_0357",

        "apexcv_tc_0330",

        "apexcv_tc_0355",

        "apexcv_tc_0369",

        "apexcv_tc_0351",

        "apexcv_tc_0308",

        "apexcv_tc_0331",

        "apexcv_tc_0368",

        "apexcv_tc_0301",

        "apexcv_tc_0320",

        "apexcv_tc_0332",

        "apexcv_tc_0323",

        "apexcv_tc_0316",

        "apexcv_tc_0307",

        "apexcv_tc_0366",

        "apexcv_tc_0304",

        "apexcv_tc_0317",

        "apexcv_tc_0326",

        "apexcv_tc_0311",

        "apexcv_tc_0306",

        "apexcv_tc_0312",

        "apexcv_tc_0334",

        "apexcv_tc_0358",

        "apexcv_tc_0335",

        "apexcv_tc_0318",

        "apexcv_tc_0327",

        "apexcv_tc_0349",

        "apexcv_tc_0310",

        "apexcv_tc_0328",

        "apexcv_tc_0370",

        "apexcv_tc_0346",

        "apexcv_tc_0303",

        "apexcv_tc_0319",

        "apexcv_tc_0345",

        "apexcv_tc_0343",

        "apexcv_tc_0352",

        "apexcv_tc_0340",

        "apexcv_tc_0315",

        "apexcv_tc_0356",

        "apexcv_tc_0344",

        "apexcv_tc_0342",

        "apexcv_tc_0371",

        "apexcv_tc_0322",

        "apexcv_tc_0314",

        "apexcv_tc_0324",

        "apexcv_tc_0325",

        "apexcv_tc_0329",

        "apexcv_tc_0333",

        "apexcv_tc_0313",

        "apexcv_tc_0354",

        "apexcv_tc_0367",

        "apexcv_tc_0348",

        "apexcv_tc_0360",

        "apexcv_tc_0305",

        "apexcv_tc_0347",

        "apexcv_tc_0362",

        "apexcv_tc_0353"

    ]

    def __init__(self, runner, tsReporter):
        self.runner = runner
        self.reporter = tsReporter
        super(apexcv_ts_003, self).__init__(runner, tsReporter)

    def run(self):
        timeout = 1000
        ts_ret = True
        msg = "-------------------------------------------------------------------------"
        # Write result to summary run log file
        log_message(logger, '%s:' %(self._test_script))
        for param in range(0, len(self._test_params)):
            log_message(self.reporter, msg)
            cmd = './apexcv_ts_003.elf %s' %(self._test_params[param])
            log_message(self.reporter, "Executing %s..." % cmd)
            self.runner.executeCmd(cmd, timeout)
            for index in range(0, len(self._test_cases)):
                self._ret_tc[index] &= self.runner.waitFor(self._test_cases[index] + " : OK", timeout)

        for idx, ret in enumerate(self._ret_tc):
            if ret!= 1:
                ts_ret = False
            # Write result to summary run log file
            log_message(logger, '%s: \'%s\'' %(self._test_cases[idx], ret), 1)

        # Save running buffer to the log file of the test script
        log_message(self.reporter, self.runner.getBuffer())
        # Clear the buffer after saving
        self.runner.clearBuffer()
        return  ts_ret

    def preExecute(self):
        return True

    def postExecute(self):
        return True
