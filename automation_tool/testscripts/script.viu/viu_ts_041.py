import re
from config import logger
from lib import TestScript
import datetime
import time

class viu_ts_041(TestScript):
    _test_script = 'viu_ts_041'

    _input_files = {
        'viu_ts_041.elf'
    }

    _test_params = [
        'viu0 '
                   ]
    _ret_tc = [1, 1]

    _test_cases = [

        "Viu_TC_134",
        "Viu_TC_135"

    ]

    def __init__(self, runner, tsReporter):
        self.runner = runner
        self.reporter = tsReporter
        super(viu_ts_041, self).__init__(runner, tsReporter)

    def run(self):
        logger.debug("Executing test script %s..." % self._test_script)
        timeout = 300
        ts_ret = True
        for param in range(0, len(self._test_params)):
            cmd = './viu_ts_041.elf %s' %(self._test_params[param])
            self.runner.executeCmd(cmd, timeout)
            for index in range(0, len(self._test_cases)):
                self._ret_tc[index] &= self.runner.waitFor(self._test_cases[index] + " : OK", timeout)

        for idx, ret in enumerate(self._ret_tc):
            if ret!= 1:
                ts_ret = False
            self.reporter.debug('%s:%s:%s' %(self._test_script, self._test_cases[idx], ret))
        if ts_ret == False:
            logger.debug('%s' %(self.runner.getBuffer()))
        self.runner.clearBuffer()
        return  ts_ret

    def preExecute(self):
        return True

    def postExecute(self):
        return True
