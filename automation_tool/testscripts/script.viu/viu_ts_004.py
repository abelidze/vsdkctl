import re
from config import logger
from lib import TestScript
import datetime
import time

class viu_ts_004(TestScript):
    _test_script = 'viu_ts_004'

    _input_files = {
        'viu_ts_004.elf'
    }

    _test_params = [
        'viu0',
        'viu1'
                   ]
    _ret_tc = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

    _test_cases = [

        "Viu_TC_013",
        "Viu_TC_014",
        "Viu_TC_015",
        "Viu_TC_016",
        "Viu_TC_017",
        "Viu_TC_018",
        "Viu_TC_019",
        "Viu_TC_020",
        "Viu_TC_021",
        "Viu_TC_022",
        "Viu_TC_023",
        "Viu_TC_024",
        "Viu_TC_025",
        "Viu_TC_026",
        "Viu_TC_027",
        "Viu_TC_028",
    ]

    def __init__(self, runner, tsReporter):
        self.runner = runner
        self.reporter = tsReporter
        super(viu_ts_004, self).__init__(runner, tsReporter)

    def run(self):
        logger.debug("Executing test script %s..." % self._test_script)
        timeout = 300
        ts_ret = True
        for param in range(0, len(self._test_params)):
            cmd = './viu_ts_004.elf %s' %(self._test_params[param])
            self.runner.executeCmd(cmd, timeout)
            for index in range(0, len(self._test_cases)):
                self._ret_tc[index] &= self.runner.waitFor(self._test_cases[index] + " : OK", timeout)

        for idx, ret in enumerate(self._ret_tc):
            if ret!= 1:
                ts_ret = False
            self.reporter.debug('%s:%s:%s' %(self._test_script, self._test_cases[idx], ret))
        if ts_ret == False:
            logger.debug('%s' %(self.runner.getBuffer()))
        self.runner.clearBuffer()
        return  ts_ret

    def preExecute(self):
        return True

    def postExecute(self):
        return True
