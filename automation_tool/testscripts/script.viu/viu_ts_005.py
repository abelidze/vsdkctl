import re
from config import logger
from lib import TestScript
import datetime
import time

class viu_ts_005(TestScript):
    _test_script = 'viu_ts_005'

    _input_files = {
        'viu_ts_005.elf'
    }

    _test_params = [
        'viu0',
        'viu1'
                   ]
    _ret_tc = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

    _test_cases = [

        "Viu_TC_029",
        "Viu_TC_030",
        "Viu_TC_031",
        "Viu_TC_032",
        "Viu_TC_033",
        "Viu_TC_034",
        "Viu_TC_035",
        "Viu_TC_036",
        "Viu_TC_037",
        "Viu_TC_038",
        "Viu_TC_039",
        "Viu_TC_040",
        "Viu_TC_041",
        "Viu_TC_042",
        "Viu_TC_043",
        "Viu_TC_044",
        "Viu_TC_045",
        "Viu_TC_046",
        "Viu_TC_047",
        "Viu_TC_048",

    ]

    def __init__(self, runner, tsReporter):
        self.runner = runner
        self.reporter = tsReporter
        super(viu_ts_005, self).__init__(runner, tsReporter)

    def run(self):
        logger.debug("Executing test script %s..." % self._test_script)
        timeout = 300
        ts_ret = True
        for param in range(0, len(self._test_params)):
            cmd = './viu_ts_005.elf %s' %(self._test_params[param])
            self.runner.executeCmd(cmd, timeout)
            for index in range(0, len(self._test_cases)):
                self._ret_tc[index] &= self.runner.waitFor(self._test_cases[index] + " : OK", timeout)

        for idx, ret in enumerate(self._ret_tc):
            if ret!= 1:
                ts_ret = False
            self.reporter.debug('%s:%s:%s' %(self._test_script, self._test_cases[idx], ret))
        if ts_ret == False:
            logger.debug('%s' %(self.runner.getBuffer()))
        self.runner.clearBuffer()
        return  ts_ret

    def preExecute(self):
        return True

    def postExecute(self):
        return True
