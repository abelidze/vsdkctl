import re
from config import logger
from lib import TestScript
import datetime
import time

class viu_ts_022(TestScript):
    _test_script = 'viu_ts_022'

    _input_files = {
        'viu_ts_022.elf'
    }

    _test_params = [
        'viu0',
        'viu1'
                   ]

    _ret_tc = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

    _test_cases = [

        "Viu_TC_088",
        "Viu_TC_089",
        "Viu_TC_090",
        "Viu_TC_091",
        "Viu_TC_092",
        "Viu_TC_093",
        "Viu_TC_094",
        "Viu_TC_095",
        "Viu_TC_096",
        "Viu_TC_097",
        "Viu_TC_098",
        "Viu_TC_099",
        "Viu_TC_100",
        "Viu_TC_101",
        "Viu_TC_102",
        "Viu_TC_103",
        "Viu_TC_104",
        "Viu_TC_105",
        "Viu_TC_106",
        "Viu_TC_107",

    ]

    def __init__(self, runner, tsReporter):
        self.runner = runner
        self.reporter = tsReporter
        super(viu_ts_022, self).__init__(runner, tsReporter)

    def run(self):
        logger.debug("Executing test script %s..." % self._test_script)
        timeout = 300
        ts_ret = True
        for param in range(0, len(self._test_params)):
            cmd = './viu_ts_022.elf %s' %(self._test_params[param])
            self.runner.executeCmd(cmd, timeout)
            for index in range(0, len(self._test_cases)):
                self._ret_tc[index] &= self.runner.waitFor(self._test_cases[index] + " : OK", timeout)

        for idx, ret in enumerate(self._ret_tc):
            if ret!= 1:
                ts_ret = False
            self.reporter.debug('%s:%s:%s' %(self._test_script, self._test_cases[idx], ret))
        if ts_ret == False:
            logger.debug('%s' %(self.runner.getBuffer()))
        self.runner.clearBuffer()
        return  ts_ret

    def preExecute(self):
        return True

    def postExecute(self):
        return True
