import re
from config import logger
from lib import TestScript
import datetime
import time

class cgd_ts_024(TestScript):
    _test_script = 'cgd_ts_024'

    _input_files = {
        'cgd_ts_024.elf'
    }

    _test_params = [
    ' '
                   ]
    _ret_tc = [1]

    _test_cases = [

        "cgd_tc_024",

    ]

    def __init__(self, runner, tsReporter):
        self.runner = runner
        self.reporter = tsReporter
        super(cgd_ts_024, self).__init__(runner, tsReporter)

    def run(self):
        logger.debug("Executing test script %s..." % self._test_script)
        timeout = 300
        ts_ret = True
        for param in range(0, len(self._test_params)):
            cmd = './cgd_ts_024.elf %s' %(self._test_params[param])
            self.runner.executeCmd(cmd, timeout)
            for index in range(0, len(self._test_cases)):
                self._ret_tc[index] &= self.runner.waitFor(self._test_cases[index] + " : OK", timeout)

        for idx, ret in enumerate(self._ret_tc):
            if ret!= 1:
                ts_ret = False
            self.reporter.debug('%s:%s:%s' %(self._test_script, self._test_cases[idx], ret))
        if ts_ret == False:
            logger.debug('%s' %(self.runner.getBuffer()))
        self.runner.clearBuffer()
        return  ts_ret

    def preExecute(self):
        return True

    def postExecute(self):
        return True
