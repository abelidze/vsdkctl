import re
from config import logger
from lib.utils import log_message
from lib import TestScript

class apu_ts_002(TestScript):
    _test_script = 'apu_ts_002'

    _input_files = {
        'apu_ts_002.elf'
    }

    _test_params = [
    ' '
                   ]
    _ret_tc = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

    _test_cases = [

        "apu_tc_0209",

        "apu_tc_0206",

        "apu_tc_0212",

        "apu_tc_0207",

        "apu_tc_0201",

        "apu_tc_0208",

        "apu_tc_0210",

        "apu_tc_0214",

        "apu_tc_0211",

        "apu_tc_0204",

        "apu_tc_0203",

        "apu_tc_0205",

        "apu_tc_0202",

        "apu_tc_0213"

    ]

    def __init__(self, runner, tsReporter):
        self.runner = runner
        self.reporter = tsReporter
        super(apu_ts_002, self).__init__(runner, tsReporter)

    def run(self):
        timeout = 200
        ts_ret = True
        msg = "-------------------------------------------------------------------------"
        # Write result to summary run log file
        log_message(logger, '%s:' %(self._test_script))
        for param in range(0, len(self._test_params)):
            log_message(self.reporter, msg)
            cmd = './apu_ts_002.elf %s' %(self._test_params[param])
            log_message(self.reporter, "Executing %s..." % cmd)
            self.runner.executeCmd(cmd, timeout)
            for index in range(0, len(self._test_cases)):
                self._ret_tc[index] &= self.runner.waitFor(self._test_cases[index] + " : OK", timeout)

        for idx, ret in enumerate(self._ret_tc):
            if ret!= 1:
                ts_ret = False
            # Write result to summary run log file
            log_message(logger, '%s: \'%s\'' %(self._test_cases[idx], ret), 1)

        # Save running buffer to the log file of the test script
        log_message(self.reporter, self.runner.getBuffer())
        # Clear the buffer after saving
        self.runner.clearBuffer()
        return  ts_ret

    def preExecute(self):
        return True

    def postExecute(self):
        return True