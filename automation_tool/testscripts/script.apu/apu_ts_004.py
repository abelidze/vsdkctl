import re
from config import logger
from lib.utils import log_message
from lib import TestScript
class apu_ts_004(TestScript):
    _test_script = 'apu_ts_004'

    _input_files = {
        'apu_ts_004.elf'
    }

    _test_params = [
    ' '
                   ]
    _ret_tc = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

    _test_cases = [

        "apu_tc_0420",

        "apu_tc_0411",

        "apu_tc_0421",

        "apu_tc_0418",

        "apu_tc_0402",

        "apu_tc_0407",

        "apu_tc_0401",

        "apu_tc_0417",

        "apu_tc_0413",

        "apu_tc_0415",

        "apu_tc_0416",

        "apu_tc_0412",

        "apu_tc_0404",

        "apu_tc_0406",

        "apu_tc_0408",

        "apu_tc_0405",

        "apu_tc_0414",

        "apu_tc_0409",

        "apu_tc_0419",

        "apu_tc_0410",

        "apu_tc_0403"

    ]

    def __init__(self, runner, tsReporter):
        self.runner = runner
        self.reporter = tsReporter
        super(apu_ts_004, self).__init__(runner, tsReporter)

    def run(self):
        timeout = 200
        ts_ret = True
        msg = "-------------------------------------------------------------------------"
        # Write result to summary run log file
        log_message(logger, '%s:' %(self._test_script))
        for param in range(0, len(self._test_params)):
            log_message(self.reporter, msg)
            cmd = './apu_ts_004.elf %s' %(self._test_params[param])
            log_message(self.reporter, "Executing %s..." % cmd)
            self.runner.executeCmd(cmd, timeout)
            for index in range(0, len(self._test_cases)):
                self._ret_tc[index] &= self.runner.waitFor(self._test_cases[index] + " : OK", timeout)

        for idx, ret in enumerate(self._ret_tc):
            if ret!= 1:
                ts_ret = False
            # Write result to summary run log file
            log_message(logger, '%s: \'%s\'' %(self._test_cases[idx], ret), 1)

        # Save running buffer to the log file of the test script
        log_message(self.reporter, self.runner.getBuffer())
        # Clear the buffer after saving
        self.runner.clearBuffer()
        return  ts_ret

    def preExecute(self):
        return True

    def postExecute(self):
        return True
