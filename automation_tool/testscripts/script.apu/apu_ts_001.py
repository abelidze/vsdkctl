import re
from config import logger
from lib.utils import log_message
from lib import TestScript

class apu_ts_001(TestScript):
    _test_script = 'apu_ts_001'

    _input_files = {
        'apu_ts_001.elf'
    }

    _test_params = [
    ' '
                   ]
    _ret_tc = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

    _test_cases = [

        "apu_tc_0111",

        "apu_tc_0107",

        "apu_tc_0113",

        "apu_tc_0119",

        "apu_tc_0130",

        "apu_tc_0104",

        "apu_tc_0105",

        "apu_tc_0125",

        "apu_tc_0101",

        "apu_tc_0109",

        "apu_tc_0110",

        "apu_tc_0102",

        "apu_tc_0114",

        "apu_tc_0112",

        "apu_tc_0120",

        "apu_tc_0117",

        "apu_tc_0118",

        "apu_tc_0123",

        "apu_tc_0108",

        "apu_tc_0103",

        "apu_tc_0122",

        "apu_tc_0124",

        "apu_tc_0126",

        "apu_tc_0116",

        "apu_tc_0127",

        "apu_tc_0129",

        "apu_tc_0121",

        "apu_tc_0128",

        "apu_tc_0106",

        "apu_tc_0115"

    ]

    def __init__(self, runner, tsReporter):
        self.runner = runner
        self.reporter = tsReporter
        super(apu_ts_001, self).__init__(runner, tsReporter)

    def run(self):
        timeout = 200
        ts_ret = True
        msg = "-------------------------------------------------------------------------"
        # Write result to summary run log file
        log_message(logger, '%s:' %(self._test_script))
        for param in range(0, len(self._test_params)):
            log_message(self.reporter, msg)
            cmd = './apu_ts_001.elf %s' %(self._test_params[param])
            log_message(self.reporter, "Executing %s..." % cmd)
            self.runner.executeCmd(cmd, timeout)
            for index in range(0, len(self._test_cases)):
                self._ret_tc[index] &= self.runner.waitFor(self._test_cases[index] + " : OK", timeout)

        for idx, ret in enumerate(self._ret_tc):
            if ret!= 1:
                ts_ret = False
            # Write result to summary run log file
            log_message(logger, '%s: \'%s\'' %(self._test_cases[idx], ret), 1)

        # Save running buffer to the log file of the test script
        log_message(self.reporter, self.runner.getBuffer())
        # Clear the buffer after saving
        self.runner.clearBuffer()
        return  ts_ret

    def preExecute(self):
        return True

    def postExecute(self):
        return True
