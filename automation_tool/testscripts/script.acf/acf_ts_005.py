import re
from config import logger
from lib import TestScript
import datetime
import time
from lib.utils import log_message

class acf_ts_005(TestScript):
    _test_script = 'acf_ts_005'

    _input_files = {
        'acf_ts_005.elf'
    }

    _test_params = [
    ' '
                   ]
    _ret_tc =  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1]

    _test_cases = [

        "acf_tc_0518",

        "acf_tc_0509",

        "acf_tc_0501",

        "acf_tc_0505",

        "acf_tc_0531",

        "acf_tc_0507",

        "acf_tc_0502",

        "acf_tc_0510",

        "acf_tc_0527",

        "acf_tc_0521",

        "acf_tc_0511",

        "acf_tc_0519",

        "acf_tc_0535",

        "acf_tc_0522",

        "acf_tc_0524",

        "acf_tc_0508",

        "acf_tc_0513",

        "acf_tc_0503",

        "acf_tc_0534",

        "acf_tc_0526",

        "acf_tc_0529",

        "acf_tc_0533",

        "acf_tc_0504",

        "acf_tc_0520",

        "acf_tc_0516",

        "acf_tc_0512",

        "acf_tc_0528",

        "acf_tc_0525",

        "acf_tc_0517",

        "acf_tc_0506",

        "acf_tc_0514",

        "acf_tc_0532",

        "acf_tc_0515",

        "acf_tc_0523",

        "acf_tc_0530",

    ]

    def __init__(self, runner, tsReporter):
        self.runner = runner
        self.reporter = tsReporter
        super(acf_ts_005, self).__init__(runner, tsReporter)

    def run(self):
        timeout = 300
        ts_ret = True
        msg = "-------------------------------------------------------------------------"
        # Write result to summary run log file
        log_message(logger, '%s:' %(self._test_script))
        for param in range(0, len(self._test_params)):
            log_message(self.reporter, msg)
            cmd = './acf_ts_005.elf %s' %(self._test_params[param])
            log_message(self.reporter, "Executing %s..." % cmd)
            self.runner.executeCmd(cmd, timeout)
            for index in range(0, len(self._test_cases)):
                self._ret_tc[index] &= self.runner.waitFor(self._test_cases[index] + " : OK", timeout)

        for idx, ret in enumerate(self._ret_tc):
            if ret!= 1:
                ts_ret = False
            # Write result to summary run log file
            log_message(logger, '%s: \'%s\'' %(self._test_cases[idx], ret), 1)

        # Save running buffer to the log file of the test script
        log_message(self.reporter, self.runner.getBuffer())
        # Clear the buffer after saving
        self.runner.clearBuffer()
        return  ts_ret

    def preExecute(self):
        return True

    def postExecute(self):
        return True
