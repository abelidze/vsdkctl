import re
from config import logger
from lib import TestScript
import datetime
import time
from lib.utils import log_message

class acf_ts_012(TestScript):
    _test_script = 'acf_ts_012'

    _input_files = {
        'acf_ts_012.elf'
    }

    _test_params = [
    ' '
                   ]
    _ret_tc = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

    _test_cases = [

        "acf_tc_1210",

        "acf_tc_1201",

        "acf_tc_1209",

        "acf_tc_1203",

        "acf_tc_1207",

        "acf_tc_1202",

        "acf_tc_1205",

        "acf_tc_1204",

        "acf_tc_1208",

        "acf_tc_1206",

    ]

    def __init__(self, runner, tsReporter):
        self.runner = runner
        self.reporter = tsReporter
        super(acf_ts_012, self).__init__(runner, tsReporter)

    def run(self):
        timeout = 300
        ts_ret = True
        msg = "-------------------------------------------------------------------------"
        # Write result to summary run log file
        log_message(logger, '%s:' %(self._test_script))
        for param in range(0, len(self._test_params)):
            log_message(self.reporter, msg)
            cmd = './acf_ts_012.elf %s' %(self._test_params[param])
            log_message(self.reporter, "Executing %s..." % cmd)
            self.runner.executeCmd(cmd, timeout)
            for index in range(0, len(self._test_cases)):
                self._ret_tc[index] &= self.runner.waitFor(self._test_cases[index] + " : OK", timeout)

        for idx, ret in enumerate(self._ret_tc):
            if ret!= 1:
                ts_ret = False
            # Write result to summary run log file
            log_message(logger, '%s: \'%s\'' %(self._test_cases[idx], ret), 1)

        # Save running buffer to the log file of the test script
        log_message(self.reporter, self.runner.getBuffer())
        # Clear the buffer after saving
        self.runner.clearBuffer()
        return  ts_ret

    def preExecute(self):
        return True

    def postExecute(self):
        return True
