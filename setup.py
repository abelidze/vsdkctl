import setuptools

with open('README.md', 'r') as fh:
    readme = fh.read()

setuptools.setup(
    name='vctl',
    version='1.0.0',
    author='Palchevskii Vitalii',
    author_email='palchevskiiv@fsoft.com.vn',
    description='Automation tool for VISION SDK',
    long_description=readme,
    long_description_content_type='text/markdown',
    package_dir={'': 'src'},
    packages=setuptools.find_packages(where='src'),
    package_data={
       'vctl': ['static/*', 'static/**/*'],
    },
    include_package_data=True,
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: No License',
        'Operating System :: OS Independent',
    ],
    entry_points={
        'console_scripts': [
            'vctl = vctl.main:main',
        ],
    },
    python_requires='>=3.5'
)
