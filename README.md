# Build tool for VISION SDK project

# Requirements:
- Python 3.4

# Latest modifications:

| Author             | Date D/M/Y |
|--------------------|------------|
| Nguyen Tri Hai     | 17/04/2018 |
| Dao Anh Xuyen      | 20/07/2018 |
| Palchevsky Vitaly  | 19/09/2019 |