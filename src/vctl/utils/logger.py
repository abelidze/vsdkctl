import logging
import sys
import os

SUCCESS_LVL = 21
STYLE_SUPPORT = sys.platform != 'win32' or os.environ.get('MSYSTEM') != None
logging.addLevelName(SUCCESS_LVL, "SUCCESS")

class CustomLogger(logging.Logger):
    def __init__(self, name, options={ }):
        super().__init__(name)
        self.options = options
        self.name = name
        self.errors = 0
        if STYLE_SUPPORT:
            self.format = '\033[90m%(asctime)s\033[0m [%(levelname)s] %(message)s'
        else:
            self.format = '%(asctime)s [%(levelname)s] %(message)s'
        self.formatter = logging.Formatter(self.format, '%Y-%m-%d %H:%M:%S')
        self.handler = logging.StreamHandler(sys.stderr)
        self.handler.setFormatter(self.formatter)
        self.handler.setLevel(logging.DEBUG)
        self.addHandler(self.handler)

    def debug(self, msg, *args, **kwargs):
        super().debug(msg, *args, **kwargs)

    def info(self, msg, *args, **kwargs):
        super().info(RichText(msg).blue, *args, **kwargs)

    def success(self, msg, *args, **kwargs):
        super().log(SUCCESS_LVL, RichText(msg).green, *args, **kwargs)

    def warn(self, msg, *args, **kwargs):
        super().warn(RichText(msg).yellow, *args, **kwargs)

    def error(self, msg, *args, **kwargs):
        self.errors += 1
        super().error(RichText(msg).red, *args, **kwargs)

    def critical(self, msg, *args, **kwargs):
        self.errors += 1
        super().critical(RichText(msg).bold.marroon, *args, **kwargs)
        sys.exit(1)


class RichText:
    STYLE = {
        'white': '\033[97m',
        'grey': '\033[90m',
        'red': '\033[91m',
        'blue': '\033[94m',
        'green': '\033[92m',
        'yellow': '\033[93m',
        'marroon': '\033[31m',
        'underline': '\033[4m',
        'italic': '\033[3m',
        'bold': '\033[1m'
    }
    END = '\033[0m'

    def __init__(self, source):
        self.text = source
        self.header = ''
        if isinstance(source, RichText):
            self.text = source.text
            self.header = source.header

    def __str__(self):
        if STYLE_SUPPORT:
            return '{}{}{}'.format(self.header, self.text, self.END)
        return '{}{}'.format(self.header, self.text)

    def __add__(self, other):
        return RichText(str(self) + str(other))

    def __radd__(self, other):
        return RichText(str(other) + str(self))

    def __getattr__(self, key):
        if not STYLE_SUPPORT:
            return self
        if key in self.STYLE:
            self.text = self.STYLE[key] + self.text
            return self
        raise AttributeError('Unknown styling method')
