from . import client
import paramiko
import logging
import time

class RemoteClient(client.RemoteClient):
    def __init__(self, host, user='', password='', port=22):
        self.log = logging.getLogger('main')
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.hostname = host if not user else '{}@{}:{}'.format(user, host, port)

    # Check connection
    def ping(self, timeout=5):
        self.log.critical('Ping is not implemented')

    # Execute remote command
    def execute(self, command, savelog=False, quite=False):
        log = ''
        ssh = None
        try:
            ssh = paramiko.SSHClient()
            ssh.load_system_host_keys()
            ssh.set_missing_host_key_policy(paramiko.WarningPolicy)
            ssh.connect(self.host,
                port=self.port,
                username=self.user,
                password=self.password)

            stdin, stdout, stderr = ssh.exec_command(command)
            for output in client.stdout(stdout):
                self.log.info('%s: %s', self.hostname, output.rstrip())
                log += output
        except:
            if not quite:
                raise
                # self.log.error('Remote command failed: {}'.format(0))
        finally:
            if ssh: ssh.close()
        return 0, log

    # Download data from remote source to local dest
    def download(self, source, dest):
        ssh, sftp = None, None
        try:
            ssh = paramiko.SSHClient()
            ssh.load_system_host_keys()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(self.host,
                port=self.port,
                username=self.user,
                password=self.password)

            sftp = ssh.open_sftp()
            sftp.get(source, dest)
        finally:
            if sftp: sftp.close()
            if ssh: ssh.close()

    # Upload data from local source to remote dest
    def upload(self, source, dest):
        ssh, sftp = None, None
        try:
            ssh = paramiko.SSHClient()
            ssh.load_system_host_keys()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(self.host,
                port=self.port,
                username=self.user,
                password=self.password)

            sftp = ssh.open_sftp()
            sftp.put(source, dest)
        finally:
            if sftp: sftp.close()
            if ssh: ssh.close()

    # Copy data from source to dest
    def copy(self, source, dest):
        ssh, sftp = None, None
        try:
            ssh = paramiko.Transport(self.host, self.port)
            ssh.connect(username=self.user, password=self.password)
            sftp = paramiko.SFTPClient.from_transport(ssh)
            sftp.get(source, dest)
        finally:
            if sftp: sftp.close()
            if ssh: ssh.close()
