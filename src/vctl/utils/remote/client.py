import logging

class RemoteClient:
    def __init__(self, host, user='', password='', port=22):
        self.log = logging.getLogger('main')
        self.host = host
        self.user = user
        self.port = port
        self.password = password

    # Check connection
    def ping(self, timeout=5):
        self.log.critical('Ping is not implemented')

    # Execute remote command
    def execute(self, command, savelog=False, quite=False):
        self.log.critical('Execute is not implemented')

    # Download data from remote source to local dest
    def download(self, source, dest):
        self.log.critical('Download is not implemented')

    # Upload data from local source to remote dest
    def upload(self, source, dest):
        self.log.critical('Upload is not implemented')

    # Copy data from source to dest
    def copy(self, source, dest):
        self.log.critical('Copy is not implemented')


def stdout(stream):
    while True:
        data = stream.readline()
        if data:
            yield data
        else:
            break
