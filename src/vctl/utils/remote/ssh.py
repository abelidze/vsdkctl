import subprocess
import logging
import time
import os

class RemoteClient:
    def __init__(self, host, user='', password='', port=22):
        self.log = logging.getLogger('main')
        self.host = host
        self.user = user
        self.port = port
        self.password = password
        self.hostname = host if not user else '{}@{}'.format(user, host)
        self.ssh = 'ssh -p {} {} -tt -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null {}'.format(
            port,
            '-o BatchMode=yes' if not password else '',
            self.hostname
        )
        self.scp = 'scp -r -P {}'.format(port)

    # Check connection
    def ping(self, timeout=5):
        start = time.time()
        while time.time() - start <= timeout:
            rc, log = self.execute('echo PING 2>&1', savelog=True, quite=True)
            if rc == 0 and log.find('PING') >= 0:
                self.log.debug('PONG!')
                return True
        self.log.warning('Remote endpoint is not responding')
        return False

    # Execute remote command
    def execute(self, command, savelog=False, quite=False):
        cmd = '{} "{}"'.format(self.ssh, command)
        rc, log = self.oscmd(cmd, savelog)
        if not quite:
            if rc == 0:
                self.log.debug('Remote command finished')
            else:
                self.log.error('Remote command failed: {}'.format(rc))
        return rc, log

    # Download data from remote source to local dest
    def download(self, source, dest):
        source = '{}:{}'.format(self.hostname, source)
        return self.copy(source, dest)

    # Upload data from local source to remote dest
    def upload(self, source, dest):
        dest = '{}:{}'.format(self.hostname, dest)
        return self.copy(source, dest)

    # Copy data from source to dest
    def copy(self, source, dest):
        self.log.debug('Copying from {} to {}'.format(source, dest))
        cmd = '{} {} {}'.format(self.scp, source, dest)
        rc, log = self.oscmd(cmd)
        if rc != 0:
            self.log.error('Copy failed: {}'.format(rc))
        return rc

    # Execute OS-dependent shell command
    def oscmd(self, cmd, savelog=False):
        log = ''
        print(cmd)
        process = subprocess.Popen(['ssh', '--help'],
            shell=True,
            env=os.environ,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT)
        if savelog:
            for output in stdout(process):
                output = output.decode('utf-8').rstrip()
                self.log.info('%s: %s', self.hostname, output)
                log += output
        else:
            for output in stdout(process):
                self.log.info('%s: %s', self.hostname, output.decode('utf-8').rstrip())
        return process.poll(), log


def stdout(process):
   while True:
    data = process.stdout.readline()
    if data:
        yield data
    else:
        break







    def make_workspace(self, aClean = 0):
        # clean workspace
        if aClean == 1:
            cmd = 'rm -r ' + self.workspace + '/'
            lRet = self.run(cmd)
            if lRet==0:
                logging.debug("Clean workspace: OK")
            else:
                print("Clean workspace fail:", lRet)
        cmd = 'mkdir -p ' + self.workspace
        # make workspace if it not exists
        lRet = self.run(cmd)
        if lRet==0:
            logging.debug("Make workspace: OK")
        else:
            print("Make workspace fail:", lRet)

    def get_file_from_remote(self, aFileSrc, aDest):
        cmd = 'scp -P' + self.port + ' ' + self.scp_workspace + aFileSrc + ' ' + aDest
        lRet, lLog, lErr = self.oscmd(cmd)
        if lRet==0:
            logging.debug("Get file from remote: OK")
        else:
            print("Get file from remote fail:", lErr)
        return lRet

    def put_file_to_remote(self, aFileSrc, aDest):
        cmd = 'scp -P' + self.port + ' ' + aFileSrc + ' ' + self.scp_workspace + aDest
        lRet, lLog, lErr = self.oscmd(cmd)
        if lRet==0:
            logging.debug("Put file to remote: OK")
        else:
            print("Copy file to remote fail:", lErr)
        return lRet

    def get_folder_from_remote(self, aFolderSrc, aDest):
        cmd = 'scp -P' + self.port + ' -r '+ self.scp_workspace + aFolderSrc + ' ' + aDest
        lRet, lLog, lErr = self.oscmd(cmd)
        if lRet==0:
            logging.debug("Get folder from remote: OK")
        else:
            print("Get folder from remote fail:", lErr)
        return lRet

    def put_folder_to_remote(self, aFolderSrc, aDest):
        cmd = 'scp -P' + self.port + ' -r '+ aFolderSrc + ' ' + self.scp_workspace + aDest
        lRet, lLog, lErr = self.oscmd(cmd)
        if lRet==0:
            logging.debug("Put folder to remote: OK")
        else:
            print("Copy folder to remote fail:", lErr)
        return lRet


# mytest = RemoteClient('10.0.0.3','xuyenda',password='88', workspace = 'tests2')
# # cmd_list = ['ls', 'pwd', 'cd vision', 'ls', 'pwd']
# cmd_list = ['pwd', 'ls']
# file_list = ['auto.py', 'test.py']
# # mytest.shell(cmd_list)
# mytest.check_connect(10)
# mytest.make_workspace(1)
# mytest.get_file_from_remote('test.log','../outputs')
# mytest.put_file_to_remote('../outputs/test3.log','./')
# mytest.put_folder_to_remote('../outputs/results/','./')
# mytest.get_folder_from_remote('results/*','./')