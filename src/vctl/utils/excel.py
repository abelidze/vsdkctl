from ..config import dotdict
from openpyxl.utils import get_column_letter
import openpyxl
import logging
import shutil
import re

class ExcelWriter:
    MAX_NESTED_COUNT = 3

    PASSED = 'Passed'
    FAILED = 'Failed'

    ALIGN_CENTER = openpyxl.styles.Alignment(vertical='center', horizontal='center')
    BLUE_FILL = openpyxl.styles.fills.PatternFill(start_color='8DB4E2', end_color='8DB4E2', fill_type='solid')
    FAIL_FILL = openpyxl.styles.fills.PatternFill(start_color='FFCCCC', end_color='FFCCCC', fill_type='solid')
    PASS_FILL = openpyxl.styles.fills.PatternFill(start_color='95DDB7', end_color='95DDB7', fill_type='solid')
    UNTEST_FILL = openpyxl.styles.fills.PatternFill(start_color='FEEDA4', end_color='FEEDA4', fill_type='solid')
    FAIL_FONT = openpyxl.styles.Font(color='A50021')
    PASS_FONT = openpyxl.styles.Font(color='006600')
    UNTEST_FONT = openpyxl.styles.Font(color='974706')
    RED_BOLD = openpyxl.styles.Font(color='A50021', bold=True)
    GREEN_BOLD = openpyxl.styles.Font(color='006600', bold=True)
    BOLD_FONT = openpyxl.styles.Font(bold=True)
    ARIAL_FONT = openpyxl.styles.Font(name='Arial', size=10)
    TH_SIDE = openpyxl.styles.Side(style='thin')
    TH_BORDER = openpyxl.styles.Border(left=TH_SIDE, right=TH_SIDE, top=TH_SIDE, bottom=TH_SIDE)
    PERCENTAGE = openpyxl.styles.numbers.FORMAT_PERCENTAGE

    def __init__(self, report, template):
        self.log = logging.getLogger('main')
        self.wb = None
        try:
            shutil.copyfile(template, report)
        except IOError:
            self.log.critical('The "%s" is busy. Please close to overwrite!', report)
        self.report = report
        self._report = report
        self.start = (1, 1)
        self.end = (1, 1)

    def __enter__(self):
        if self.wb is None or self.report != self._report:
            return self.open()
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.close()

    #region FILL

    # Add image to specified sheet at current start
    def fillImage(self, sheet, img):
        if not self.sheetExists(sheet):
            self.log.error('Can\'t find sheet with name `%s`', sheet)
            return self

        row, col = self.start
        cell = '{}{}'.format(get_column_letter(col), row)
        self.wb[sheet].add_image(openpyxl.drawing.image.Image(img), cell)
        return self

    # Replace tags in specified sheet on current range
    def fillInfo(self, sheet, info):
        if not self.sheetExists(sheet):
            self.log.error('Can\'t find sheet with name `%s`', sheet)
            return self

        if type(info) is dict:
            info = defaultdict(lambda: '', info)

        # Regular expression for matching and replacing constants
        regex = re.compile(r'(<([^<>\s]+)>)')

        srow, scol = self.start
        erow, ecol = self.end
        ws = self.wb[sheet]
        for i in range(srow, erow + 1):
            cell = ws.cell(row=i, column=scol)
            if not cell.value: continue
            for j in range(scol, ecol + 1):
                cell = ws.cell(row=i, column=j)
                value = cell.value
                if not value: continue

                k = 0 # Counter is important due to config.dotdict.__missing__ behavior
                while k < self.MAX_NESTED_COUNT and regex.search(value):
                    value = regex.sub(lambda x: '{{{}}}'.format(x.group(2)), value).format_map(info)
                    k += 1
                cell.value = value
        return self

    # Fill data represented as array of tuples
    def fillData(self, sheet, data, numerate=False):
        if self.wb is None:
            self.log.error('Can\'t fill info. No document opened.')
            return self

        if not self.sheetExists(sheet):
            self.wb.create_sheet(sheet)

        ws = self.wb[sheet]
        row, col = self.start
        for (i, rowData) in enumerate(data):
            if numerate:
                rowData = (i + 1,) + rowData
            self.writeRow(ws, row + i, col, rowData)
        return self

    #endregion
    #region FORMAT

    def formatTitle(self, sheet, title, color=BLUE_FILL, font=BOLD_FONT):
        if not self.sheetExists(sheet):
            self.log.error('Can\'t find sheet with name `%s`', sheet)
            return self

        ws = self.wb[sheet]
        for (i, row) in enumerate(self.rows()):
            line = title[i]
            for (j, col) in enumerate(self.columns()):
                # Set width or merge cells
                text, width = line[j]
                if width == 0:
                    firstCell = self.cell(row - 1, col)
                    lastCell = self.cell(row, col)
                    self.merge(ws, firstCell, lastCell)
                else:
                    ws.column_dimensions[get_column_letter(col)].width = width

                # Set style
                cell = ws.cell(row=row, column=col)
                cell.fill = color
                cell.font = font
                cell.alignment = self.ALIGN_CENTER
        return self

    def formatResultTable(self, sheet):
        if not self.sheetExists(sheet):
            self.log.error('Can\'t find sheet with name `%s`', sheet)
            return self

        ws = self.wb[sheet]
        currentRange = self.range()
        startCell = self.cell(*self.start)

        ws.freeze_panes = ws[startCell]

        passedDXF = openpyxl.styles.differential.DifferentialStyle(font=self.PASS_FONT, fill=self.PASS_FILL)
        failedDXF = openpyxl.styles.differential.DifferentialStyle(font=self.FAIL_FONT, fill=self.FAIL_FILL)
        passedRule = openpyxl.formatting.rule.Rule(type='containsText', operator='containsText', text=self.PASSED, dxf=passedDXF)
        failedRule = openpyxl.formatting.rule.Rule(type='containsText', operator='containsText', text=self.FAILED, dxf=failedDXF)

        passedRule.formula = ['NOT(ISERROR(SEARCH("{}",{})))'.format(self.PASSED, startCell)]
        failedRule.formula = ['NOT(ISERROR(SEARCH("{}",{})))'.format(self.FAILED, startCell)]

        ws.conditional_formatting.add(currentRange, passedRule)
        ws.conditional_formatting.add(currentRange, failedRule)
        return self

    def formatSummaryTable(self, sheet):
        if not self.sheetExists(sheet):
            self.log.error('Can\'t find sheet with name `%s`', sheet)
            return self

        ws = self.wb[sheet]
        # ws.merge_cells('{}:{}'.format(self.cell(*self.start), self.cell(self.end[0], self.start[1])))
        return self

    #endregion
    #region UTILS

    def open(self):
        self.wb = openpyxl.load_workbook(self.report)
        self._report = self.report
        return self

    def save(self):
        self.wb.save(self.report)
        return self

    def close(self):
        self.wb.save(self.report)
        self.wb = None
        return self

    def at(self, row, col):
        self.start = (row, col)
        return self

    def to(self, row, col):
        self.end = (row, col)
        return self

    def till(self, drow, dcol):
        self.end = (self.start[0] + drow - 1, self.start[1] + dcol - 1)
        return self

    def cell(self, row, col):
        return '{}{}'.format(get_column_letter(col), row)

    def columns(self):
        return range(self.start[1], self.end[1] + 1)

    def rows(self):
        return range(self.start[0], self.end[0] + 1)

    def range(self, cell1=None, cell2=None):
        if cell1 is None or cell2 is None:
            cell1 = self.cell(*self.start)
            cell2 = self.cell(*self.end)
        return '{}:{}'.format(cell1, cell2)

    def merge(self, ws, cell1=None, cell2=None):
        if type(ws) is str:
            if not self.sheetExists(ws):
                return self
            ws = self.wb[ws]
        ws.merge_cells(self.range(cell1, cell2))
        return self

    def writeRow(self, ws, row, col, data):
        for (i, val) in enumerate(data):
            cell = ws.cell(row=row, column=col+i)
            cell.value = val

    def writeColumn(self, ws, row, col, data):
        for (i, val) in enumerate(data):
            cell = ws.cell(row=row + i, column=col + 1)
            cell.value = val
            cell.font = self.TX_FONT
            cell.alignment = openpyxl.styles.Alignment(horizontal='center', wrapText=True)
               
    def applyStyle(self, ws, **kwargs):
        if type(ws) is str:
            if not self.sheetExists(ws):
                return self
            ws = self.wb[ws]
        cellRange = ws[self.range()]
        for row in cellRange:
            for cell in row:
                for k, v in kwargs.items():
                    setattr(cell, k, v)
        return self

    def sheetExists(self, sheet):
        if self.wb is None:
            self.log.error('Can\'t fill info. No document opened.')
            return False
        if not sheet in self.wb:
            return False
        return True

    #endregion


# Convert column index (start from 1) to A-Z ASCII character
# def getColumnChar(column):
#     if column > 0 and column < 27:
#         return chr(column + 64)
#     logging.critical('The column index is out of supported range A-Z')
#     exit(1)
