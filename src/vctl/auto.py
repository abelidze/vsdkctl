#!/usr/bin/python

import sys, getopt, os, shutil
import subprocess
import datetime
import logging
import timeit

sys.path.insert(0, './src')
from gen_report import gen_report
from build import Build
from remote_ssh import RemoteSSH

__start = timeit.default_timer()

logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
TODAY = datetime.date.today()
CURRENT_DIR = os.path.dirname(os.path.realpath(__file__))
OUT_DIR = CURRENT_DIR + "/outputs_" + str(TODAY)
SDK_ROOT = CURRENT_DIR + '/../../..'
TEST_DIR = SDK_ROOT + '/tests'
DEMOS_DIR = SDK_ROOT + '/demos'

def run_os_cmd(cmd):
    process = subprocess.Popen(cmd,
                               shell=True,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    (lLog, lErr) = process.communicate()
    lRet = process.returncode
    return lRet, lLog, lErr

def ts_out_name(module, platform):
    return ('upload_' + str(platform) + '_' + str(module))

def ts_out_full_dir(path, name):
    return (path + '/' + name + '/topics/sub_testsuite')

def ts_cmd(py_version, module, platform, out_dir):
    return (py_version + ' test_spec.py' + ' -m ' + str(module) + ' -o ' + str(out_dir) + ' -p ' + str(platform))
def make_folder_name(platform, os, installer, compiler, module, target):
    return (platform + '_' + os + '_' + installer + '_'+ compiler + '_'+ module + '_'+ target)
def rs_in_full_dir(path, name):
    return (path + '/' + name)
def main(argv):
    # Process argument
    # -------------------------------------------------------------
    str_help = "help:\n\
    -m | --module:  [option]\n\
                test_all -> All module from build to test report\n\
                test_apex_all -> All apex module from build to test report\n\
                test_isp_all -> All isp module from build to test report\n\
                demos -> All demos module from build to test report\n\
                yourmodule -> we can choose multi modules\n\
    -r | --report: [option] \n\
                all -> generate all report for all modules that were selected. From build to test report\n\
                template -> generate all report template for all modules that wre selected.\n\
                fill -> fill result to test report for all modules that were selected.\n\
    -e | --execute: [option] \n\
                all -> run all modules that were selected, from build to run. Default None\n\
                only -> run all modules that were selected, only run.\n\
    -b | --build: [option] \n\
                all -> compile and use make cleansub depend by installer bin or src\n\
                update -> compile do not use make cleansub\n\
                clean -> cleansub module\n\
    -t | --target:[require]  linux-o, linux-d, sa-d, sa-o, apu-nxp, apu-tct, x86-gnu-windows-d,x86-gnu-linux-d... default None\n\
    -p | --platform: [require] vsdk or vsky: default vsdk\n\
    -s | --os: [option] win or linux: default linux\n\
    -c | --compiler:[require] tct or nxp default nxp\n\
    -i | --installer: [require] src or bin default src \n\
    -x | --idir: [option] input directory to build \n\
    -y | --odir: [option] output workspace \n\
    -n | --hostname: [option] hostname to remote e.g 10.0.0.2 \n\
    -u | --username: [option] username to remote e.g root \n\
       | --port: [option] port to remote e.g 22 \n\
    -w | --workspace: [option] workspace on remote e.g tests \n\
    -v | --version: [option] version for python default: python \n\
    -d | --data: [option] copy data in this path \n\
       | --cleanremote: [option] yes or no \n\
       | --restype: [option] use result type xml or log, default xml \n\
    -h | --help"
    MODULE_LIST = ['apexcv', 'apu', 'acf', 'fdma', 'oal', 'sdi', 'h264enc', 'apex', 'cgd', 'h264dec', 'umat', 'viu', 'seq', 'jpegdcd', 'csi']
    gModule_apex = ['apexcv', 'apu', 'acf']
    gModule_isp = ['fdma', 'oal', 'sdi', 'h264enc', 'apex', 'cgd', 'h264dec', 'umat', 'viu', 'seq', 'jpegdcd', 'csi']
    gModule = []
    gReport = 'None'
    gExecute = 'None'
    gBuild = 'None'
    gInstaller = 'src'
    gPlatform = 'vsdk'
    gCompiler = 'nxp'
    gTarget =[]
    gIndir = 'None'
    gOutdir = 'None'
    gHostname = '10.0.0.3'
    gUsername = 'xuyenda'
    gPort = '22'
    gWorkspace = 'tests'
    gOs = 'linux'
    gPyversion = 'python'
    gDataPath = 'None'
    gCleanRemote = 'yes'
    gResType = 'xml'
    try:
        opts, args = getopt.getopt(argv,"hm:r:e:b:i:p:c:t:x:y:n:u:w:v:s:d:",\
            ["module=","report=","execute=","build=","installer=","platform=",\
            "compiler=","target=","idir=", "odir=","hostname=", "username=",\
            "workspace=", "version=", "os=", "data=", "cleanremote=", "port=", "restype="])
    except getopt.GetoptError:
        print(str_help)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print(str_help)
            sys.exit()
        elif opt in ("-m", "--module"):
            if arg == "test_apex_all":
                gModule += gModule_apex
            elif arg == "test_isp_all":
                gModule += gModule_isp
            elif arg == "test_all":
                gModule += gModule_isp + gModule_apex
            else:
                gModule.append(arg)
        elif opt in ("-r", "--report"):
            gReport = arg
            if gReport == 'all' or gReport == 'template' or gReport == 'fill':
                print("gReport=", gReport)
            else:
                print("Argparser gReport invalid")
                print(str_help)
                sys.exit()
        elif opt in ("-e", "--execute"):
            gExecute = arg
        elif opt in ("-b", "--build"):
            gBuild = arg
        elif opt in ("-i", "--installer"):
            gInstaller = arg
            if gInstaller == 'src' or gInstaller == 'bin':
                print("gInstaller=", gInstaller)
            else:
                print("Argparser gInstaller invalid")
                print(str_help)
                sys.exit()
        elif opt in ("-p", "--platform"):
            gPlatform = arg
        elif opt in ("-s", "--os"):
            gOs = arg
        elif opt in ("-c", "--compiler"):
            gCompiler = arg
        elif opt in ("-x", "--idir"):
            gIndir = arg
        elif opt in ("-y", "--odir"):
            gOutdir = arg
        elif opt in ("-n", "--hostname"):
            gHostname = arg
        elif opt in ("-u", "--username"):
            gUsername = arg
        elif opt in ("--port"):
            gPort = arg
        elif opt in ("-w", "--workspace"):
            gWorkspace = arg
        elif opt in ("-v", "--version"):
            gPyversion = arg
        elif opt in ("-d", "--data"):
            gDataPath = arg
        elif opt in ("--cleanremote"):
            if arg == 'yes' or arg == 'no':
                gCleanRemote = arg
            else:
                print("Argparser cleanremote invalid")
                print(str_help)
                sys.exit()
        elif opt in ("--restype"):
            if arg == 'xml' or arg == 'log':
                gResType = arg
            else:
                print("Argparser cleanremote invalid")
                print(str_help)
                sys.exit()
        elif opt in ("-t", "--target"):
            temp = arg
            if temp  == 'linux-d' or temp == 'linux_d':
                temp = 'linux_d'
            elif temp == 'linux-o' or temp == 'linux_o':
                temp = 'linux_o'
            elif temp == 'sa-d' or temp == 'sa_d':
                temp = 'sa_d'
            elif temp == 'sa-o' or temp == 'sa_o':
                temp = 'sa_o'
            elif temp == 'apu-nxp' or temp == 'apu_nxp':
                temp = 'apu_nxp'
            elif temp == 'apu-tct' or temp == 'apu_tct':
                temp = 'apu_tct'
            elif temp == 'x86-gnu-windows-d' or temp == 'x86_gnu_windows_d':
                temp = 'x86-gnu-windows-d'
            elif temp == 'x86-gnu-linux-d' or temp == 'x86_gnu_linux_d':
                temp = 'x86-gnu-linux-d'
            else:
                print("Argparser target invalid")
                print(str_help)
                sys.exit()
            gTarget.append(temp)
        else:
            print("argv invalid")
            print(str_help)
    # print information
    logging.info("gPlatform %s", str(gPlatform))
    logging.info("gOs %s", str(gOs))
    logging.info("gInstaller %s", str(gInstaller))
    logging.info("gCompiler %s", str(gCompiler))
    logging.info("gModule %s", str(gModule))
    logging.info("gTarget %s", str(gTarget))
    logging.info("gExecute %s", str(gExecute))
    logging.info("gPyversion %s", str(gPyversion))
    logging.info("gIndir %s", str(gIndir))
    logging.info("gOutdir %s", str(gOutdir))
    logging.info("gDataPath %s", str(gDataPath))
    logging.info("gHostname %s", str(gHostname))
    logging.info("gUsername %s", str(gUsername))
    logging.info("gPort %s", str(gPort))
    logging.info("gCleanRemote %s", str(gCleanRemote))
    logging.info("gResType %s", str(gResType))
    # -------------------------------------------------------------
    #  End process argument

    # Make output
    # -------------------------------------------------------------
    if gOutdir == 'None':
        gOutdir = OUT_DIR
    # builds folder
    builds_dir = gOutdir + "/builds"
    # gReports folder
    reports_dir = gOutdir + "/reports"
    # results folder
    results_dir = gOutdir + "/results"
    try:
        if not os.path.exists(builds_dir):
            os.makedirs(builds_dir)
        if not os.path.exists(reports_dir):
            os.makedirs(reports_dir)
        if not os.path.exists(results_dir):
            os.makedirs(results_dir)
    except OSError:
        print("Creation of the outputs directory failed")
    
    # -------------------------------------------------------------
    #  End Make output

    # Process Build
    # -------------------------------------------------------------
    # Check gBuild == 'all' or gBuild == 'update' or gBuild == 'clean'
    # Clean
    for mod in gModule:
        for tar in gTarget:
            log = "Start clean with module = " + mod +  " target = " + tar + " build = " + gBuild
            logging.debug(log)
            if gIndir == 'None':
                if str(MODULE_LIST).find(mod) >= 0:
                    gIndir = TEST_DIR
                else:
                    gIndir = SDK_ROOT
            out_dir_build = builds_dir + '/' + make_folder_name(gPlatform, gOs,gInstaller,gCompiler,mod,tar)
            if not os.path.exists(out_dir_build):
                os.makedirs(out_dir_build)
            build_clean = Build(gPlatform, gInstaller, gCompiler, mod, tar)            
            logging.debug(gIndir)
            logging.debug(out_dir_build)
            if gBuild == 'all':
                logging.debug("Start CleanTest")
                build_clean.CleanTest(gIndir)
            elif gBuild == 'clean':
                logging.debug("Start CleansubTest")
                build_clean.CleansubTest(gIndir)
            else:
                logging.info("Build no anything")
    # Build
    for mod in gModule:
        for tar in gTarget:
            log = "Start build with module = " + mod +  " target = " + tar + " build = " + gBuild
            logging.debug(log)
            if gIndir == 'None':
                if str(MODULE_LIST).find(mod) >= 0:
                    gIndir = TEST_DIR
                else:
                    gIndir = SDK_ROOT
            out_dir_build = builds_dir + '/' + make_folder_name(gPlatform, gOs,gInstaller,gCompiler,mod,tar)
            if not os.path.exists(out_dir_build):
                os.makedirs(out_dir_build)
            build = Build(gPlatform, gInstaller, gCompiler, mod, tar)            
            logging.debug(gIndir)
            logging.debug(out_dir_build)
            if gBuild == 'all' or gBuild == 'update':
                logging.debug("Start BuildTest")                
                build.BuildTest(gIndir, out_dir_build)
                if gDataPath != 'None':
                    cmd = 'cp -a ' + gDataPath + ' ' + out_dir_build
                    os.system(cmd)
            else:
                logging.info("Build no anything")
    # -------------------------------------------------------------
    #  End process Build

    # Process execute
    # -------------------------------------------------------------
    # Get name execute folder
    logging.info("Get name execute folder")
    if gExecute == 'all' or gExecute == 'only':
        folder_list = []
        scirpt_run_all = CURRENT_DIR + '/run_all_tests_linux.sh'
        scirpt_run_emb = CURRENT_DIR + '/emb.sh'
        ex_list_file = CURRENT_DIR + '/ex_list.txt'
        for mod in gModule:
            for tar in gTarget:
                folder = make_folder_name(gPlatform, gOs, gInstaller, gCompiler, mod, tar)
                folder_full_name = builds_dir + '/' + folder
                folder_list.append(folder_full_name)
                logging.debug(folder_full_name)


        # Copy to board
        # Check connect
        logging.info("Check connect")
        remotessh = RemoteSSH(gHostname,gUsername,password='88', workspace = gWorkspace, port = gPort)
        lRetRemote = 0
        for iter in range(1,10):
            lRetRemote = remotessh.check_connect(12)
            if lRetRemote == 0:
                logging.debug("Connect to remote OK")
                break;
        if lRetRemote != 0:
            print("Error: Can not connect to remote ")
            sys.exit()
        # make workspace if it not exist
        logging.info("make workspace if it not exist")
        if gCleanRemote == 'yes':
            remotessh.make_workspace(1)
        else:
            remotessh.make_workspace()
        # Start copy   
        logging.debug("Start copy...")     
        for folder in folder_list:
            shutil.copy(scirpt_run_emb, folder)
            shutil.copy(ex_list_file, folder)
            remotessh.put_folder_to_remote(folder,'./')
        # Copy script run all tests
        logging.debug("Copy script run all tests...")  
        remotessh.put_file_to_remote(scirpt_run_all, './')
        # Execute on board
        logging.debug("Execute on board...")  
        cmd = 'cd ~/' + gWorkspace + '; sh run_all_tests_linux.sh'
        logging.debug(cmd)
        remotessh.run_cmd(cmd)
        # Get result from board
        logging.debug("Get result from board...")  
        remotessh.get_folder_from_remote('test_results/*', results_dir)

    # -------------------------------------------------------------
    #  End process execute

    # Process report
    # -------------------------------------------------------------
    logging.debug("Process report...")  
    if gReport == 'all' or gReport == 'template':
        # Generate test spec take input for test report
        logging.debug("Generate test spec take input for test report...")  
        for mod in gModule:
            try:
                cmd = ts_cmd(gPyversion, mod, gPlatform, reports_dir)
                print(cmd)
                os.chdir(os.path.join(CURRENT_DIR, 'test_spec'))
                os.system(cmd)
                os.chdir(CURRENT_DIR)
                logging.debug("Generate test spec OK")
            except OSError:
                print("Generate test spec fail")
            else:
                out = ts_out_name(mod, gPlatform)
                logging.debug("Generate test spec done: Output", out)

        # Start generate report
        logging.debug("Start generate report...")
        for mod in gModule:
            temp_name = ts_out_name(mod, gPlatform)
            report_name = gPlatform + '_' + gOs + '_' + gInstaller + '_' + gCompiler + '_' + mod + '.xls'
            report_full_name = reports_dir + "/" + report_name
            logging.debug(report_full_name)
            f_report = gen_report(mod, report_full_name, "XuyenDao",resType = gResType)
            reports_dir_tmp = reports_dir
            reports_dir_tmp = ts_out_full_dir(reports_dir_tmp,temp_name)
            logging.debug(reports_dir_tmp)
            f_report.create_template(reports_dir_tmp)
        if gReport == 'all':
            # Fill report
            # Start generate report
            logging.debug("Start fill report...")
            for mod in gModule:
                for tar in gTarget:
                    temp_name = make_folder_name(gPlatform, gOs, gInstaller, gCompiler, mod, tar)
                    report_name = gPlatform + '_' + gOs + '_' + gInstaller + '_' + gCompiler + '_' + mod + '.xls'
                    report_full_name = reports_dir + "/" + report_name
                    logging.debug(report_full_name)
                    f_report = gen_report(mod, report_full_name, "XuyenDao",resType = gResType)
                    reports_dir_tmp = results_dir
                    reports_dir_tmp = rs_in_full_dir(reports_dir_tmp, temp_name)
                    logging.debug(reports_dir_tmp)
                    f_report.fill(tar, reports_dir_tmp)
    elif gReport == 'fill':
        # Fill report
        # Start generate report
        for mod in gModule:
            for tar in gTarget:
                temp_name = make_folder_name(gPlatform, gOs, gInstaller, gCompiler, mod, tar)
                report_name = gPlatform + '_' + gOs + '_' + gInstaller + '_' + gCompiler + '_' + mod + '.xls'
                report_full_name = reports_dir + "/" + report_name
                logging.debug(report_full_name)
                f_report = gen_report(mod, report_full_name, "XuyenDao",resType = gResType)
                reports_dir_tmp = results_dir
                reports_dir_tmp = rs_in_full_dir(reports_dir_tmp, temp_name)
                logging.debug(reports_dir_tmp)
                f_report.fill(tar, reports_dir_tmp)
    else:
        logging.debug("Do not generate report...")
    # -------------------------------------------------------------
    #  End process report

if __name__ == "__main__":

    main(sys.argv[1:])
    __stop = timeit.default_timer()
    print ("Total time = ",__stop - __start, "s")
