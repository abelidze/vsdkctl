from .utils.logger import CustomLogger
from .app import Application
import logging

def main():
    logging.setLoggerClass(CustomLogger)
    app = Application()
    app.run()
