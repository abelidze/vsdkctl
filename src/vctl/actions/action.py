from contextlib import contextmanager
import logging
import os

class Action:
    def __init__(self, options={ }):
        self.log = logging.getLogger('main')

    @staticmethod
    def setParser(parser):
        pass

    def run(self, workspace, *args, **kwargs):
        self.log.critical('Method run for chosen action is not implemented')


@contextmanager
def pushd(path):
    lastPath = os.getcwd()
    os.chdir(path)
    yield
    os.chdir(lastPath)
