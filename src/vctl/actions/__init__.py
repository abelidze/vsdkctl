from .build import Build
from .report import Report
from .junit import JUnit
from .run import Run

available = {
    'run': Run,
    'build': Build,
    'junit': JUnit,
    'report': Report
}

# __all__ = [ 'run', 'build', 'junit', 'report' ]