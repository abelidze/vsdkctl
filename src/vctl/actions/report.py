from .junit import JUnit
from .action import Action
from ..utils.excel import ExcelWriter
from .. import config
from collections import defaultdict
from junitparser import JUnitXml
from openpyxl import load_workbook
import logging
import shutil
import os
import re

env = os.environ.get

class Report(Action):
    help = 'EXCEL report generation'
    description = \
        'Report action provides a convenient for generating EXCEL files from your logs / JUnit reports\n' \
        'Usage examples:\n' \
        ' - vctl report $WORKSPACE -o myreport.xlsx\n' \
        ' - vctl report $WORKSPACE -t mytemplate.xlsx -o myreport.xlsx\n' \
        ' - vctl report $WORKSPACE -p "(apex|csi)" -o apex_and_csi_summary.xlsx'

    TITLE_PREFIX = [
        ('No', 5),
        ('Test Suite', 20),
        ('Test Case', 20),
        ('Brief Description', 55),
    ]

    TITLE_SUFFIX = [
        ('Hardware', 15),
        ('Ticket ID', 15),
        ('Error Description', 20),
        ('Comment', 15),
    ]

    INFO_SHEET = 'Document Info'
    SUMMARY_SHEET = 'Summary'

    # w - ExcelWriter, s - sheet, r - row, c - column. True / False - access results sheet
    SUMMARY = [
        ( 'Total# of developed test cases', True, { 'font': ExcelWriter.BOLD_FONT },
            lambda w, s, r, c: '=COUNT(\'{}\'!$A$1:$A$1000)'.format(s)
        ),
        ( 'Total# of tests run this run', False, { 'font': ExcelWriter.BOLD_FONT },
            lambda w, s, r, c: '={} + {}'.format(w.cell(r + 2, c), w.cell(r + 3, c))
        ),
        ( 'Total# of passed this run', True, { 'font': ExcelWriter.GREEN_BOLD },
            lambda w, s, r, c: '=COUNTIF(\'{}\'!{}:{}, "{}")'.format(s, w.cell(1, c), w.cell(1000, c), w.PASSED)
        ),
        ( 'Total# of failed this run', True, { 'font': ExcelWriter.RED_BOLD },
            lambda w, s, r, c: '=COUNTIF(\'{}\'!{}:{}, "{}")'.format(s, w.cell(1, c), w.cell(1000, c), w.FAILED)
        ),
        ( 'Percentage passed of this run', False, { 'font': ExcelWriter.GREEN_BOLD, 'number_format': ExcelWriter.PERCENTAGE },
            lambda w, s, r, c: '={} / (100 * {})%'.format(w.cell(r + 2, c), w.cell(r + 1, c))
        ),
        ( 'Percentage passed of all tests', False, { 'font': ExcelWriter.GREEN_BOLD, 'number_format': ExcelWriter.PERCENTAGE },
            lambda w, s, r, c: '={} / (100 * {})%'.format(w.cell(r + 2, c), w.cell(r, c))
        ),
    ]

    def __init__(self, options={ }):
        self.log = logging.getLogger('main')
        self.options = defaultdict(lambda: '', options)
        self.options['pattern'].replace('/', os.sep)
        self.pattern = re.compile('.*{}$'.format(self.options['pattern']), re.IGNORECASE)

    @staticmethod
    def setParser(parser):
        parser.add_argument('-p', '--pattern',
            dest='pattern',
            default=env('VCTL_PATTERN', '.*'),
            help='Pattern for matching report targets. \
                \nDefault: .* \
                \nENV: VCTL_PATTERN')
        parser.add_argument('-c', '--config',
            dest='config',
            default=env('VCTL_CONFIG', 'default'),
            help='Path to configuration file. \
                \nDefault: {WORKSPACE}/static/default.yml \
                \nENV: VCTL_CONFIG')
        parser.add_argument('-t', '--template',
            dest='template',
            default=env('VCTL_TEMPLATE', ''),
            help='Path to custom template for generated spreadsheet. \
                \nDefault: {WORKSPACE}/static/template.xlsx \
                \nENV: VCTL_TEMPLATE')
        parser.add_argument('-o', '--output',
            dest='output',
            default=env('VCTL_XLSX_REPORT', ''),
            help='Output path for EXCEL report file. \
                \nDefault: {WORKSPACE}/report.xlsx \
                \nENV: VCTL_XLSX_REPORT')
        parser.add_argument('--genspec',
            action='store_true',
            help='Generate spec from source code')

    # Run Report action
    def run(self, workspace):
        # Init and validate workspace
        self.initWorkspace(workspace)
        self.validate()
        output = os.path.join(self.workspace, 'report.xlsx')
        if self.options['output']:
            output = self.options['output']

        # Load suites
        suites = None
        junitReport = os.path.abspath(os.path.join(self.workspace, 'report.xml'))
        logsDir = os.path.abspath(os.path.join(self.workspace, 'logs'))
        if os.path.isfile(junitReport):
            xml = JUnitXml.fromfile(junitReport)
            suites = { suite.name: { case.name: case.result is None for case in suite } for suite in xml }
            self.log.info('Suites were loaded from JUnit report file: "%s"', junitReport)
        elif os.path.isdir(logsDir):
            action = JUnit()
            suites = action.collectResults(logsDir)
            self.log.info('Suites were generated from logs: "%s"', logsDir)
        else:
            self.log.critical('No suites available. Please ensure that your workspace contains logs/ or junit report file')

        # Load configuration
        info = config.load(self.options['config'] or 'default')

        # Generate excel report and fill suites
        self.write(suites, output, info)
        self.log.success('XLSX report saved to "%s"', output)

    # Writes provided suites to output destination
    def write(self, suites, output, info, template=None, logo=None):
        excel = ExcelWriter(output, template or self.template)
        data = self.buildResultData(excel, suites, info.description, info.hardware[info.MODULE])
        core = self.buildCore(info.build)

        # Determine ranges depending on targets
        oLen =  len(core['os'])
        tLen =  len(core['target'])
        cLen =  len(core['comp'])
        targetsCount = oLen * tLen
        # if core['comp']:
        #     headerRows = 3
        #     targetsCount *= len(core['comp'])
        # else:
        headerRows = 2
        headerCols = len(self.TITLE_PREFIX) + len(self.TITLE_SUFFIX) + targetsCount

        # Construct title
        resultTitle1 = [t for x in core['os'] for t in [(x, 12)] * tLen]
        resultTitle2 = [(x, 12) for x in core['target']] * oLen
        title = [ self.TITLE_PREFIX + resultTitle1 + self.TITLE_SUFFIX ]
        title.append(
            [('', 0)] * len(self.TITLE_PREFIX) + resultTitle2 + [('', 0)] * len(self.TITLE_SUFFIX)
        )
        rawTitle = [[t[0] for t in row ] for row in title]

        from pprint import pprint

        # Create sheets, fill results and save
        with excel as writer:
            # B2
            writer.at(2, 2).fillImage(self.INFO_SHEET, logo or self.logo)
            # B6:E22
            writer.at(6, 2).to(22, 5).fillInfo(self.INFO_SHEET, info)
            # A1:E19
            writer.at(1, 1).to(19, 5).fillInfo(self.SUMMARY_SHEET, info)

            # Foreach board information, create and fill corresponding sheet
            for sheet in core['board']:
                writer \
                    .at(1, 1) \
                        .fillData(sheet, rawTitle) \
                    .at(headerRows + 1, 1) \
                        .fillData(sheet, data, numerate=True) \
                    .till(len(data), headerCols) \
                        .applyStyle(sheet, border=writer.TH_BORDER) \
                    .at(headerRows + 1, 5) \
                    .till(len(data), targetsCount) \
                        .formatResultTable(sheet) \
                    .at(1, 1).till(headerRows, headerCols) \
                        .formatTitle(sheet, title)

        # Fill formulas for summary (after saving)
        with excel as writer:
            # start from B22
            row, col, step = headerRows + 22, 2, len(self.SUMMARY) + headerRows
            for sheet in core['board']:
                summaryTitle = [
                    [(sheet, 40)] + resultTitle1,
                    [('', 0)] + resultTitle2
                ]
                rawSummaryTitle = [[t[0] for t in row ] for row in summaryTitle]
                data = self.buildSummaryData(writer, core, sheet, row, col)
                writer \
                    .at(row, col) \
                        .fillData(self.SUMMARY_SHEET, data) \
                    .till(len(data), len(data[0])) \
                        .applyStyle(self.SUMMARY_SHEET, border=writer.TH_BORDER, alignment=writer.ALIGN_CENTER) \
                        .formatSummaryTable(self.SUMMARY_SHEET) \
                    .till(len(data), 1) \
                        .applyStyle(self.SUMMARY_SHEET, font=writer.BOLD_FONT) \
                    .at(row - headerRows, col) \
                        .fillData(self.SUMMARY_SHEET, rawSummaryTitle) \
                    .till(headerRows, len(data[0])) \
                        .formatTitle(self.SUMMARY_SHEET, summaryTitle)

                # Apply styling per row
                for (i, entry) in enumerate(self.SUMMARY):
                    name, accessResults, style, func = entry
                    writer \
                        .at(row + i, col + 1) \
                        .till(1, targetsCount) \
                            .applyStyle(self.SUMMARY_SHEET, **style)
                row += step

    # Build array of tuples with suites and additional info
    def buildResultData(self, writer, suites, spec=None, hardware=None):
        spec = spec or { }
        hardware = isinstance(hardware, dict) and hardware or { }
        result = []
        for suite, cases in suites.items():
            for case, conclusion in cases.items():
                conclusion = writer.PASSED if conclusion else writer.FAILED
                try: # pythonic way
                    description = spec[suite][case]
                except KeyError:
                    description = ''
                try: # pythonic way
                    hw = hardware[suite]
                except KeyError:
                    hw = ''
                result.append( (suite, case, description, conclusion, hw) )
        return result

    # Build array of tuples for summary information
    def buildSummaryData(self, writer, core, sheet, row, col):
        length = len(core['os']) * len(core['target'])
        if False: #core['comp']:
            length *= len(core['comp'])

        result = []
        resultsCol = len(self.TITLE_PREFIX)
        for entry in self.SUMMARY:
            name, accessResults, style, func = entry
            summary = [ name ]
            for i in range(1, length + 1):
                if accessResults:
                    summary.append(func(writer, sheet, row, resultsCol + i))
                else:
                    summary.append(func(writer, sheet, row, col + i))
            result.append(tuple(summary))
        return result

    # Build meta-data of executed binaries
    def buildCore(self, info):
        # core = { }
        # if 'nxp' in compDict or 'tct' in compDict:
        #     core['comp'] = sorted(compDict.keys())
        #     targetDict = compDict[core['comp'][0]]
        # else:
        #     core['comp'] = []
        #     targetDict = compDict
        return {
            'comp': [],
            'os': [info.platforms['linux']],
            'target': [info.targets['build-v234ce-gnu-linux-d']],
            'board': [info.boards['evb']]
        }

    # InitWorkspace
    def initWorkspace(self, workspace):
        self.workspace = os.path.abspath(workspace)
        self.staticDir = os.path.abspath(os.path.join(workspace, 'static'))
        self.specDir = os.path.abspath(os.path.join(self.staticDir, 'spec'))
        self.template = self.options['template'] or os.path.join(self.staticDir, 'template.xlsx')
        self.logo = os.path.join(self.staticDir, 'logo.png')

        if self.options['fresh']:
            if os.path.exists(self.specDir): shutil.rmtree(self.specDir)

        if not os.path.exists(self.specDir):
            os.mkdir(self.specDir)

    # Validation
    def validate(self):
        # Template should exist
        if not os.path.isfile(self.template):
            self.log.critical('Template "%s" doen\'t exist', self.template)

        # Logo image should exist
        if not os.path.isfile(self.logo):
            self.log.critical('The logo file "%s" doesn\'t exist!', self.logo)

        # Check if required sheets do exist
        sheets = ['Document Info', 'Summary']
        wb = load_workbook(self.template, read_only=True)
        if not all(sheet in wb.sheetnames for sheet in sheets):
            self.log.critical('Template must contain "%s" sheets!', sheets)
