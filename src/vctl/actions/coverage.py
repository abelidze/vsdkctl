from .action import Action, pushd
from collections import defaultdict
import logging
import shutil
import sys
import os
import re

env = os.environ.get

class Coverage(Action):
    def __init__(self, options={ }):
        self.log = logging.getLogger('main')

# export ARM_CC_GCOV_OPT=1
# export ARM_CX_GCOV_OPT=1
# export ARM_LD_GCOV_OPT=1

# lcov --gcov-tool ntoaarch64-gcov --directory . --capture --rc lcov_branch_coverage=1 --rc lcov_excl_line=assert --output-file fast.info

# genhtml -t "fast" --function-coverage --branch-coverage --rc genhtml_branch_coverage=1 --rc geninfo_gcov_all_blocks=1 --rc lcov_branch_coverage=1 --rc lcov_excl_line=assert -o umat_gcov_html fast.info
