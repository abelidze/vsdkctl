from .action import Action, pushd
from urllib.parse import urlparse
from ..utils.remote.pmiko import RemoteClient
from collections import defaultdict
from fnmatch import fnmatch
import logging
import subprocess
import shutil
import sys
import os
import re

env = os.environ.get

class Run(Action):
    def __init__(self, options={ }):
        self.log = logging.getLogger('main')
        self.options = defaultdict(lambda: '.*', options)
        self.options['pattern'].replace('/', os.sep)
        url = urlparse('ssh://{}'.format(self.options['hostname'] or ''))
        self.ssh = RemoteClient(url.hostname, url.username, url.password, url.port or self.options['port'])
        self.dest = self.options['destination'] or '/tmp/run'
        self.pattern = re.compile('.*{}$'.format(self.options['pattern']), re.IGNORECASE)
        self.workspace = None

    @staticmethod
    def setParser(parser):
        parser.add_argument('hostname', nargs='?',
            default=env('VCTL_HOST', 'localhost'),
            help='Represents user@host:port for connecting. ENV: VCTL_HOST. Default is localhost')
        parser.add_argument('destination',
            default=env('VCTL_DEST', '/tmp/{}'.format(os.path.basename(os.getcwd()))),
            help='Remote workspace to work with. ENV: VCTL_DEST. Default is `/tmp/$(basename $(pwd))` (OS independent)')
        parser.add_argument('-P', '--port',
            dest='port',
            default=env('VCTL_PORT', '22'),
            help='Port to use. It is ignored if you pass it in `hostname`. ENV: VCTL_PORT. Default is 22')
        parser.add_argument('-p', '--pattern',
            dest='pattern',
            default=env('VCTL_PATTERN', '.*'),
            help='Run only those executables that matches pattern. ENV: VCTL_PATTERN. Default: .*')
        # parser.add_argument('--sync',
        #     action='store_true',
        #     help='Sync workspaces using SCP before running')
        # parser.add_argument('--rsync',
        #     action='store_true',
        #     help='Sync workspaces using RSYNC before running')

    # Run
    def run(self, workspace):
        self.initWorkspace(workspace)
        self.ssh.execute('ls -la')
        # for target in self.targets(self.appsDir):
        #     self.copy(target)

    # InitWorkspace
    def initWorkspace(self, workspace):
        self.workspace = os.path.abspath(workspace)
        self.driversDir = os.path.abspath(os.path.join(self.workspace, 'drivers'))
        self.assetsDir = os.path.abspath(os.path.join(self.workspace, 'assets'))
        self.appsDir = os.path.abspath(os.path.join(self.workspace, 'apps'))
        if not os.path.exists(self.appsDir):
            os.mkdir(self.appsDir)
        if not os.path.exists(self.driversDir):
            os.mkdir(self.driversDir)
        if not os.path.exists(self.assetsDir):
            os.mkdir(self.assetsDir)

    # Copy
    def copy(self, path):
        rc = 1
        with pushd(path):
            # Copy binaries
            for file in os.listdir('.'):
                if fnmatch(file, '*.ko'):
                    self.log.debug('Copying {} from {} to {}'.format(file, path, self.driversDir))
                    shutil.copy(file, self.driversDir)
                    rc = 0
                if fnmatch(file, '*.elf'):
                    self.log.debug('Copying {} from {} to {}'.format(file, path, self.appsDir))
                    shutil.copy(file, self.appsDir)
                    rc = 0
                if rc == 1 and fnmatch(file, '*.a'):
                    rc = 2
                if fnmatch(file, '*.cmm'):
                    self.log.debug('Copying {} from {} to {}'.format(file, path, self.workspace))
                    shutil.copy(file, self.workspace)

            # Only static libs found
            if rc == 2:
                self.log.info('Only static libraries are found')
                rc = 0

            # Copy assets
            assetsDir = os.path.abspath('../data')
            if os.path.exists(assetsDir):
                self.log.info('Copying assets from {} to {}'.format(assetsDir, self.assetsDir))
                os.system('cp -a {}{}. {}'.format(assetsDir, os.sep, self.assetsDir))
        return rc

    # Targets
    def targets(self, path):
        for subdir, dirs, files in os.walk(path):
            if self.pattern.match(subdir) is not None:
                dirs[:] = []
                yield subdir
