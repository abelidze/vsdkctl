from .action import Action
from collections import defaultdict, Mapping
from junitparser import TestCase, TestSuite, JUnitXml, Failure
import xml.etree.ElementTree as ET
import logging
import os
import re

env = os.environ.get
splitext = os.path.splitext

class JUnit(Action):
    help = 'JUnit report generation'
    description = \
        'JUnit action helps you to generate JUnit XML reports from your logs and CppUnit xml output\n' \
        'Usage examples:\n' \
        ' - vctl junit $WORKSPACE\n' \
        ' - vctl junit $WORKSPACE -p "(apex|csi)"\n' \
        ' - vctl junit -l /path/to/logs_and_xml -o /path/to/output.xml' \

    def __init__(self, options={ }):
        self.log = logging.getLogger('main')
        self.options = defaultdict(lambda: '', options)
        self.options['pattern'].replace('/', os.sep)
        self.pattern = re.compile('.*{}$'.format(self.options['pattern']), re.IGNORECASE)
        self.logregex = re.compile(r'([^\s]+)::([^\s]+)', re.IGNORECASE)
        self.okmsg = ' : OK'

    @staticmethod
    def setParser(parser):
        parser.add_argument('-p', '--pattern',
            dest='pattern',
            default=env('VCTL_PATTERN', '.*'),
            help='Collect result only for specific targets. \
                \nDefault: .* \
                \nENV: VCTL_PATTERN')
        parser.add_argument('-o', '--output',
            dest='output',
            default=env('VCTL_XML_REPORT', ''),
            help='Output path for XML report file. \
                \nDefault: {WORKSPACE}/report.xml \
                \nENV: VCTL_XML_REPORT')
        parser.add_argument('-l', '--logs',
            dest='logs',
            default=env('VCTL_LOGS', ''),
            help='Path to logs for parsing. \
                \nDefault: {WORKSPACE}/logs \
                \nENV: VCTL_LOGS')

    # Run JUnit action
    def run(self, workspace):
        path = os.path.join(workspace, 'logs')
        if self.options['logs']:
            path = self.options['logs']
        if not os.path.exists(path):
            self.log.critical('%s doesn\'t exist', path)
        suites = self.collectResults(path)
        report = self.generateXml(suites)
        output = os.path.join(workspace, 'report.xml')
        if self.options['output']:
            output = self.options['output']
        report.write(output)
        self.log.success('XML report saved to "%s"', output)

    # Generate JUnit from collected suites
    def generateXml(self, suites):
        xml = JUnitXml()
        for suite, cases in suites.items():
            suite = TestSuite(suite)
            if not cases:
                xml.add_testsuite(suite)
                continue

            for case, result in cases.items():
                case = TestCase(case)
                if not result:
                    case.result = Failure()
                suite.add_testcase(case)

            xml.add_testsuite(suite)
        return xml

    # Collect results from .xml and .log files
    def collectResults(self, path):
        suites = { }
        supported = ['.xml', '.log']
        files = [y for y in ({ splitext(x)[0]: True for x in os.listdir(path) if splitext(x)[1] in supported })]
        for file in files:
            file = os.path.join(path, file)
            if os.path.exists(file + '.xml'):
                update(suites, self.parseXml(file))
            else:
                update(suites, self.parseLog(file))
        return suites

    # Parse xml file
    def parseXml(self, filename):
        filepath = filename + '.xml'
        if not os.path.exists(filepath):
            self.log.error('Can\'t open "%s"', filepath)
            return { }

        suitename = os.path.basename(filename)
        result = { suitename: { } }
        root = ET.parse(filepath).getroot()

        # Passed tests
        for tests in root.iter('SuccessfulTests'):
            for test in tests.findall('Test'):
                test = test.find('Name')
                if test is None: continue

                suite, case = test.text.strip().split('::')
                if suite not in result:
                    self.log.warn('"%s" may contain more than one test-suite, it\'s not recommended', filename)
                    result[suite] = { }
                result[suite][case] = True
                self.log.debug('XML: %s::%s passed', suite, case)

        # Failed tests
        for tests in root.iter('FailedTests'):
            for test in tests.findall('Test'):
                test = test.find('Name')
                if test is None: continue

                suite, case = test.text.strip().split('::')
                if suite not in result:
                    self.log.warn('"%s" may contain more than one test-suite, it\'s not recommended', filename)
                    result[suite] = { }
                result[suite][case] = False
                self.log.debug('XML: %s::%s failed', suite, case)
        return result

    # Parse log file
    def parseLog(self, filename):
        filepath = filename + '.log'
        if not os.path.exists(filepath):
            self.log.error('Can\'t open "%s"', filepath)
            return { }

        suitename = os.path.basename(filename)
        result = { suitename: { } }
        data = open(filepath).read()

        for m in self.logregex.finditer(data):
            suite, case = m.group(1), m.group(2)
            if suite not in result:
                self.log.warn('"%s" may contain more than one test-suite, it\'s not recommended', filename)
                result[suite] = { }
            result[suite][case] = data.find(self.okmsg, m.end(0)) != -1
            self.log.debug('LOG: %s::%s %s', suite, case, 'passed' if result[suite][case] else 'failed')
        return result


def update(d, u):
    for key, val in u.items():
        if isinstance(val, Mapping):
            tmp = update(d.get(key, { }), val)
            d[key] = tmp
        elif isinstance(val, list):
            d[key] = (d.get(key, []) + val)
        else:
            d[key] = u[key]
    return d
