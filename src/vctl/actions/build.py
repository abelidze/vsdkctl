from .action import Action, pushd
from collections import defaultdict
from fnmatch import fnmatch
import logging
import subprocess
import shutil
import sys
import os
import re

env = os.environ.get

# Data used for validation
MODULE_LIST = ['apexcv', 'apu', 'acf', 'fdma', 'oal', 'sdi', 'h264enc', 'apex', 'cgd', 'h264dec', 'umat', 'viu', 'seq', 'jpegdcd', 'csi', 'demos']
TARGET_BOARD = ['linux-d','linux_d', 'linux-o', 'linux_o', 'sa-d', 'sa_d', 'sa-o', 'sa_o']
TARGET_PC = ['x86-gnu-linux-d','x86-gnu-windows-d', 'apu-nxp', 'apu-tct','apu_nxp', 'apu_tct']
COMPILERS = ['nxp', 'tct']

class Build(Action):
    help = 'Simplifies using NBUILD'
    description = \
        'Build action sumplifies usage of NBUILD and provides you very flexible way to build multiple different targets. \n' \
        'Usage examples:\n' \
        ' - vctl build $WORKSPACE -p kernel -b $SDK/libs -s $SDK/3rdparty/oal/libs/kernel/driver \n' \
        ' - vctl build $WORKSPACE -p apex -b $SDK/tests/fpt/unittest\n' \
        ' - vctl build $WORKSPACE -p apps -b $SDK/demos -c qcc --os qnx --mode o'

    def __init__(self, options={ }, strategy='all'):
        self.log = logging.getLogger('main')
        self.options = defaultdict(lambda: '', options)
        self.options['pattern'].replace('/', os.sep)
        self.target = self.options['target'].format_map(defaultdict(lambda: '.*', options))
        self.target = re.compile(self.target, re.IGNORECASE)
        self.pattern = re.compile('.*{}$'.format(self.options['pattern']), re.IGNORECASE)
        self.buildDirs = self.options['build'] or ['.']
        self.scanDirs = self.options['scan'] or [ ]
        self.strategy = 'build' if self.options['noclean'] else strategy
        self.cmd = {
            'CLEAN': 'make APU_COMP={} clean -j{}'
                .format(self.options['apucomp'], self.options['jobs']),
            'CLEANSUB': 'make APU_COMP={} cleansub -j{}'
                .format(self.options['apucomp'], self.options['jobs']),
            'MAKE': 'make APU_COMP={} allsub -j{}'
                .format(self.options['apucomp'], self.options['jobs'])
        }
        self.cache = { }

    @staticmethod
    def setParser(parser):
        parser.add_argument('-b', '--build',
            dest='build',
            action='append',
            default=[],
            help='Location for building targets. Default is current location')
        parser.add_argument('-s', '--scan',
            dest='scan',
            action='append',
            required=False,
            help='Location for additional search and copy binaries')
        parser.add_argument('-j', '--jobs',
            dest='jobs',
            default=env('VCTL_JOBS', '16'),
            help='Set jobs for multithreading build. \
                \nDefault: 16 \
                \nENV: VCTL_JOBS')
        parser.add_argument('-p', '--pattern',
            dest='pattern',
            default=env('VCTL_PATTERN', '.*'),
            help='Pattern for searching application/module containing build targets. \
                \nDefault: .* \
                \nENV: VCTL_PATTERN')
        parser.add_argument('-t', '--target',
            dest='target',
            default=env('VCTL_TARGET', '{prefix}-{compiler}-{os}-{mode}'),
            help='Build target. Can be pattern that refer other options. \
                \nDefault: {prefix}-{compiler}-{os}-{mode} \
                \nENV: VCTL_TARGET')
        parser.add_argument('-a', '--apucomp',
            dest='apucomp',
            default=env('VCTL_APUCOMP', 'nxp'),
            help='APU compiler. It would be passed to APU_COMP variable for make. \
                \nDon\'t forget to export APU_TOOLS environment variable before building. \
                \nDefault: nxp \
                \nENV: VCTL_OS \
                \nNBUILD defines: "x86", "nxp", "tct"')
        parser.add_argument('-c', '--compiler',
            dest='compiler',
            default=env('VCTL_COMPILER', 'gnu'),
            help='Target\'s compiler. It should be defined in your PATH. \
                \nDefault: gnu \
                \nENV: VCTL_COMPILER \
                \nNBUILD defines: \
                \n - tct for APU2 (Synopsys vendor), \
                \n - nxp for APU2 (NXP vendor), \
                \n - ghs for AArch64 (Green Hills vendor), \
                \n - qcc for AArch64 (QNX vendor), \
                \n - gnu for AArch64/M0 (Linaro vendor), \
                \n - gnu for x86 (GNU vendor)')
        parser.add_argument('-m', '--mode',
            dest='mode',
            default=env('VCTL_MODE', 'd'),
            help='Target build mode. \
                \nDefault: d \
                \nENV: VCTL_MODE \
                \nNBUILD defines: "d" for debug and "o" for optimized')
        parser.add_argument('--prefix',
            dest='prefix',
            default=env('VCTL_PREFIX', 'build-v234ce'),
            help='Prefix for build target. Usually contains target platform. \
                \nDefault: build-v234ce \
                \nENV: VCTL_PREFIX \
                \nNBUILD defines: \
                \n - v234ce for TreeRunner, \
                \n - isp for IPU, \
                \n - apu for APU2, \
                \n - x86 for local development')
        parser.add_argument('--os',
            dest='os',
            default=env('VCTL_OS', 'linux'),
            help='Target OS. \
                \nDefault: linux \
                \nENV: VCTL_OS \
                \nNBUILD defines: "windows", "linux", "qnx", "sa", "integrity"')
        parser.add_argument('--noclean',
            action='store_true',
            help='Don\'t clean the targets')
        parser.add_argument('--nondestructive',
            action='store_true',
            help='Leave workspace [output] untouched')

        parser.set_defaults(nondestructive=False)

    # Run Build action
    def run(self, workspace):
        # Build and copy
        for inDir in self.buildDirs:
            if self.strategy == 'clean':
                self.cleanAll(inDir)
            elif self.strategy == 'build':
                self.buildAll(inDir, workspace)
            elif self.strategy == 'all':
                self.cleanBuildAll(inDir, workspace)
            else:
                self.log.critical('Unknown strategy %s for build action', self.strategy)

        # Copy only
        if self.scanDirs:
            self.initWorkspace(workspace)
            patternBackup = self.pattern
            self.pattern = re.compile('.*')
            for inDir in self.scanDirs:
                for target in self.targets(inDir):
                    self.copy(target)
            self.pattern = patternBackup

    # CleanAll
    def cleanAll(self, inDir):
        for target in self.targets(inDir):
            self.clean(target)

    # CleansubAll
    def cleansubAll(self, inDir):
        for target in self.targets(inDir):
            self.cleansub(target)

    # BuildAll
    def buildAll(self, inDir, outDir):
        self.initWorkspace(outDir)
        for target in self.targets(inDir):
            self.make(target)

    # CleanBuildAll
    def cleanBuildAll(self, inDir, outDir):
        self.initWorkspace(outDir)
        for target in self.targets(inDir):
            self.clean(target)
            self.make(target)

    # InitWorkspace
    def initWorkspace(self, workspace):
        self.workspace = os.path.abspath(workspace)
        self.driversDir = os.path.abspath(os.path.join(self.workspace, 'drivers'))
        self.assetsDir = os.path.abspath(os.path.join(self.workspace, 'assets'))
        self.appsDir = os.path.abspath(os.path.join(self.workspace, 'apps'))

        if self.options['fresh']:
            if os.path.exists(self.driversDir): shutil.rmtree(self.driversDir)
            if os.path.exists(self.assetsDir): shutil.rmtree(self.assetsDir)
            if os.path.exists(self.appsDir): shutil.rmtree(self.appsDir)

        if not os.path.exists(self.appsDir):
            os.mkdir(self.appsDir)
        if not os.path.exists(self.driversDir):
            os.mkdir(self.driversDir)
        if not os.path.exists(self.assetsDir):
            os.mkdir(self.assetsDir)

    # Clean
    def clean(self, targetDir):
        self.log.info('Cleaning %s ...', targetDir)
        with pushd(targetDir):
            process = subprocess.Popen(self.cmd['CLEAN'], shell=True,
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT)
            for output in stdout(process):
                self.log.debug(output.decode('utf-8').rstrip())
            return process.poll()

    # Cleansub
    def cleansub(self, targetDir):
        self.log.info('Cleansub %s ...', targetDir)
        with pushd(targetDir):
            process = subprocess.Popen(self.cmd['CLEANSUB'], shell=True,
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT)
            for output in stdout(process):
                self.log.debug(output.decode('utf-8').rstrip())
            return process.poll()

    # Make
    def make(self, targetDir):
        targetDir = os.path.abspath(targetDir)
        self.log.info('Building %s ...', targetDir)
        with pushd(targetDir):
            rc = 0
            process = subprocess.Popen(self.cmd['MAKE'], shell=True,
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT)
            for output in stdout(process):
                self.log.debug(output.decode('utf-8').rstrip())
            rc = process.poll()

            if targetDir in self.cache:
                self.log.error('Already built %s', targetDir)
            else:
                self.cache[targetDir] = True

            if rc == 0:
                rc = self.copy('.')
                if rc == 0:
                    self.log.success('BUILD PASSED: %s', targetDir)
                else:
                    self.log.error('BUILD FAILED [no content]: %s', targetDir)
            else:
                self.log.error('BUILD FAILED: %s', targetDir)
            return rc

    # Copy
    def copy(self, path):
        rc = 1
        with pushd(path):
            # Copy binaries
            for file in os.listdir('.'):
                if fnmatch(file, '*.ko'):
                    self.log.debug('Copying %s from %s to %s', file, path, self.driversDir)
                    shutil.copy(file, self.driversDir)
                    rc = 0
                if fnmatch(file, '*.elf'):
                    self.log.debug('Copying %s from %s to %s', file, path, self.appsDir)
                    shutil.copy(file, self.appsDir)
                    rc = 0
                if rc == 1 and fnmatch(file, '*.a'):
                    rc = 2
                if fnmatch(file, '*.cmm'):
                    self.log.debug('Copying %s from %s to %s', file, path, self.workspace)
                    shutil.copy(file, self.workspace)

            # Only static libs found
            if rc == 2:
                self.log.info('Only static libraries are found')
                rc = 0

            # Copy assets
            assetsDir = os.path.abspath('../data')
            if os.path.exists(assetsDir):
                self.log.info('Copying assets from %s to %s', assetsDir, self.assetsDir)
                os.system('cp -a {}{}. {}'.format(assetsDir, os.sep, self.assetsDir))
        return rc

    # Modules
    def modules(self, path):
        for subdir, dirs, files in os.walk(path):
            if self.pattern.match(subdir) is not None:
                dirs[:] = []
                yield subdir

    # Targets
    def targets(self, path):
        for moduleDir in self.modules(path):
            for root, dirs, files in os.walk(moduleDir):
                for subdir in dirs:
                    if self.target.match(subdir) is None:
                        continue
                    target = os.path.join(root, subdir)
                    if os.path.isfile(os.path.join(target, 'Makefile')):
                        yield os.path.abspath(target)


def stdout(process):
   while True:
    data = process.stdout.readline()
    if data:
        yield data
    else:
        break


    # if aPlatform == 'vsdk' or aPlatform == 'vsky':
    #     self.platform = aPlatform
    # else:
    #     print("Error: Argparse platform incorrect. Only support vsdk or vsky")
    # if aInstaller == 'src' or aInstaller == 'bin':
    #     self.installer = aInstaller
    # else:
    #     print("Error: Argparse installer incorrect. Only support src or bin")
    # if aCompiler == 'nxp' or aCompiler == 'tct':
    #     self.compiler = aCompiler
    # else:
    #     print("Error: Argparse compiler incorrect. Only support nxp or tct")
    # self.target = ""
    # if str(TARGET_PC).find(aTarget) >= 0:
    #     self.target = aTarget.replace('_','-')
    # elif str(TARGET_BOARD).find(aTarget) >= 0:
    #     self.target = 'build-v234ce-gnu-' + aTarget.replace('_','-')
    # else:
    #     print("Error: Argparse target incorrect. Only support array")
    #     print(TARGET_BOARD)
    #     print(TARGET_PC)