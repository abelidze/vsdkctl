from collections import defaultdict
from glob import iglob
import yaml
import os
import re
import io

ROOT = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
ROOT_STATIC = os.environ.get('VCTL_STATIC', os.path.join(ROOT, 'static'))
WILDCARD = re.compile(r'^.*(\*|\?|\[!?.+\]).*$')

cache = { }

# Return path to default static files
def static():
    return ROOT_STATIC

# Load yml config from static
def load(name, root=ROOT_STATIC):
    if not name.endswith('.yml'):
        name += '.yml'
    path = os.path.join(root, name)
    return read(path)

# Read yml config by full path
def read(path):
    if path in cache:
        return cache[path]
    if not os.path.isfile(path):
        return dotdict()
    with open(path, 'r') as f:
        result = dotdict(yaml.load(f, Loader=IncludeLoader))
        cache[path] = result
        return result

# Loader with include support
class IncludeLoader(yaml.SafeLoader):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_constructor('!include', self.include)
        if 'root' in kwargs:
            self.root = kwargs['root']
        elif isinstance(self.stream, io.IOBase):
            self.root = os.path.dirname(self.stream.name)
        else:
            self.root = os.path.curdir

    @staticmethod
    def include(self, node):
        path = os.path.join(self.root, self.construct_scalar(node))
        result = None
        if re.match(WILDCARD, path):
            result = { k: v for f in iglob(path, recursive=True) for k, v in read(f).items() }
        else:
            result = read(path)
        return result


class dotdict(defaultdict):
    def __init__(self, *args, **kwargs):
        super().__init__(dotdict, *args, **kwargs)
        for k, v in list(self.items()):
            if type(v) == dict:
                self[k] = dotdict(v)

    def __str__(self):
        return ''

    def __missing__(self, key):
        return '<{}>'.format(key)

    def __getitem__(self, key):
        value = super().__getitem__(key)
        if type(value) == str:
            return value.format(**self)
        return value

    def __setattr__(self, key, value):
        self[key] = value

    def __getattr__(self, key):
        return self[key]
