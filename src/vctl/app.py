from . import actions
from . import config
import shutil
import argparse
import logging
import timeit
import sys
import os

env = os.environ.get

class Application:
    def __init__(self):
        self.log = logging.getLogger('main')
        self.workspaces = { }
        self.workspace = None

    # InitWorkspace
    def initWorkspace(self, workspace, fresh=False):
        workspace = os.path.abspath(workspace)
        if workspace in self.workspaces: return

        self.workspaces[workspace] = True

        self.workspace = workspace
        if not os.path.exists(self.workspace):
            os.makedirs(self.workspace)
        elif fresh:
            shutil.rmtree(self.workspace)
            os.makedirs(self.workspace)

        self.staticDir = os.path.join(workspace, 'static')
        if not os.path.exists(self.staticDir):
            shutil.copytree(config.static(), self.staticDir)
        elif fresh:
            shutil.rmtree(self.staticDir)
            shutil.copytree(config.static(), self.staticDir)

        self.log.info('Workspace is setted up at %s', self.workspace)

    # Entrypoint
    def run(self):
        self.start = timeit.default_timer()

        # Main options parser
        parser = argparse.ArgumentParser(description='Automation tool for VISION SDK')
        parser.add_argument('--debug', action='store_true', help='Enable debugging logger')

        # Action's subparsers
        subparsers = parser.add_subparsers(help='Available actions', dest='action')
        for command, action in actions.available.items():
            title = '{} action'.format(command).capitalize()
            subparser = subparsers.add_parser(
                command,
                help=title if not hasattr(action, 'help') else action.help,
                description=title if not hasattr(action, 'description') else action.description,
                formatter_class=argparse.RawTextHelpFormatter
            )
            action.setParser(subparser)
            subparser.add_argument('--debug',
                action='store_true',
                help='Enable debugging logger')
            subparser.add_argument('--fresh',
                action='store_true',
                help='Clean workspace only for current action')
            subparser.add_argument('--freshall',
                action='store_true',
                help='Clean the whole workspace before execution')
            subparser.add_argument('workspace', nargs='?',
                default=env('VCTL_WORKSPACE', '.'),
                help='Working directory for output files. \
                    \nDefault is current workdir \
                    \nENV: VCTL_WORKSPACE')

        # Hanle options and choose action
        self.args = parser.parse_args()

        if self.args.debug:
            self.log.setLevel(logging.DEBUG)
        else:
            self.log.setLevel(logging.INFO)

        if self.args.action == None:
            parser.print_usage()
            return
        elif not self.args.action in actions.available:
            self.log.critical('Unknown command `%s`', self.args.action)

        # Prepare workspace
        self.initWorkspace(self.args.workspace, self.args.freshall)

        # Execute action
        self.action = actions.available[self.args.action](vars(self.args))
        self.action.run(self.args.workspace)

        # switch
            # build
                # discover targets by input and pattern
                # clean
                # build with predefined settings

            # coverage
                # build with gcov support
                # run, collect lcov files

            # junit
                # collect output xml | parse logs
                # parse stdout > stdin
                # produce JUnit files

            # report
                # collect everything and do magic

        # Finish execution
        self.log.debug('Execution time: %f%s', timeit.default_timer() - self.start, 's')
        if self.log.errors > 0:
            self.log.warn('Finished with %s error%s', self.log.errors, 's' if self.log.errors > 1 else '')
            sys.exit(1)

